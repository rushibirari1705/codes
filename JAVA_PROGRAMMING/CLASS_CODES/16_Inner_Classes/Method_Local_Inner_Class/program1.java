

// Method Local Inner class

class Outer{
    int x = 100;

        void method1(){
            int y = 1002;
            //Method-local-Inner class 1
            class Inner1{
                void m1(){
                    System.out.println("In Method-local Inner1 class Method");
                }
            }
            class Inner2{
               void m2(){
                    System.out.println("In Method-local Inner2 class Method");
                }
            }

            Inner1 obj1 = new Inner1();
            obj1.m1();
            System.out.println("value of x : " +y);

            Inner2 obj2 = new Inner2();
            obj2.m2();
        }
}
class client{
    public static void main(String[] args) {
        Outer obj = new Outer();
        obj.method1();
        
    }
}