
// Method Local Inner class

class Outer{
    int x = 100;

        void m1(){
            
            //Method-local-Inner class 1
            class Inner1{
                void m1(){
                    System.out.println("In Method-local Inner1 class Method");
                }
            }
           
            Inner1 obj1 = new Inner1();
            obj1.m1();
        }
          
}
class client{
    public static void main(String[] args) {
        Outer obj = new Outer();
        obj.m1();
        
    }
}