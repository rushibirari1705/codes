
// Real Time example of method local class

class Bank{

    void CreateAccount(String name){

        class SavingAccount{
            void CustomerName(){
                System.out.println(name);
            }
        }

        class currentAccount{
            void CustomerName(){
                System.out.println(name);
            }
        }

        SavingAccount s1 = new SavingAccount();
        s1.CustomerName();

        currentAccount c1 = new currentAccount();
        c1.CustomerName();
        
    }

}

class client{
    public static void main(String[] args) {

        Bank obj = new Bank();
        obj.CreateAccount("Rushikesh Birari");
        
    }
}