
// Concept of normal inner class

class Outer{
    

        // inner class
        class Inner{
            void method1(){
                System.out.println("In Inner method m1");
            }
        }
    
        void method1(){
            System.out.println("In Outer method m1");
        }
}

class client{
    public static void main(String[] args) {
        // to access the inner class first we need to create object of outer class

        Outer obj = new Outer();

        obj.method1(); // call outer method
        
        Outer.Inner obj1 = obj.new Inner();

        obj1.method1(); // call inner method
    }
}

/* Note :   In this code if u want to access inner class methods then we need to create 
 *          object of outer class  and on that object we need to create object of inner class.
 *           and on that object call the method of inner class.
 */