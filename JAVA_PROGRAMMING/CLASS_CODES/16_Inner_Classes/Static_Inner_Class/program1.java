

// concept of Static Inner class

class Outer{
    int x = 10;
    static int y = 101;

        static class Inner{
            void m1(){
                System.out.println("In static inner class");
               // System.out.println(x); // error  :non-static variable x cannot be referenced from a static context
                System.out.println(y);
            }
        }

    void m1(){
        System.out.println("In Outer class");
    }
}

class client{
    public static void main(String[] args) {

        Outer.Inner obj = new Outer.Inner(); // Inner class is static so called directly by class name
        
        obj.m1();
        
    }
}
/*  Note :  static Inner class is defined by static keyword in class name and this class is static so it directly 
 *           by class name
 */