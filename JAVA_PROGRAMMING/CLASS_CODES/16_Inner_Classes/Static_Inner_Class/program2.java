
// concept of Static Inner class

class Outer{
    int x = 10;
         static class Inner{
            static int y = 101;
            void m1(){
                System.out.println("In static inner class");
                System.out.println(y);
            }
        }

    void m1(){
        System.out.println("In Outer class");
    }
}

class client{
    public static void main(String[] args) {

        Outer.Inner obj = new Outer.Inner(); // Inner class is static so called directly by class name
        
        obj.m1();
        
    }
}
/*  Note :  static Inner class is defined by static keyword in class name and this class is static so it directly 
 *           by class name
 */