

// Concept of static varibles in Inner classes

class Outer{
    int x = 10;
    static int y =  20;
        
        class Inner{
            int a = 100;
            final static int b = 1001;
        }
}

class client{
    public static void main(String[] args) {
        // accessing static variable directly class name
        System.out.println(Outer.y);
        // accessing non-static variable by creating object of that class
        Outer obj = new Outer();
        System.out.println(obj.x);

        // accessing static variable directly class name
        System.out.println(Outer.Inner.b);

        // accessing non- static variable by creating object of inner class
        System.out.println(obj.new Inner().a);

    }
}