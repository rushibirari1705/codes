
// ArrayList concept 
/*  How to create ArrayList and perform add elements operation
 */

 import java.util.ArrayList;

 class ArrayListDemo {
    public static void main(String[] args) {
        // creating a new ArrayList

        ArrayList<Integer> list = new ArrayList<Integer>();

        // Add elements to ArrayList

        list.add(10);
        list.add(20);
        list.add(30);
        list.add(40);
        list.add(50);
        list.add(10);
        
        
        System.out.println(list);
    }
 }

/*Note   : 
 *      In the ArrayList duplicate elements are also store and we can add elements as per requirement of user
 */