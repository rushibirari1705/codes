

// ArrayList concept 
/* create ArrayList and perform all  operation add, remove, get, size
 */

 import java.util.ArrayList;

 class ArrayListDemo {
    public static void main(String[] args) {
        // creating a new ArrayList

        ArrayList<Integer> list = new ArrayList<Integer>();

        // Add elements to ArrayList

        list.add(10);
        list.add(20);
        list.add(30);
        list.add(40);
        list.add(50);
        list.add(60);
        
        
        System.out.println("ArrayList after adding elements  : "+list);

        // get element from arrayList
        // by using index we can get elements from list

        System.out.println( "Index 1 : "+ list.get(1));
        System.out.println( "Index 2 : "+ list.get(2));
        System.out.println( "Index 3 : "+ list.get(3));
        System.out.println( "Index 4 : "+ list.get(4));

        // Add element in between 

        list.add(1,100);
        list.add(2,200);
        System.out.println("After adding element at position : "  + list);

        // modify element in list

        list.set(5, 500);        
        System.out.println("After modifying element  : "  + list);
        // get size of list

       System.out.println("Size of list: " + list.size());

        
        // remove elements from ArrayList
        
        list.remove(0);
        list.remove(1);

        System.out.println("After removing elements  :  "+list);
        
    }
 }

