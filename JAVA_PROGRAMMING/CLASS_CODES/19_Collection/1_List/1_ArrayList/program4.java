
// ArrayList concept 
/*  We use all methods of ArrayList */

import java.util.*;

class ArrayListDemo {
    public static void main(String[] args){
        ArrayList<Integer> list = new ArrayList<>();

        // Add elements
            list.add(10);
            list.add(40);
            list.add(50);
            list.add(20);
            list.add(30);
            

            System.out.println(list);

                
            // check list is empty or not 

            System.out.println(list.isEmpty()); // true : list is empty & false : list is not empty

            // Sort list

            Collections.sort(list); 
            System.out.println("After sorting : "   + list);
        
            // remove all

            list.removeAll(list);
            System.out.println(list);
     }
}