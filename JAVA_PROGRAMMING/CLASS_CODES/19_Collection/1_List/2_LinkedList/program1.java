
// Creating a Linkedlist and add Integer elements to list

import java.util.*;

class LinkedListDemo{
    public static void main(String[] args) {
        
        LinkedList<Integer> list = new LinkedList<>();

        list.add(10);        
        list.add(20);
        list.add(30);
        list.add(40);
    
       
        System.out.println("Linked list elements are : "+list);

    }
}