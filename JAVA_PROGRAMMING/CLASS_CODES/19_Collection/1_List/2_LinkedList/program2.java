
// Creating a Linkedlist and add String elements to list
import java.util.*;

class LinkedListDemo{
    public static void main(String[] args) {
        
        LinkedList<String> list = new LinkedList<>();

        list.add("Rushi");        
        list.add("Pavan");
        list.add("Vinit");
        list.add("Nikhil");
    
       
        System.out.println("Linked list elements are : "+list);

    }
}