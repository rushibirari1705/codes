
// Creating a Linkedlist and add Integer & String elements to list

import java.util.*;

class LinkedListDemo{
    public static void main(String[] args) {
        
        LinkedList list = new LinkedList();

        list.add(10);        
        list.add(20);
        list.add(30);
        list.add(40);

        list.add("Rushi");
        list.add("Nikhil");
        list.add("pavan");
        list.add("Vinit");
    
       
        System.out.println("Linked list elements are : "+list);

    }
}