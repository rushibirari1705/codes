
// Creating a Linkedlist and perform operations

import java.util.*;

class LinkedListDemo{
    public static void main(String[] args) {
        
        // create a new LinkedList
        LinkedList<Integer> list = new LinkedList<>();

        // add elements to the list
        list.add(10);        
        list.add(20);
        list.add(30);
        list.add(40);
    
       
        System.out.println("Linked list elements are : "+list);

        // add element to first

        list.addFirst(100);
         System.out.println("After add first - Linked list elements are : "+list);

         // add element to last
         list.addLast(200);
         System.out.println("After addLast- Linked list elements are : "+list);

         // get first element of list
         System.out.println("First element of list is : "+list.getFirst());

         // get last element of list
         System.out.println("Last element of list is :  "+list.getLast());
        
         // peek first element of list
         System.out.println("peek First element :  "+list.peekFirst());


         // remove last element of list
         list.removeLast();
         System.out.println("After removeLast - Linked list elements are : "+list);

         // remove first element of list
         list.removeFirst();
         System.out.println("After removeFirst- Linked list elements are : "+list);
        
        

    }
}