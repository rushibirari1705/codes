
// Use of Collections class for sorting the list 

import java.util.*;


class Demo{
    public static void main(String[] args) {
        ArrayList<String> al = new ArrayList<String>();

        al.add("Rushi");
        al.add("Vinit");
        al.add("Pavan");
        al.add("Nikhil");
        al.add("Ganesh");
        al.add("Rushi");

        System.out.println(al);

        // now we want to sort this arraylist by using TreeSet()

        TreeSet<String> ts = new TreeSet<>(al);

        System.out.println(ts);

        /* Note : By using TreeSet we get loss of data from the list because TreeSet removes Duplicate data */
    }
}