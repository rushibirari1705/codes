
// use of comparator interface for sorting data for User Defined classes

import java.util.*;

class Employee{

    String empName = null;
    float empSal = 0.0f;

    Employee(String empName, float empSal){
        this.empName = empName;
        this.empSal = empSal;

    }

    public String toString(){
        return "{ " + empName + " : " + empSal + " }";
    }
}

// use of Comparator interface

class SortByName implements Comparator<Employee>{

    // Having To abstract methods compare & equals

    public int compare(Employee obj1, Employee obj2) {

        return obj1.empName.compareTo(obj2.empName);

    }

}

class SortBySal implements Comparator<Employee>{

    // Having To abstract methods compare & equals

    public int compare(Employee obj1, Employee obj2) {

        return (int)(obj1.empSal - obj2.empSal); // typecasting for return type is int

    }

}

class Demo{
    public static void main(String[] args) {
        
        ArrayList<Employee> al = new ArrayList<>();

        al.add(new Employee("Rushi",  645566.55f));
        al.add(new Employee("Vinit" , 431212.22f));
        al.add(new Employee("Nikhil" ,243433.66f));
        al.add(new Employee("Ganesh" ,243403.66f));
        al.add(new Employee("Pavan" , 434123.66f));
        al.add(new Employee("Nikhil" ,756575.66f));

        System.out.println(al); // gives as per insertion sequence in o/p

        // give for sorting

        Collections.sort(al, new SortBySal());

        System.out.println(al); // Sort by Name of Employee

    }
}