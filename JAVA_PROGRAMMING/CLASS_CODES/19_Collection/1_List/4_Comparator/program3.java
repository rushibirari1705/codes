

// Real time example of Movies

import java.util.*;

class Movies {
    String movie_Name = null;
    double movie_coll = 0.0;
    float movie_rating = 0.0f;

    Movies(String movie_Name, double movie_coll, float movie_rating){
        this.movie_Name = movie_Name;
        this.movie_coll = movie_coll;
        this.movie_rating = movie_rating;

    }

    public String toString(){
        return "{ " + movie_Name + " : " + movie_coll +" : " + movie_rating + " }";
    }
}

class SortByName implements Comparator{

    public int compare(Object obj1, Object obj2){
        return ((Movies)obj1).movie_Name.compareTo(((Movies)obj2).movie_Name);
    }
}

class SortByColl implements Comparator{

    public int compare(Object obj1, Object obj2){

        return (int)((((Movies)obj1).movie_coll) - ((Movies)obj2).movie_coll);
    }
}

class SortByRating implements Comparator{
    public int compare(Object obj1, Object obj2){

        return ((int)((((Movies)obj1).movie_rating) - ((Movies)obj2).movie_rating));
        
    }
}

class Demo{
    public static void main(String[] args) {
        
        ArrayList al = new ArrayList();

        al.add(new Movies("Gadar2", 233.220, 8.9f));
        al.add(new Movies("OMG2", 268.10, 8.4f));
        al.add(new Movies("KGF2", 633.220, 9.6f));
        al.add(new Movies("Tanhaji", 433.220, 9.4f));
        al.add(new Movies("Ved", 133.220, 8.7f));

        System.out.println(al);

        // for sorting use Collections.sort() method

        Collections.sort(al,new SortByName()); // sort by name

        System.out.println(al);


        Collections.sort(al, new SortByColl()); // sort by Coll

        System.out.println(al);

        Collections.sort(al, new SortByRating()); // sort by Rating

        System.out.println(al);

    }
}