
// Concept of HashSet in collection
/* HashSet(class) is child of Set(Interface)
 * where each element is unique in set , No duplicates are allowed
 */

 import java.util.*;

 class HashSetDemo{
    public static void main(String[] args) {
        
        // use hashset
        HashSet<Integer>  hs = new HashSet<>();

        // Adding elements to HashSet

        hs.add(10);
        hs.add(20);
        hs.add(20);
        hs.add(40);
        hs.add(50);


        System.out.println(" HashSet elements are : "+hs);
    }
 }