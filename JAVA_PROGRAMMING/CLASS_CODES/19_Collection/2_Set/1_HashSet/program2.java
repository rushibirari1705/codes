

// Concept of HashSet in collection
/* HashSet(class) is child of Set(Interface)
 * where each element is unique in set , No duplicates are allowed
 */

 import java.util.*;

 class HashSetDemo{
    public static void main(String[] args) {
        
        // use hashset
        HashSet<String>  hs = new HashSet<>();

        // Adding elements to HashSet

        hs.add("Kanha");
        hs.add("Rahul");
        hs.add("Ashish");
        hs.add("Badhe");
        hs.add("Rahul");
        hs.add("Ashish");


        System.out.println(" HashSet elements are : "+hs);
    }
 }