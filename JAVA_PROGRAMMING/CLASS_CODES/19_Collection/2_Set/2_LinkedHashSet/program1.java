
/* Concept of LinkedHashSet 
 * Note : In LinkedHashSet  sequence of adding elements is maintain i.e 
 * as we insert elements in that order we get it.
 */

 import java.util.*;

 class LinkedHashSetDemo{
    public static void main(String[] args) {

        // create a new LinkedHashSet
    
        LinkedHashSet<Integer> hs = new LinkedHashSet<>();

        // adding elements to LinkedHashSet        
        hs.add(10);
        hs.add(30);
        hs.add(20);
        hs.add(10);
        hs.add(10);

        System.out.println(hs);
        
    }
 }
 /* In this code the sequence of Inserting elements is maintain by LinkedHashSet */