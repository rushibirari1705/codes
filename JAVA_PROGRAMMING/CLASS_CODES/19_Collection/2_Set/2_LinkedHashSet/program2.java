/* Concept of LinkedHashSet 
 * Note : In LinkedHashSet  sequence of adding elements is maintain i.e 
 * as we insert elements in that order we get it.
 */

 import java.util.*;

 class LinkedHashSetDemo{
    public static void main(String[] args) {

        // create a new LinkedHashSet
    
        LinkedHashSet<String> hs = new LinkedHashSet<>();

        // adding elements to LinkedHashSet        
        hs.add("Kanha");
        hs.add("Rahul");
        hs.add("Ashish");
        hs.add("Badhe");
        hs.add("Rahul");
        hs.add("Ashish");

        System.out.println(hs);
        
    }
 }
 /* In this code the sequence of Inserting elements is maintain by LinkedHashSet */