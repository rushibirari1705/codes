
// User defined class & Real time example of LinkedHashSet

import java.util.*;

class CricPlayer{
    int jerNo = 0;
    String pName =  null;

    CricPlayer(int jerNo, String pName) {
        this.jerNo = jerNo;
        this.pName = pName;
    }
}

class LinkedHashSetDemo {
    public static void main(String[] args) {
        
        LinkedHashSet hs = new LinkedHashSet();

        hs.add(new CricPlayer(18, "Virat"));
        hs.add(new CricPlayer(07, "Dhoni"));
        hs.add(new CricPlayer(45, "Rohit"));
        hs.add(new CricPlayer(07, "Dhoni"));

        System.out.println(hs);
    }
}

/* Note :
 *     In this code, the add() method add elements to LinkedHashSet but they consider the addresses of objects 
 *      and all objects addresses are unique. so it gives 4 different addresses in LinkedHashSet
 */