
/*  Concept of TreeSet in java & use multiple methods
 *   TreeSet concept in Collection where TreeSet Store the objects in proper order
 *      
 * Set -> SortedSet -> NavigableSet -> TreeSet
 * 
 * TreeSet is class 
 * 
 */

 import java.util.*;
 class TreeSetDemo{
        public static void main(String[] args){
            // Create a new TreeSet

            TreeSet<Integer> ts = new TreeSet<>();

            // adding elements to the TreeSet

            ts.add(20);
            ts.add(30);
            ts.add(10);
            ts.add(10);
            ts.add(50);

            System.out.println("  TreeSet elements are : " + ts);

            // Adding elements

            ts.add(100);
            // remove elements from TreeSet

            //ts.remove(10);
            System.out.println("After adding element TreeSet is : " + ts);
        }
 }

