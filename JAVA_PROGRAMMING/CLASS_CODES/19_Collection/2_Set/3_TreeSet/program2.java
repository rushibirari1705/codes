
/*  creating a TreeSet of string & use multiple methods
 *   TreeSet concept in Collection where TreeSet Store the objects in proper order
 *      
 * Set -> SortedSet -> NavigableSet -> TreeSet
 * 
 * TreeSet is class 
 * 
 */

 import java.util.*;
 class TreeSetDemo{
        public static void main(String[] args){
            // Create a new TreeSet

            TreeSet<String> ts = new TreeSet<>();

            // adding elements to the TreeSet
            ts.add("Kanha");
            ts.add("Ashish");
            ts.add("Rahul");
            ts.add("Badhe");
            ts.add("Rushi");


            System.out.println("  TreeSet elements are : " + ts);

            ts.remove("Rushi");

             System.out.println("After removing element TreeSet is : " + ts);
        }
 }

