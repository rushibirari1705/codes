

// use comparable interface for user defined classes by template

import java.util.*;

class Movies implements Comparable<Movies>{

    String movie_Name = null;
    float movie_Coll = 0.0f;

    Movies(String movie_Name, float movie_Coll){
        this.movie_Name = movie_Name;
        this.movie_Coll = movie_Coll;

    }

    public int compareTo(Movies obj){

        return (movie_Name.compareTo(((Movies)obj).movie_Name));

    }

    public String toString(){
        return movie_Name ;
    }

}

class TreeSetDemo{
    public static void main(String[] args) {
        
        TreeSet<Movies> ts = new TreeSet<>();

        ts.add(new Movies("Gadar2", 135.56f ));
        ts.add(new Movies("OMG2", 150.00f ));
        ts.add(new Movies("Ved ", 234.55f ));
        ts.add(new Movies("KGF2", 678.56f ));

        System.out.println(ts); // by default print in ascending order

    }
}
