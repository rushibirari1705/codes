
// Concept of NavigableSet  & allMethods of NavigableSet

import java.util.*;

class NavigableSetDemo{
    public static void main(String[] args) {

        // NavigableSet creation
        
        NavigableSet<String> ns = new TreeSet<>();

        // add objects to set
        ns.add("Rushi");
        ns.add("Pavan");
        ns.add("Vinit");
        ns.add("Nikhil");
        ns.add("Ganesh");

        // print set
        System.out.println(ns); //[Ganesh, Nikhil, Pavan, Rushi, Vinit]

        //lower() : gives lower element of given element
        System.out.println(ns.lower("Rushi")); //Pavan

        //higher : gives higher element of given element
        System.out.println(ns.higher("Vinit")); //null

        //floor() :  gives floor value of given element
        System.out.println(ns.floor("Vinit")); // Vinit

        // ceiling() : gives ceiling value of given element
        System.out.println(ns.ceiling("rushi"));

        //pollFirst() : gives
        System.out.println(ns.pollFirst());


        //pollLast() : gives
          System.out.println(ns.pollLast());
    }
}