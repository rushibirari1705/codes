
// Concept of NavigableSet in Java Collection

import java.util.*;

class NavigableSetDemo{
    public static void main(String[] args) {

        // NavigableSet creation
        
        NavigableSet<String> ns = new TreeSet<>();

        // add objects to set
        ns.add("Rushi");
        ns.add("Pavan");
        ns.add("Vinit");
        ns.add("Nikhil");
        ns.add("Ganesh");

        // print set
        System.out.println(ns);
    }
}