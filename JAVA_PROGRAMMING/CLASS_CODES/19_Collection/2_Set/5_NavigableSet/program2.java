// Concept of NavigableSet  & use lower() & higher() Methods of NavigableSet for string

import java.util.*;

class NavigableSetDemo{
    public static void main(String[] args) {

        // NavigableSet creation
        
        NavigableSet<String> ns = new TreeSet<>();

        // add objects to set
        ns.add("Rushi");
        ns.add("Pavan");
        ns.add("Vinit");
        ns.add("Nikhil");
        ns.add("Ganesh");

        // print set
        System.out.println(ns); //[Ganesh, Nikhil, Pavan, Rushi, Vinit]

        //lower() : gives lower element of given element
        System.out.println(ns.lower("Rushi")); //Pavan

        //higher : gives higher element of given element
        System.out.println(ns.higher("Vinit")); //null
    }
}