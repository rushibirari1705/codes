
// Concept of NavigableSet  & use lower() & higher() Methods of NavigableSet for Integer

import java.util.*;

class NavigableSetDemo{
    public static void main(String[] args) {

        // NavigableSet creation
        
        NavigableSet<Integer> ns = new TreeSet<>();

        // add objects to set
        ns.add(10);
        ns.add(15);
        ns.add(20);
        ns.add(300);
        ns.add(101);
        // print set
        System.out.println(ns); //[10, 15, 20, 101, 300]

        //lower() : gives lower element of given element
        System.out.println(ns.lower(10)); //null

        //higher : gives higher element of given element
       System.out.println(ns.higher(11)); //15
    }
}