
//Concept of HashMap & HashMap all Methods

import java.util.*;
class HashMapDemo {
    public static void main(String[] args) {

        // HashMap creation
        HashMap<Integer , String> hm = new HashMap<>();
        hm.put(101,"Vinit");
        hm.put(105, "Rushi");
        hm.put(103, "Pavan");
        hm.put(104, "Nikhil");
        hm.put(102,"Shubham");

        System.out.println(hm); //{101=Vinit, 102=Shubham, 103=Pavan, 104=Nikhil, 105=Rushi}

        //All Methods
        // get() : gives value of given key
        System.out.println(hm.get(105)); // Rushi
        System.out.println(hm.get(110)); // null : key is not present
        
        
        // keySet() : gives Set of all keys presnt in Map
        System.out.println(hm.keySet()); //[101, 102, 103, 104, 105]

        // values() : gives set of values present in Map
        System.out.println(hm.values());//[Vinit, Shubham, Pavan, Nikhil, Rushi]

        // entrySet() : gives set of entries present in Map
        System.out.println(hm.entrySet()); //[101=Vinit, 102=Shubham, 103=Pavan, 104=Nikhil, 105=Rushi]

        //remove() : remove entry from Map of given key
        System.out.println(hm.remove(102));

        System.out.println(hm); 

    }
}