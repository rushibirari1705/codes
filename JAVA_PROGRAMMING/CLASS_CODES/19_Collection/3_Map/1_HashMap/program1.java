
// concept of HashSet & HashMap in Java Collection

import java.util.*;

class HashMapDemo {
    public static void main(String[] args) {
        
        //HashSet creation

        HashSet<String> hs = new HashSet<>();

        // add elements to set
        hs.add("Kanha");
        hs.add("Ashish");
        hs.add("Badhe");
        hs.add("Rahul");

        System.out.println(hs);// [Rahul, Ashish, Badhe, Kanha]

        // HashMap creation
        HashMap<String, String> hm = new HashMap<>();

        hm.put("Kanha", "Infosys");
        hm.put("Ashish", "Barclays");
        hm.put("Badhe", "CarPro");
        hm.put("Rahul","BMC");

        System.out.println(hm);//{Rahul=BMC, Ashish=Barclays, Badhe=CarPro, Kanha=Infosys}

    }
}

/*  Note :
 *          Observed the both output for HashSet & HashMap where sequence is same for both i.e
 *           HashSet internally use HashMap by using Hashing function
 */
