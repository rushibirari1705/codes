
// Simple Concept of HashMap in java Collection

import java.util.*;
class HashMapDemo{
    public static void main(String[] args) {
        
        // create a new HashMap
        HashMap<String ,String > hm = new HashMap<>();

        // put elements into the Map
        hm.put("Kanha", "Infosys");
        hm.put("Ashish", "Barclays");
        hm.put("Badhe", "CarPro");
        hm.put("Rahul","BMC");

        System.out.println(hm);//{Rahul=BMC, Ashish=Barclays, Badhe=CarPro, Kanha=Infosys}

    }
}

/*Note :
 *      In HashMap uses Key- Value pair where key is unique and value may or may not be same
 */