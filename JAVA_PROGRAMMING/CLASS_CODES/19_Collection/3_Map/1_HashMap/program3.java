

// Concept of HashMap for Duplicate key in java Collection

import java.util.*;
class HashMapDemo{
    public static void main(String[] args) {
        
        // create a new HashMap
        HashMap<String ,String > hm = new HashMap<>();

        // put elements into the Map
        hm.put("Kanha", "Infosys");
        hm.put("Ashish", "Barclays");
        hm.put("Badhe", "CarPro");
        hm.put("Rahul","BMC");
        hm.put("Kanha", "Nvidia");

        System.out.println(hm);//{Rahul=BMC, Ashish=Barclays, Badhe=CarPro, Kanha=Nvidia}

    }
}

/*Note : 
 * In this scenario key is duplicate then the value of key will be replaced by newnone value
 * {Rahul=BMC, Ashish=Barclays, Badhe=CarPro, Kanha=Nvidia}
 * here, kanha=CarPro  then Kanha=Nvidia 
 */
