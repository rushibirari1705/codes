
// How to Find hashCode() of given object

class HashCodeDemo{
    public static void main(String[] args) {
        
        String str1 = "RAM";
        String str2 = "SAM";
        String str3 = "KAM";

        System.out.println(str1.hashCode()); //80894
        System.out.println(str2.hashCode()); //81855
        System.out.println(str3.hashCode()); //74167
    }
}

/*Note :
 *   How to find hashCode :
 * 
 *              RAM : s[ R A M]
 *                     s[2 1 0]
 * 
 *      HashCode = s[2] * 31^2 + s[1] * 31^1 + s[0] * 31^0
 *               = s[1] * 31^2 + s[1] * 31 + s[0] * 1
 *               = 82 * 31 * 31 + 65 * 31 + 77 * 1
 *               = 78802 + 2015 + 77
 * 
 *               = 80894
 *                  
 */