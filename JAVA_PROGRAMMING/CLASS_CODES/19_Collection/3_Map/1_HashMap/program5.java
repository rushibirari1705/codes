
// HashMap scenarios for elements for short range Integer value
import java.util.*;
class HashMapDemo {
    public static void main(String[] args) {

        // HashMap creation
        HashMap<Integer , String> hm = new HashMap<>();
        hm.put(10,"Vinit");
        hm.put(14, "Rushi");
        hm.put(13, "Pavan");
        hm.put(12, "Nikhil");
        hm.put(11,"Shubham");

        System.out.println(hm);//{10=Vinit, 11=Shubham, 12=Nikhil, 13=Pavan, 14=Rushi}
        
        
    }
}