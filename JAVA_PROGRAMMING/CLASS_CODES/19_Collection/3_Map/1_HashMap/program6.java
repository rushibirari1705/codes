

// HashMap scenarios for elements for wide range Integer value
import java.util.*;
class HashMapDemo {
    public static void main(String[] args) {

        // HashMap creation
        HashMap<Integer , String> hm = new HashMap<>();
        hm.put(101,"Vinit");
        hm.put(105, "Rushi");
        hm.put(103, "Pavan");
        hm.put(104, "Nikhil");
        hm.put(102,"Shubham");

        System.out.println(hm);//{101=Vinit, 102=Shubham, 103=Pavan, 104=Nikhil, 105=Rushi}
        
        
    }
}