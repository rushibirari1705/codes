
// HashMap scenarios for elements for wide range Integer value
import java.util.*;
class HashMapDemo {
    public static void main(String[] args) {

        // HashMap creation
        HashMap<Integer , String> hm = new HashMap<>();
        hm.put(113,"Vinit");
        hm.put(111, "Rushi");
        hm.put(112, "Pavan");
        hm.put(109, "Nikhil");
        hm.put(110,"Shubham");
        

        System.out.println(hm); //
        
        
    }
}