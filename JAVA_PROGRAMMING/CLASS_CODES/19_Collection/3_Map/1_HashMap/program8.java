

// HashMap scenarios for elements for wide range Integer value
import java.util.*;
class HashMapDemo {
    public static void main(String[] args) {

        // HashMap creation
        HashMap<Integer , String> hm = new HashMap<>();
        hm.put(new Integer(113),"Vinit");
        hm.put(new Integer(111), "Rushi");
        hm.put(new Integer(112), "Pavan");
        hm.put(new Integer(109), "Nikhil");
        hm.put(new Integer(110),"Shubham");
        

        System.out.println(hm); //
        
        
    }
}