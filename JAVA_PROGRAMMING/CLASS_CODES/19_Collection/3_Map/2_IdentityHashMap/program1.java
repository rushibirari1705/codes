
// Concept of IdentityHashMap in Java Collection
import java.util.*;
class IdentityHashMapDemo {
    public static void main(String[] args) {

        //create a new IdentityHashMap
        IdentityHashMap<Integer, String> hm = new IdentityHashMap<>();

        hm.put(new Integer(11),"Rushi" );
        hm.put(11,"Pavan" );
        hm.put(12,"Vinit" );
        hm.put(10,"Nikhil" );

        System.out.println(hm);//{11=Pavan, 10=Nikhil, 11=Rushi, 12=Vinit}

        
    }
}

/*Note : 
 *  -This class is use to add duplicate keys to the Map so we use IdentityHashMap for storing that key-value pair
 *  - It allows us to add duplicate keys 
 */