
// Concept of IdentityHashMap by creating object using new keyword in Java Collection

import java.util.*;
class IdentityHashMapDemo {
    public static void main(String[] args) {

        //create a new IdentityHashMap
        IdentityHashMap<Integer, String> hm = new IdentityHashMap<>();

        hm.put(new Integer(11),"Rushi" );
        hm.put(new Integer(11),"Pavan" );
        hm.put(new Integer(11),"Vinit" );
        hm.put(new Integer(11),"Nikhil" );

        System.out.println(hm); //{11=Pavan, 11=Nikhil, 11=Rushi, 11=Vinit}

        
    }
}
