
// Concept of Garbage Collector in Java

class Demo{
    
    String str = null;

    Demo(String str){
        this.str = str;
    }

    //Garbage Collector method
    public void finalize(){
        System.out.println("Notify");
    }

    public String toString(){
        return str;
    }

    void fun(){
        Demo obj = new Demo("C2W");
        obj = null;
        
    }

}

class GCDemo{
    public static void main(String[] args) {
        
        //creating objects of Demo

        Demo obj1 = new Demo("Core2Web");
        Demo obj2 = new Demo("Biencaps");
        Demo obj3 = new Demo("Incubator");

        System.out.println(obj1);
        System.out.println(obj2);
        System.out.println(obj3);

        obj1 = null;
        obj2 = null;

        obj3.fun();

        System.gc();

        System.out.println("In main");
        for(int i = 0; i< 10; i++){
            System.out.println("In Main for loop");
        }

    }
}