
// Concept of Garbage Collector in WeakHashMap 

import java.util.WeakHashMap;

class Demo{
    
    String str = null;

    Demo(String str){
        this.str = str;
    }

    //Garbage Collector method
    public void finalize(){
        System.out.println("Notify");
    }

    public String toString(){
        return str;
    }

}

class GCDemo{
    public static void main(String[] args) {
        
        //creating objects of Demo

        Demo obj1 = new Demo("Core2Web");
        Demo obj2 = new Demo("Biencaps");
        Demo obj3 = new Demo("Incubator");

        System.out.println(obj1);
        System.out.println(obj2);
        System.out.println(obj3);

        WeakHashMap hm = new WeakHashMap();
        hm.put(obj1, 2016);
        hm.put(obj2, 2019);
        hm.put(obj3, 2022);

        System.out.println(hm);

        obj1 = null;

        System.gc();// Garbage collector

        System.out.println(hm);
      
    }
}