

// Concept of TreeMap in collection & perform  all methods operations


import java.util.*;

class TreeMapDemo{
    public static void main(String[] args) {
        
        // Creating a TreeMap

        TreeMap tm = new TreeMap();

        tm.put("Ind", "India");
        tm.put("Ban", "Bangladesh");
        tm.put("Can", "Canada");
        tm.put("Aus","Australia");
        tm.put("Pak", "Pakistan");

        System.out.println(tm);//{Aus=Australia, Ban=Bangladesh, Can=Canada, Ind=India, Pak=Pakistan} in sorted order
    
        // All methods

        //size():
        System.out.println(tm.size()); // 5

        //containsKey() :
        System.out.println(tm.containsKey("Ind")); // India is present then o/p is True

        //containsValue() :
        System.out.println(tm.containsValue("India")); // True

        //firstKey() :
        System.out.println(tm.firstKey());

        //lastKey() :
        System.out.println(tm.lastKey());

        //lowerKey() :
        System.out.println(tm.lowerKey("Pak"));

        //floorKey() :
        System.out.println(tm.floorKey("Chi"));

        //ceilingKey() :
        System.out.println(tm.ceilingKey("Ind"));

    
    }


}
