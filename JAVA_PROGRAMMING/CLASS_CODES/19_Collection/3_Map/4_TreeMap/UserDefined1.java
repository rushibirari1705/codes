

// TreeMap for user Defined class by Comparable interface
import java.util.*;
class Platform implements Comparable{

    String platformName = null;
    int foundationYear = 0;

    Platform(String platformName, int foundationYear){
        this.platformName = platformName;
        this.foundationYear = foundationYear;
    }

    public String toString(){
        return "{ " + platformName + " : " +foundationYear + " }";
    }
    //compare by FoundationYear
    public int compareTo(Object obj){
        return foundationYear - ((Platform)obj).foundationYear;
        
    }

}

class TreeMapDemo{
    public static void main(String[] args) {
        
        // Create a new TreeMap

        TreeMap tm = new TreeMap();

        tm.put(new Platform("Youtube", 2005),"Google");
        tm.put(new Platform("Instagram", 2010),"Meta");
        tm.put(new Platform("facebook", 2004),"Meta");
        tm.put(new Platform("ChatGPT", 2022),"OpenAI");
       // tm.put(new Platform("ChatGPT", 2022),"AI"); if this include then it replace old value with new value


         System.out.println(tm);
    }
}
