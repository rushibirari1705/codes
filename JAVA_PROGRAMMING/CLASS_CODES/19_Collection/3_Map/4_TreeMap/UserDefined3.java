
// TreeMap for user Defined class by Comparator interface in different class sort by Platform Name

import java.util.*;

class Platform {

    String platformName = null;
    int foundationYear = 0;

    Platform(String platformName, int foundationYear){
        this.platformName = platformName;
        this.foundationYear = foundationYear;
    }

    public String toString(){
        return "{ " + platformName + " : " +foundationYear + " }";
    }
    
}

// sort by FoundationYear of platform
class SortByFoundationYear implements Comparator{

    public int compare(Object obj1, Object obj2){

        return ((Platform)obj1).platformName.compareTo(((Platform)obj2).platformName);
    }

}

class TreeMapDemo{
    public static void main(String[] args) {
        
        // Create a new TreeMap

        TreeMap tm = new TreeMap( new SortByFoundationYear());

        tm.put(new Platform("Youtube", 2005),"Google");
        tm.put(new Platform("Instagram", 2010),"Meta");
        tm.put(new Platform("Facebook", 2004),"Meta");
        tm.put(new Platform("ChatGPT", 2022),"OpenAI");
       // tm.put(new Platform("ChatGPT", 2022),"AI"); if this include then it replace old value with new value


         System.out.println(tm);//{{ ChatGPT : 2022 }=OpenAI, { Facebook : 2004 }=Meta, { Instagram : 2010 }=Meta, { Youtube : 2005 }=Google}
    }
}
