
// Concept of TreeMap in collection & perform operations
// Elements are stored in sorted way

import java.util.*;

class TreeMapDemo{
    public static void main(String[] args) {
        
        // Creating a TreeMap

        TreeMap tm = new TreeMap();

        tm.put("Ind", "India");
        tm.put("Ban", "Bangladesh");
        tm.put("Can", "Canada");
        tm.put("Aus","Australia");
        tm.put("Pak", "Pakistan");

        System.out.println(tm);//{Aus=Australia, Ban=Bangladesh, Can=Canada, Ind=India, Pak=Pakistan} in sorted order
    }
}
