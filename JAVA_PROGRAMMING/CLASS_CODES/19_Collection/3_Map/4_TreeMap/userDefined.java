
// TreeMap for user Defined class by Comparable interface
import java.util.*;
class Platform implements Comparable{

    String platformName = null;
    int foundationYear = 0;

    Platform(String platformName, int foundationYear){
        this.platformName = platformName;
        this.foundationYear = foundationYear;
    }

    public String toString(){
        return "{ " + platformName + " : " +foundationYear + " }";
    }
    //compare by platformName
    public int compareTo(Object obj){
        
        return this.platformName.compareTo(((Platform)obj).platformName);
    }

}

class TreeMapDemo{
    public static void main(String[] args) {
        
        // Create a new TreeMap

        TreeMap tm = new TreeMap();

        tm.put(new Platform("Youtube", 2005),"Google");
        tm.put(new Platform("Instagram", 2010),"Meta");
        tm.put(new Platform("facebook", 2004),"Meta");
         tm.put(new Platform("ChatGPT", 2022),"OpenAI");


         System.out.println(tm);
    }
}
