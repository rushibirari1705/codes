
// SortedMap (Interface) concept and use TreeMap ( class ) and use their all methods

import java.util.*;
class SortedMapDemo {
    public static void main(String[] args) {
        
        // Create a new SortedMap
        SortedMap sm = new TreeMap();

        sm.put("Ind", "India");
        sm.put("Pak", "Pakistan");
        sm.put("Aus","Australia");
        sm.put("NZ", "New Zealand");
        sm.put("SL" ,"SriLanka");

        System.out.println(sm);//{Aus=Australia, Ind=India, NZ=New Zealand, Pak=Pakistan, SL=SriLanka}

        //subMap() : return subMap between given keys
        System.out.println(sm.subMap("Aus", "Pak")); //{Aus=Australia, Ind=India, NZ=New Zealand}
       
        //headMap() : return headMap from given key
        System.out.println(sm.headMap("Pak"));//{Aus=Australia, Ind=India, NZ=New Zealand}

        //tailMap() : return tailMap from given key
        System.out.println(sm.tailMap("NZ"));//{NZ=New Zealand, Pak=Pakistan, SL=SriLanka}

        //firstKey() : return firstKey from given keyset
        System.out.println(sm.firstKey());//Aus

        //lastKey() : return lastKey from given keyset
        System.out.println(sm.lastKey()); //SL

        //keySet() : return keySet from given keyset
        System.out.println(sm.keySet());//[Aus, Ind, NZ, Pak, SL]

        //values() : return valuesSet from given values
        System.out.println(sm.values());//[Australia, India, New Zealand, Pakistan, SriLanka]

        //entrySet() : return entrySet from given collection
        System.out.println(sm.entrySet()); //[Aus=Australia, Ind=India, NZ=New Zealand, Pak=Pakistan, SL=SriLanka]





    }
} 