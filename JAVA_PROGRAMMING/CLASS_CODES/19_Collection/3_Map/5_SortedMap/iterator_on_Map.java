

// Use iterator on Map by using iterator method 

import java.util.*;
class SortedMapDemo {
    public static void main(String[] args) {
        
        // Create a new SortedMap
        SortedMap sm = new TreeMap();

        sm.put("Ind", "India");
        sm.put("Pak", "Pakistan");
        sm.put("Aus","Australia");
        sm.put("NZ", "New Zealand");
        sm.put("SL" ,"SriLanka");

        System.out.println(sm);//{Aus=Australia, Ind=India, NZ=New Zealand, Pak=Pakistan, SL=SriLanka}

        //use iterator() to iterate over the each entry on map
    
        Set<Map.Entry> data = sm.entrySet();

        Iterator<Map.Entry> itr = data.iterator();

        while (itr.hasNext()){
           
            //System.out.println(itr.next());

            Map.Entry entry = itr.next();
            System.out.println("{ " + entry.getKey() + " : " + entry.getValue() + " }");

            //System.out.println( entry.getKey()); // gives key one by one 
        }

        //gives single entry in output
    }

} 