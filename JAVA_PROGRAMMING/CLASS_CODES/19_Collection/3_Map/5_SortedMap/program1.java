
// SortedMap (Interface) concept and use TreeMap ( class ) in java 

import java.util.*;
class SortedMapDemo {
    public static void main(String[] args) {
        
        // Create a new SortedMap
        SortedMap sm = new TreeMap();

        sm.put("Ind", "India");
        sm.put("Pak", "Pakistan");
        sm.put("Aus","Australia");
        sm.put("NZ", "New Zealand");
        sm.put("SL" ,"SriLanka");

        System.out.println(sm);//{Aus=Australia, Ind=India, NZ=New Zealand, Pak=Pakistan, SL=SriLanka}
    }
} 