
// concept of Hashtable in java collection 
/*
 * Hashtable is a child of Dictionary class which is abstract class
 * 
 */
import java.util.*;

 class HashtableDemo {

    public static void main(String[] args) {
        // creating a new Hashtable

        Hashtable ht = new Hashtable();

        ht.put(10,"Sachin");
        ht.put(18,"Virat");
        ht.put(7,"MSD");
        ht.put(1, "Rahul");
        ht.put(45, "Rohit");

        System.out.println(ht); //{10=Sachin, 7=MSD, 18=Virat, 45=Rohit, 1=Rahul}
        

    }

 }