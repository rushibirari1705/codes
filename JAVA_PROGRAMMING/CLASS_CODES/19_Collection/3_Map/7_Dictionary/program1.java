
// concept of Dictionary in java collection & use methods

import java.util.*;

 class DictionaryDemo {

    public static void main(String[] args) {
        // creating a new Hashtable

        Dictionary ht = new Hashtable();

        ht.put(10,"Sachin");
        ht.put(7,"MSD");
        ht.put(18,"Virat");
        ht.put(1, "Rahul");
        ht.put(45, "Rohit");

        System.out.println(ht); //{10=Sachin, 7=MSD, 18=Virat, 45=Rohit, 1=Rahul}

        // get() :  returns value of given key
        System.out.println(ht.get(10)); // Sachin

        // Enumeration keys() : returns all keys one by one
        Enumeration itr1 = ht.keys();
        while (itr1.hasMoreElements()){
            System.out.println(itr1.nextElement());
        }

        // Enumeration elements() : returns all elements one by one
        Enumeration itr2 = ht.elements();
        while (itr2.hasMoreElements()){
            System.out.println(itr2.nextElement());
        }
        

    }

 }