
// Concept of Properties class in java it requires compulsory key & value in String format.
// it deal with file system.

import java.util.*;
import java.io.*;

class Demo{

    public static void main(String[] args) throws IOException {

        // Properties class
        Properties obj = new Properties();

        FileInputStream fobj = new FileInputStream("friends.properties");

        obj.load(fobj); // loading properties file

        String name = obj.getProperty("Ashish");
        System.out.println(name); // Barclays (retrieved from .properties file)

        obj.setProperty("Shashi", "Core2Web");

        FileOutputStream outObj = new FileOutputStream("friends.properties");

        obj.store(outObj, "Updated by Shashi");

        // get one by one property from the properties file
        Enumeration itr1 = obj.propertyNames();

        while(itr1.hasMoreElements()){

            System.out.println(itr1.nextElement());

        }



        
    }
    
}