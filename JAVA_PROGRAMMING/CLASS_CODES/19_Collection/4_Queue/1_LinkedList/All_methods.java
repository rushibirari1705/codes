
// Create Queue using LinkedList and use various methods

import java.util.*;


class QueueDemo {
    public static void main(String[] args) {
        
        //create a queue
        Queue que = new LinkedList();

        //store objects in the queue

        que.offer("Rushi");
        que.offer("Pavan");
        que.offer("Vinit");
        que.offer("Nikhil");
        que.offer("Tejas");
        que.add("Shubham");

        System.out.println(que);//[Rushi, Pavan, Vinit, Nikhil, Tejas,Shubham]

        //Methods
        //poll() :  delete the front data
        que.poll();

        // remove() :  delete the data and throws an exception NoSuchElementException
        que.remove();

        System.out.println(que);//[Vinit, Nikhil, Tejas, Shubham]

        // peek() : retrieve the element
        System.out.println(que.peek()); // Vinit

        //elements() : retrieve the element
        System.out.println(que.element()); // Vinit

        System.out.println(que);//[Vinit, Nikhil, Tejas, Shubham]

    }
}