
// Queue concept in java collection we create a queue using linked list

import java.util.*;

class QueueDemo {
    public static void main(String[] args) {
    
     // Create a queue
     Queue que = new LinkedList();

     //add elements to queue by using offer method

     que.offer(10);
     que.offer(20);
     que.offer(50);
     que.offer(40);
     que.offer(30);
     

     System.out.println(que);//[10, 20, 50, 40, 30]

    }
}