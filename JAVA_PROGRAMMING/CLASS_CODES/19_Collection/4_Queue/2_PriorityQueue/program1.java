
// PriorityQueue concept in java collection & use PriorityQueue

import java.util.*;

class PriorityQueueDemo {
    public static void main(String[] args) {
        //creating PriorityQueue
        PriorityQueue que = new PriorityQueue();

        // add elements to queue

        que.offer(20);
        que.offer(10);
        que.offer(50);
        que.offer(30);
        que.offer(40);

        System.out.println(que); //[10, 20, 50, 30, 40] sometimes uses MIN Heap or Max Heap
    
    }
}