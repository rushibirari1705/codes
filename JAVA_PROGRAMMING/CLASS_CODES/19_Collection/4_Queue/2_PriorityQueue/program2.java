

// PriorityQueue concept in java collection & use PriorityQueue for string

import java.util.*;

class PriorityQueueDemo {
    public static void main(String[] args) {
        //creating PriorityQueue
        PriorityQueue que = new PriorityQueue();

        // add elements to queue

        que.offer("Kanha");
        que.offer("Ashish");
        que.offer("Rahul");
        que.offer("Rajesh");
        que.offer("Badhe");

        System.out.println(que); //[Ashish, Badhe, Rahul, Rajesh, Kanha] sometimes uses MIN Heap or Max Heap
    
    }
}