
// Deque[Double-ended Queue] concept in java collection and use Deque

import java.util.*;

class QueueDemo {
    public static void main(String[] args) {
        
        // creating a Deque
        Deque dq = new ArrayDeque();

        // add elements to the queue
        dq.offer(10);
        dq.add(40);
        dq.offer(20);
        dq.offer(30);
        
        System.out.println(dq);
        


    }
}