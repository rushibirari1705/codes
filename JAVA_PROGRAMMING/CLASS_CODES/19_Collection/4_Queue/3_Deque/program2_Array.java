
// Deque[Double-ended Queue] concept in java collection and use Deque Methods..

import java.util.*;

class QueueDemo {
    public static void main(String[] args) {
        
        // creating a Deque
        Deque dq = new ArrayDeque();

        // add elements to the queue
        dq.offer(10);
        dq.add(40);
        dq.offer(20);
        dq.offer(30);
        dq.offer(50);
        
        System.out.println(dq);//[10, 40, 20, 30, 50]

        //Methods
        //offerFirst() : add element to queue at First position
        dq.offerFirst(5);

        // offer last() : add element to queue at Last position

        System.out.println(dq);//[5, 10, 40, 20, 30, 50]

        //pollFirst() : remove element of First position from queue
        dq.pollFirst();

        //pollLast() : remove element of Last position from queue
        dq.pollLast();

        System.out.println(dq);//[10, 40, 20, 30]

        //peekFirst()  : retrieve element of First position from Queue
        System.out.println(dq.peekFirst());//10

        //peekLast() : retrieve element of Last position from Queue
        System.out.println(dq.peekLast());//30

        // Use iterator on Queue
        //iterator() : in ascending order

        Iterator itr1 = dq.iterator();
        while (itr1.hasNext()){
            System.out.println(itr1.next());
        }

        //descendingIterator() : in descending order
        Iterator itr2 = dq.descendingIterator();
        while (itr2.hasNext()){
            System.out.println(itr2.next());
        }
        
    }
}