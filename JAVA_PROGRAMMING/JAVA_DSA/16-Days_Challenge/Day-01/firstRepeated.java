
// GFG code:
// First Repeated element from array.
import java.util.*;
class Solution{

    int firstRepeated(int[] arr, int n) {

            // approach 2

            HashMap<Integer,Integer> hm = new HashMap<Integer,Integer>();

            int ans = n;

            for(int i = 0; i < n; i++){
                if(!hm.containsKey(arr[i]))
                    hm.put(arr[i], i);
                else if(hm.containsKey(arr[i])){
                    if(hm.get(arr[i]) < ans)
                        ans = hm.get(arr[i]);
                }

            }
            if(ans == n)
                return -1;
            return ans + 1;
        }

}

class Client{
    public static void main(String[] args) {
        
        int[] arr = new int[]{1, 5, 3, 4, 3, 5, 6};

        System.out.println(new Solution().firstRepeated(arr,arr.length));
    }
}