
// Problem 3 :
// key pair

import java.util.*;
class Solution{


    // Approach 1 : gives TLE
    // boolean keyPair (int[] nums,int x){

    //     for (int i = 0; i < nums.length; i++) {
    //         for (int j = i + 1; j < nums.length; j++) {
    //             if (nums[i] + nums[j] == x) {
    //                 return true;
    //             }
    //         }
    //     }

    //     return false;
    // }

    //approch 2:
    boolean hasArrayTwoCandidates(int arr[], int n, int x) {
        
        HashSet<Integer> h= new HashSet<>();
        for(int i = 0;i < n ; i++){
            
            if(h.contains(x - arr[i]))
                return true;

            h.add(arr[i]);
        }
        return false;
    }

}

class Client{
    public static void main(String[] args) {
        
        int[] arr = new int[]{1,2,4,3,6};

        int x = 10;

        if(new Solution().hasArrayTwoCandidates(arr,arr.length,x))
            System.out.println("YES ");
        else{
            System.out.println("NO");
        }


    }
}