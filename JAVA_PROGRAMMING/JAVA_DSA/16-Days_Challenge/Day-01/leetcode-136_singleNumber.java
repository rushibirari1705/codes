
import java.util.*;
class Solution{

    int singleNumber(int[] nums){

        if (nums.length == 1) {
            return nums[0];
        }
        Arrays.sort(nums);

        for (int i = 1; i < nums.length; i += 2) {
            if (nums[i] != nums[i - 1]) {
                return nums[i - 1];
            }
        }

        return nums[nums.length - 1];
    }

}

class Client{
    public static void main(String[] args) {
        
        int[] arr = new int[]{4,1,2,1,2};

        Solution obj = new Solution();
        System.out.println(obj.singleNumber(arr));

    }
}