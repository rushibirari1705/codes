
/*
 * problem 2 : 
        Find the smallest and second smallest element in an array
        Company: Amazon, Goldman Sachs
        Platform: GFG
        Description:

        Given an array of integers, your task is to find the smallest and second smallest
        element in the array. If smallest and second smallest do not exist, print -1.

        Example 1:
        Input :
        5
        2 4 3 5 6
        Output :
        2 3
        Explanation:
        2 and 3 are respectively the smallest
        and second smallest elements in the array.

        Example 2:
        Input :
        6
        1 2 1 3 6 7
        Output :
        1 2
        Explanation:
        1 and 2 are respectively the smallest
        and second smallest elements in the array.
 */

import java.util.*;
class Solution{

    long[] smallest_and_secondSmallest(long[] arr){
            long sm = Integer.MAX_VALUE;
            long s_sm = Integer.MAX_VALUE;
            Arrays.sort(arr);
            for(int i=0; i<arr.length - 1;i++){
                if(arr[i] != arr[i+1]){
                    sm = arr[i];
                    s_sm = arr[i+1];
                    return new long[]{sm, s_sm};
                }
            }

        return new long[]{-1};
    }

}

class Client{
    public static void main(String[] args) {
        
        long[] arr = new long[]{2,2,2,2,6};

        
        Solution sc = new Solution();

        long[] result = sc.smallest_and_secondSmallest(arr);

        for(int i = 0; i < result.length; i++)
            System.out.print(result[i] + " ");

    }
}