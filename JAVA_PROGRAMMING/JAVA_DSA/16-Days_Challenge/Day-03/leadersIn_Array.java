
    /*  Code2: Leaders in an Array
        Company : PayU, Adobe, Microsoft, Synopsys, Coditas, Hashedln, Betsol
        Platform : GFG
        Description:

    Given an array A of positive integers. Your task is to find the leaders in the
    array. An element of an array is a leader if it is greater than or equal to all the
    elements to its right side. The rightmost element is always a leader.

    Example 1:
    Input:
    n = 6
    A[] = {16,17,4,3,5,2}
    Output: 17 5 2
    Explanation: The first leader is 17 as it is greater than all the elements to its
    right.Similarly, the next leader is 5. The right most element is always a leader so it is
    also included.

    Example 2:
    Input:
    n = 5
    A[] = {1,2,3,4,0}
    Output: 4 0
    Explanation: 0 is the rightmost element and 4 is the only element which is
    greate than all the elements to its right.

    Expected Time Complexity: O(n)
    Expected Auxiliary Space: O(n)

    Constraints:
    1 <= n <= 107
    0 <= Ai <= 107 */

import java.util.*;

class Solution {
        static ArrayList<Integer> leaders(int arr[], int n){
            // approach test cases fail
            // ArrayList<Integer> al = new ArrayList<>();

            // int i,j,k;
            // for(i = 0; i < n; i++){
            //     k = arr[i];       
            //     for(j = i+1; j < n; j++){
            //         if(arr[j] >= k)   
            //             break; 
            //     }
            //     if(j == n)
            //         al.add(k);
            // }
            // return al;

            ArrayList<Integer> al = new ArrayList<>();
            int max = arr[n-1];
            for(int i = n-1; i >= 0;i--)
            {
                if(arr[i] >= max){
                    max = arr[i];
                    al.add(max);
                }
            }
            
            Collections.reverse(al);
            return al;
            
        }
}

class Client{
    public static void main(String[] args) {
        int[] arr = new int[]{16,17,4,3,5,2};

        ArrayList<Integer> leaders = Solution.leaders(arr, arr.length);
        System.out.println(leaders);
    }
}