/*
    Code 1: Majority Element

        Company: Flipkart, Accolite, Amazon, Microsoft, D-E-Shaw, Google, Nagarro, Atlassian
        
        Platform : Leetcode - 169, GFG

        Fraz’s & striver’s SDE sheet.
        Description

        Given an array nums of size n, return the majority element.
        The majority element is the element that appears more than [n / 2⌋ times. You
        may assume that the majority element always exists in the array.
        
        Example 1:
        Input: nums = [3,2,3]
        Output: 3

        Example 2:
        Input: nums = [2,2,1,1,1,2,2]
        Output: 2

        Constraints:
        n == nums.length
        1 <= n <= 5 * 104
        -109 <= nums[i] <= 109
 */

class Solution{
    int majorityElement(int[] arr){

        int maxCount = 0;
        int n = 0;
        for(int i = 0; i < arr.length; i++){
            int cnt = 0;
            for(int j = 0; j <arr.length; j++){
                if(arr[j] == arr[i])
                    cnt++;
            }
            if(cnt > maxCount){
                maxCount = cnt;
                n = arr[i];
            }
            
        }

        if(maxCount <= arr.length/2)
            return -1;
        else
            return arr[n];

    }
}

class Client{
    public static void main(String[] args) {
        
        int[] nums = new int[]{2,2,1,1,1,2,2};

        Solution s1 = new Solution();

        int ret  = s1.majorityElement(nums);
        System.out.println(ret);

    }
}
