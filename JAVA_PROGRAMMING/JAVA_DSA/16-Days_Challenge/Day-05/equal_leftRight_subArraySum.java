

/*
 * Code2 : Equal Left and Right Subarray Sum
 * 
    Company : Amazon, Adobe
    Platform : GFG
    Description :
    Given an array A of n positive numbers. The task is to find the first index in the
    array such that the sum of elements before it equals the sum of elements after it.
    Note: Array is 1-based indexed.
    Example 1:
    Input:
    n = 5
    A[] = {1,3,5,2,2}
    Output: 3
    Explanation: For second test case at position 3 elements before it (1+3) =
    elements after it (2+2).

 */

class Solution {
    static int equalSum(int[] A, int N){
        // int flag = 0;

        // for(int i=0; i<n; i++){
        //     int rSum = 0;
        //     int lSum = 0;

        //     for(int j = 0; j < i; j++){
        //         lSum += arr[j];
        //     }
        //     for(int k = i + 1 ; k < n; k++){
        //         rSum += arr[k];
        //     }

        //     if(rSum == lSum){
        //         flag = 1;
        //         return i + 1;
        //     }
        // }
        // return -1;

        int sum = 0;
        for(int i=0; i<N; i++){
            sum = sum+A[i];
        }
        for(int i=0; i<N; i++){
            int start = 0;
            sum = sum-A[i];
            if(start==sum){
                return i+1;
            }
            else{
                start = start+A[i];
                sum = sum-A[i];
            }
        }
        return -1;
    }
}
class Client{
    public static void main(String[] args) {

        int[] arr = new int[]{1,3,5,2,2};

        System.out.println(Solution.equalSum(arr,arr.length));
        
    }
}