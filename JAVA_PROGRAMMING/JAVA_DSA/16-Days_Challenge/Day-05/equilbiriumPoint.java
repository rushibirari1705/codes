
/*
 * CopyrightCode3 : Equilibrium Point
    Company : Amazon, Adobe
    Platform : GFG
    Description :
    Given an array A of n non negative numbers. The task is to find the first
    equilibrium point in an array. Equilibrium point in an array is an index (or position) such
    that the sum of all elements before that index is the same as the sum of elements after
    it.
    Note: Return equilibrium point in 1-based indexing. Return -1 if no such point exists.
    Example 1:
    Input:
    n = 5
    A[] = {1,3,5,2,2}
    Output: 3
    Explanation: The equilibrium point is at position 3 as the sum of elements
    before it (1+3) = sum of elements after it (2+2)

 */


class Solution {
    public static int equilibriumPoint(long A[], int N) {
        
        long sum = 0;
        for(int i = 0; i < N; i++){
            sum = sum + A[i];
        }
        for(int i = 0; i < N; i++){
            long start = 0;
            sum = sum - A[i];
            if(start == sum){
                return i+1;
            }
            else{
                start = start + A[i];
                sum = sum - A[i];
            }
        }
        return -1;
    }
}
class Client{
    public static void main(String[] args) {

        long[] arr = new long[]{1,3,5,2,2};

        System.out.println(Solution.equilibriumPoint(arr,arr.length));
        
    }
}