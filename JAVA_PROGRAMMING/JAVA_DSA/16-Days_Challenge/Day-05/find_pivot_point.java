
/*
 * Code 1: Find Pivot Index
    Company: Amazon, Adobe
    Platform: Leetcode - 724
    Given an array of integers nums, calculate the pivot index of this array.
    The pivot index is the index where the sum of all the numbers strictly to the left of the
    index is equal to the sum of all the numbers strictly to the index's right.
    If the index is on the left edge of the array, then the left sum is 0 because there are no
    elements to the left. This also applies to the right edge of the array.
    Return the leftmost pivot index. If no such index exists, return -1.

    Example 1:
    Input: nums = [1,7,3,6,5,6]
    Output: 3
    Explanation:
    The pivot index is 3.
    Left sum = nums[0] + nums[1] + nums[2] = 1 + 7 + 3 = 11
    Right sum = nums[4] + nums[5] = 5 + 6 = 11
 */

class Solution {
    static int pivotIndex(int[] arr) {
        int flag = 0;

        for(int i=0; i< arr.length; i++){
            int rSum = 0;
            int lSum = 0;

            for(int j = 0; j < i; j++){
                lSum += arr[j];
            }
            for(int k = i + 1 ; k < arr.length; k++){
                rSum += arr[k];
            }

            if(rSum == lSum){
                flag = 1;
                return i + 1;
            }
        }
        return -1;
        
        //approach 2
        // int lsum=0,total=0;
        // for(int i:nums)
        //     total=total+i;
        
        // for(int j = 0;j < nums.length; lsum += nums[j++]){
        //     if(lsum * 2 == total-nums[j])
        //         return j;

        // }  
        // return -1; 
    }
}

class Client{
    public static void main(String[] args) {
        int[] arr = new int[]{1,7,3,6,5,6};
        
        System.out.println(Solution.pivotIndex(arr));

    }
}