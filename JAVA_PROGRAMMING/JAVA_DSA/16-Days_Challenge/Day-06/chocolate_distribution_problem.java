
/*
    Code3 : Chocolate Distribution problem

        Company : Flipkart
        Platform : GFG
        Love Bubbars’s SDE sheet

        Description :
        Given an array A[ ] of positive integers of size N, where each value represents
        the number of chocolates in a packet. Each packet can have a variable number of
        chocolates. There are M students, the task is to distribute chocolate packets among M
        students such that :
        1. Each student gets exactly one packet.
        2. The difference between maximum number of chocolates given to a student and
        minimum number of chocolates given to a student is minimum.
        
        Example 1:
        Input:
        N = 8, M = 5
        A = {3, 4, 1, 9, 56, 7, 9, 12}
        Output: 6
        Explanation: The minimum difference between maximum chocolates and
        minimum chocolates is 9 - 3 = 6 by choosing the following M packets :{3, 4, 9, 7,9}.

        Example 2:
        Input:
        N = 7, M = 3
        A = {7, 3, 2, 4, 9, 12, 56}
        Output: 2
        Explanation: The minimum difference between maximum chocolates and
        minimum chocolates is 4 - 2 = 2 by choosing the following M packets :{3, 2, 4}.
 */

import java.util.*;

class Solution{
    static long findMinDiff (ArrayList<Integer> al, int n, int m){
        // Collections.sort(a);
        // long minDiff = Integer.MAX_VALUE;

        // for( int i = 0; i + m + 1 < n; i++ ){
        //     minDiff = Math.min(a.get(i + (int)m - 1 ) - a.get(i) ,minDiff);

        // } 
        // return minDiff;

        Collections.sort(al);
        long minDiff = Integer.MAX_VALUE;
        
        Integer[] arr = new Integer[al.size()];
        arr = al.toArray(arr);

        for (int i = 0; i + m -1 < n; i++){
            int diff = arr[i + m - 1] - arr[i];
            if(diff < minDiff){
                minDiff = diff;
            }
        }
        return minDiff;
    }
}

class Client{
    public static void main(String[] args) {
        int N = 7, M = 3;
        //int[] arr = new int[]{7, 3, 2, 4, 9, 12, 56};


        ArrayList<Integer> list = new ArrayList<>();
        list.add(7);
        list.add(3);
        list.add(2);
        list.add(4);
        list.add(9);
        list.add(12);
        list.add(56);

        System.out.println(Solution.findMinDiff(list, N, M));
    }
}