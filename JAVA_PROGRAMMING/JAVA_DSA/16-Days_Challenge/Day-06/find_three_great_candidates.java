

/*
    Code2: Three Great Candidates/ Three Ninja Candidates/
    Maximum Product of Three Numbers
    Company: Flipkart, Amazon, Snapdeal
    Platform: Leetcode- 628, GFG, Coding Ninja
    Fraz’s SDE Sheet
    Description :
    The hiring team aims to find 3 candidates who are great collectively. Each
    candidate has his or her ability expressed as an integer. 3 candidates are great
    collectively if the product of their abilities is maximum. Given abilities of N candidates in
    an array arr[], find the maximum collective ability from the given pool of candidates.
    Example 1:
    Input:
    N = 5
    Arr[] = {10, 3, 5, 6, 20}
    Output: 1200
    Explanation:The multiplication of 10, 6 and 20 is 1200.

    Example 2:
    Input:
    N = 5
    Arr[] = {-10, -3, -5, -6, -20}
    Output: -90
    Explanation:
    Multiplication of -3, -5 and -6 is -90.
 */


class Solution {
    static long maxProduct(int[] arr, int n) {
        if(n<3)
            return -1;

        long min1 = Integer.MAX_VALUE;
        long min2 = Integer.MAX_VALUE;

        long max1 = Integer.MIN_VALUE;
        long max2 = Integer.MIN_VALUE;
        long max3 = Integer.MIN_VALUE;

        for (int num : arr) {
            if (num > max1) {
                max3 = max2;
                max2 = max1;
                max1 = num;
            } else if (num > max2) {
                max3 = max2;
                max2 = num;
            } else if (num > max3) {
                max3 = num;
            }

            if (num < min1) {
                min2 = min1;
                min1 = num;
            } else if (num < min2) {
                min2 = num;
            }
        }

        long product1 = max1 * max2 * max3;
        long product2 = max1 * min1 * min2;

        return Math.max(product1, product2);
        

    }
}

class Client{
    public static void main(String[] args) {
        
        int[] arr = new int[]{10,3,2};
        System.out.println(Solution.maxProduct(arr,arr.length));

    }
}
