
/*
    Code2: Element with left side smaller and right side greater
    Company: Zoho, Amazon, OYO Rooms, Intuit
    Platform: GFG
    Description :
    Given an unsorted array of size N. Find the first element in the array such that all
    of its left elements are smaller and all right elements are greater than it.
    Note: Left and right side elements can be equal to required elements. And extreme
    elements cannot be required.

    Example 1:
    Input:
    N = 4
    A[] = {4, 2, 5, 7}
    Output:
    5
    Explanation:
    Elements on left of 5 are smaller than 5
    and on right of it are greater than 5.

    Example 2:
    Input:
    N = 3
    A[] = {11, 9, 12}
    Output:
    -1
 */

class Solution{
    
    static int findElement(int arr[], int n){
        int leftLargest[] = new int[n];
        int rightSmallest[] = new int[n];
        leftLargest[0] = arr[0];
        rightSmallest[n-1] = arr[n-1];
        
        int left = arr[0];
        for (int i = 1; i<n; i++){
            if (arr[i]>left)
                left = arr[i];

            leftLargest[i] = left;
        }
        
        int small = arr[n-1];
        for (int i = n-2; i>=0; i--){
            if (arr[i]<small)
                small = arr[i];
                
            rightSmallest[i] = small;
        }
        
        for (int i = 1; i<n-1; i++){
            if (rightSmallest[i] == leftLargest[i])
                return rightSmallest[i];
        }
        return -1;
        
    }
}

class Client{
    public static void main(String[] args) {
        int[] arr = new int[]{11,9,12};

        System.out.println(Solution.findElement(arr, arr.length));
    }
}