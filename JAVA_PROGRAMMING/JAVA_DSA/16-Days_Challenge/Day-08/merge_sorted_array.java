
/*
    Code 1: Merge Sorted Array
    Company: Amazon, Samsung, LinkedIn +50 companies
    Platform: Leetcode - 88
    Fraz’s and Striver’s SDE sheet
    Description:
    You are given two integer arrays nums1 and nums2, sorted in non-decreasing order,
    and two integers m and n, representing the number of elements in nums1 and nums2
    respectively.
    Merge nums1 and nums2 into a single array sorted in non-decreasing order.
    The final sorted array should not be returned by the function, but instead be stored
    inside the array nums1. To accommodate this, nums1 has a length of m + n, where the
    first m elements denote the elements that should be merged, and the last n elements
    are set to 0 and should be ignored. nums2 has a length of n.
    Example 1:
    Input: nums1 = [1,2,3,0,0,0], m = 3, nums2 = [2,5,6], n = 3
    Output: [1,2,2,3,5,6]
    Explanation: The arrays we are merging are [1,2,3] and [2,5,6].
    The result of the merge is [1,2,2,3,5,6] with the underlined elements coming from
    nums1.
    Example 2:
    Input: nums1 = [1], m = 1, nums2 = [], n = 0
    Output: [1]
    Explanation: The arrays we are merging are [1] and [].
    The result of the merge is [1].

 */

import java.util.Arrays;

class Solution {
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int k = m;
        int i = 0;
        while(k < m + n){
            nums1[k++] = nums2[i++]; 
        }
        Arrays.sort(nums1);
        for(int j : nums1)
            System.out.print( j + " ");

        
    }
}

class Client{
    public static void main(String[] args) {
        int[] nums1 = new int[]{1,2,3,0,0,0};
        int[] nums2 = new int[]{2,5,6};

        Solution s1 = new Solution();
        s1.merge(nums1, nums1.length - nums2.length, nums2, nums2.length);

    }
}