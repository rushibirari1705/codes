
/*
 * Code 3:Buildings receiving sunlight
    Company: Amazon, Microsoft
    Given are the heights of certain Buildings which lie adjacent to each other. Sunlight
    starts falling from the left side of the buildings. If there is a building of a certain Height,
    all the buildings to the right side of it having lesser heights cannot see the sun. The task
    is to find the total number of such buildings that receive sunlight.

    Example 1:
    Input:
    N = 6
    H[] = {6, 2, 8, 4, 11, 13}
    Output:
    4
    Explanation:
    Only buildings of height 6, 8, 11 and
    13 can see the sun, hence output is 4.

 */

class Solution{
    static int longest(int[] arr,int n){
        int totalCount = 0, maxHeight = Integer.MIN_VALUE;
        
        for(int i = 0; i < n; i++ ) {
            if(arr[i] >= maxHeight) {
                maxHeight = arr[i];
                totalCount++;
            }
        }
        return totalCount;
    }
}

class Client{
    public static void main(String[] args) {
        int[] arr = new int[]{6, 2, 8, 4, 11, 13};

        System.out.println(Solution.longest(arr,arr.length));
    }
}