

/*   Code 1: Missing Number
    Company: Flipkart, Morgan, Stanley, Accolite, Amazon, Microsoft, D-E-Shaw, Ola
    Cabs, Payu, Visa, Intuit, Adobe, Cisco, Qualcomm
    Platform: LeetCode - 268, GFG, Coding Ninjas
    Striver’s SDE Sheet
    Description:
    Given an array nums containing n distinct numbers in the range [0, n], return the only
    number in the range that is missing from the array.

    Example 1:
    Input: nums = [3,0,1]
    Output: 2
    Explanation: n = 3 since there are 3 numbers, so all numbers are in the range
    [0,3]. 2 is the missing number in the range since it does not appear in nums.

    Example 2:
    Input: nums = [0,1]
    Output: 2
    Explanation: n = 2 since there are 2 numbers, so all numbers are in the range
    [0,2]. 2 is the missing number in the range since it does not appear in nums.
 */

class Solution{
    static int missingNumber(int[] nums){

        int n = nums.length;

        int totalSum = n * (n + 1) / 2; // guass theorem
        int sum = 0;

        for(int i = 0; i < n; i++){
            sum += nums[i];
        }

        return totalSum - sum;

    }
}

class Client{
    public static void main(String[] args) {
        
        int nums[] = new int[]{3,0,2};

        System.out.println(Solution.missingNumber(nums));
    }
}