
/*
    Code 2: Longest Common prefix in a Array

    Company: VMWare, Microsoft, Google
    Platform: leetcode-14

    Description:
    Write a function to find the longest common prefix string amongst an array of
    strings.
    If there is no common prefix, return an empty string "".
    Example 1:
    Input: strs = ["flower","flow","flight"]
    Output: "fl"

    Example 2:
    Input: strs = ["dog", "racecar", "car"]
    Output: ""
    Explanation: There is no common prefix among the input strings.

    Constraints:
    1 <= strs.length <= 200
    0 <= strs[i].length <= 200
    strs[i] consists of only lowercase English letters.
 */

import java.util.Arrays;

class Solution {
    static String longestCommonPrefix(String[] strs) {     
        // String ans = strs[0];
        // String sub = "";
        
        // for (int i = 1; i < strs.length; i++) {

        //     int len  = Math.min(ans.length(), strs[i].length());
        //     for (int j = 0; j < len; j++) {

        //         if (ans.charAt(j) == strs[i].charAt(j)) {
        //             sub += ans.charAt(j);
        //         }
        //         else{
        //             break;
        //         }
        //     }
        //     ans = sub;
        //     sub = "";
        // }
        // return ans;

        // using sorting
        Arrays.sort(strs);
        
        String s1 = strs[0];
        String s2 = strs[strs.length - 1];

        int index = 0;

        while (index < s1.length() && index < s2.length()) {
            if (s1.charAt(index) == s2.charAt(index)) {
                index++;
            } else {
                break;
            }
        }

        return s2.substring(0, index );
    }
}

class Client{
    public static void main(String[] args) {
        String[] str = new String[]{"flower","flow","flight"};

        System.out.println(Solution.longestCommonPrefix(str));
    }
}