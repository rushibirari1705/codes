
/*
    Code3: Product array puzzle

    Company: Flipkart, Morgan Stanley, Accolite, Amazon, Microsoft, D-E-Shaw, Intuit,
    Opera
    Platform: GFG

    Description:
    Given an array nums[] of size n, construct a Product Array P (of same size n)
    such that P[i] is equal to the product of all the elements of nums except nums[i].

    Example 1:

    Input:
    n = 5
    nums[] = {10, 3, 5, 6, 2}
    Output:
    180 600 360 300 900
    Explanation:
    For i=0, P[i] = 3*5*6*2 = 180.
    For i=1, P[i] = 10*5*6*2 = 600.
    For i=2, P[i] = 10*3*6*2 = 360.
    For i=3, P[i] = 10*3*5*2 = 300.
    For i=4, P[i] = 10*3*5*6 = 900.

    Example 2:
    Input:
        n = 2
        nums[] = {12,0}
    Output:
        0 12
 */

class Solution { 
	static long[] productExceptSelf(int nums[], int n) { 
        long[] result = new long[n];
        // long product = 1;

        // if(n == 2){
        //     result[0] = nums[1];
        //     result[1] = nums[0];
        // }else{
        //     for(int i = 0; i < nums.length; i++)
        //         product *= nums[i];

        //     for(int i = 0; i < nums.length;i++){
        //         result[i] = product / nums[i];
            
        //     }
        // }
            
        // return result;  test cases fail

        long leftval = 1;
        for(int i = 0;i < n ;i++){
            result[i] = leftval;
            leftval *= nums[i];
        }

        long rightval = 1;

        for(int i = n - 1;i >= 0; i--){
            result[i] = result[i] * rightval;
            rightval *= nums[i];
        }
        return result;
	} 
} 

class Client{
    public static void main(String[] args) {
        
        int[] arr = new int[]{10, 3, 5, 6, 2} ;

        long[] res = Solution.productExceptSelf(arr, arr.length);
        for(long i : res) 
            System.out.print(i + "  ");


    }
}

