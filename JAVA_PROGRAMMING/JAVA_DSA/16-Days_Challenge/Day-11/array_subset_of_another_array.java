
/*
    Code 3: Array Subset of another array

    Company: Accolite, Qualcomm, GE

    Platform: GFG
    Description:
    Given two arrays: a1[0..n-1] of size n and a2[0..m-1] of size m. Task is to check
    whether a2[] is a subset of a1[] or not. Both the arrays can be sorted or unsorted.
    There can be duplicate elements.

    Example 1:
    Input:
    a1[] = {11, 7, 1, 13, 21, 3, 7, 3}
    a2[] = {11, 3, 7, 1, 7}
    Output:
    Yes
    Explanation:
    a2[] is a subset of a1[]

    Example 2:
    Input:
    a1[] = {1, 2, 3, 4, 4, 5, 6}
    a2[] = {1, 2, 4}
    Output:
    Yes
    Explanation:
    a2[] is a subset of a1[]

    Example 3:
    Input:
    a1[] = {10, 5, 2, 23, 19}
    a2[] = {19, 5, 3}

    Output:
    No
    Explanation:
    a2[] is not a subset of a1[]
 */

import java.util.*;

class Solution {
    static String isSubset( long a1[], long a2[], long n, long m) {
        // HashSet<Long> hs = new HashSet<Long>();
        // for(int i = 0; i < n; i++){
        //     hs.add(a1[i]);
        // }
        // int flag = 0;
        // for(int i = 0; i < m; i++){
        //     if(hs.contains(a2[i])){
        //         flag = 1;
        //         continue;
        //     }else{
        //         flag = 0;
        //         break;
        //     }
        // }
        
        // if(flag == 1)
        //     return "Yes";
        // else
        //     return "No";  // test Cases failed

        Arrays.sort(a1);
        Arrays.sort(a2);
        int i = 0;
        int j = 0;
        int count = 0;
        while(i < m && j < n){
            if(a1[j] == a2[i]){
                count++;
                i++;   
            }
            j++;
            
        }
        
        if(count == m)
            return "Yes";
        else
            return "No";

    }
}

class Client{
    public static void main(String[] args) {
        long[] a1 = new long[]{1, 2, 3, 4, 5, 6, 7, 8};

        long[] a2 = new long[]{1,2,3,1};
        long m = a1.length;
        long n = a2.length;
        String retVal = Solution.isSubset(a1, a2,m,n);

        if(retVal == "Yes")
            System.out.println("Yes");
        else
            System.out.println("No");

    }
}