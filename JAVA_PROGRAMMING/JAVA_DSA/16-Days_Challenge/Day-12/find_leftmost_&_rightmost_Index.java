
/*
    Code 3: Left most and right most index

    Company: Amazon, Microsoft, Google

    Platform: GFG
    Description:
    Given a sorted array with possibly duplicate elements. The task is to find indexes
    of first and last occurrences of an element X in the given array.
    Note: If the element is not present in the array return {-1,-1} as pair.
    Example 1:
    Input:
    N = 9
    v[] = {1, 3, 5, 5, 5, 5, 67, 123, 125}
    X = 5
    Output:
    2 5
    Explanation: Index of first occurrence of 5 is 2 and index of last occurrence of 5
    is 5.
    Example 2:
    Input:
    N = 9
    v[] = {1, 3, 5, 5, 5, 5, 7, 123, 125}
    X = 7
    Output:
    6 6
    Can you solve the problem in expected time complexity?
    Expected Time Complexity: O(Log(N))
    Expected Auxiliary Space: O(1)
 */
class pair  {  
    long first, second;  
    public pair(long first, long second)  {  
        this.first = first;  
        this.second = second;  
    }  
}
class Solution {
    
    static pair Indexes(long arr[], long x){
        int first = -1,second = -1;
        // int i = 0;
        // int j = arr.length - 1;

        for(int k = 0; k < arr.length; k++){
            if(arr[k] == x){
                first = k;
                break;
            }
        }
        for(int k = arr.length - 1; k  >= 0; k--){
            if(arr[k] == x){
                second = k;
                break;
            }
        }
        System.out.println(first + " " + second );
        return new pair(first, second);

    }
}

class Client{
    public static void main(String[] args) {
        
        long arr[] = {1, 3, 5, 5, 5, 5, 7, 123, 125};
        long x = 7;
        Solution.Indexes(arr, x);

    }
}