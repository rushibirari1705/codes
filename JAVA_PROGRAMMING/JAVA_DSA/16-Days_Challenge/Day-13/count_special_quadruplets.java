
/*
    Code 2: Count Special Quadruplets
    
    Companies : Google, Amazon
    Platform : Letcode-1995.
    Hint:
    Given a 0-indexed integer array nums, return the number of distinct quadruplets
    (a, b, c, d) such that:
    nums[a] + nums[b] + nums[c] == nums[d], and
    a < b < c < d

    Example 1:
    Input: nums = [1,2,3,6]
    Output: 1
    Explanation: The only quadruplet that satisfies the requirement is (0, 1, 2, 3)
    because 1 + 2 + 3 == 6.
    Example 2:
    Input: nums = [3,3,6,4,5]
    Output: 0
    Explanation: There are no such quadruplets in [3,3,6,4,5].

    Example 3:
    Input: nums = [1,1,1,3,5]
    Output: 4
    Explanation: The 4 quadruplets that satisfy the requirement are:
    - (0, 1, 2, 3): 1 + 1 + 1 == 3
    - (0, 1, 3, 4): 1 + 1 + 3 == 5
    - (0, 2, 3, 4): 1 + 1 + 3 == 5
    - (1, 2, 3, 4): 1 + 1 + 3 == 5

    Constraints:
    4 <= nums.length <= 50
    1 <= nums[i] <= 100
 */

class Solution {
    static int countQuadruplets(int[] nums) {
        int cnt = 0;
        for (int i1 = 0; i1<nums.length - 3; i1++)
            for (int i2 = i1 + 1; i2 < nums.length - 2; i2++)
                for (int i3 = i2 + 1; i3 <nums.length - 1; i3++)
                    for (int i4 = i3 + 1; i4 <nums.length; i4++)
                        if (nums[i1] + nums[i2] + nums[i3] == nums[i4])
                            cnt++;
        return cnt;
        
    }
}
class Client{
    public static void main(String[] args) {
        int arr [] = {1,1,1,3,5};

        System.out.println(Solution.countQuadruplets(arr));
    }
}