
/*
    Code 1: Intersection of Two Arrays

    Company: Accolite, Amazon, Microsoft, PayPal, Rockstand
    Platform: Leetcode - 349
    Striver’s SDE sheet

    Given two integer arrays nums1 and nums2, return an array of their intersection.
    Each element in the result must be unique and you may return the result in any
    order.
    Example 1:
    Input: nums1 = [1,2,2,1], nums2 = [2,2]
    Output: [2]
    Example 2:
    Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
    Output: [9,4]
    Explanation: [4,9] is also accepted.
    Constraints:
    1 <= nums1.length, nums2.length <= 1000
    0 <= nums1[i], nums2[i] <= 1000

 */

import java.util.*;

class Solution {
    static int[] intersection(int[] nums1, int[] nums2) {
        
        HashSet<Integer> al = new HashSet<>();

        for(int i = 0;i < nums1.length;i++){
            for(int j = 0;j < nums2.length;j++){
                if(nums1[i] == nums2[j]){
                    al.add(nums1[i]);  
                }
            }
        }

        int arr[] =new int[al.size()];
        int j = 0;         
        for(int i :al){
            arr[j] =i;
            j++;
        }
        return arr;

    }
}

class Client{
    public static void main(String[] args) {
        
        int nums1[] = {4,9,5};
        int nums2[] = {9,4,9,8,4};

        int res[] = Solution.intersection(nums1, nums2);
        for(int i : res)
            System.out.print(i + "  ");
    }
}