
/*
 * Code 2: Length Unsorted Subarray
 * 
    Company: Flipkart, Microsoft, Adobe, Make my trip
    Platform: GFG
    Given an unsorted array Arr of size N. Find the subarray Arr[s...e] such that
    sorting this subarray makes the whole array sorted.

    Example 1:
    Input:
    N = 11
    Arr[] ={10,12,20,30,25,40,32,31,35,50,60}
    Output: 3 8
    Explanation: Subarray starting from index
    3 and ending at index 8 is required
    subarray. 
    Initial array: 10 12 20 30 25 40 32 31 35 50 60 
    Final array: 10 12 20 25 30 31 32 35 40 50 60
    (After sorting the bold part)

    Example 2:
    Input:
    N = 9
    Arr[] = {0, 1, 15, 25, 6, 7, 30, 40, 50}
    Output: 2 5
    Explanation: Subarray starting from index
    2 and ending at index 5 is required
    Subarray.
    Initial array: 0 1 15 25 6 7 30 40 50
    Final array: 0 1 6 7 15 25 30 40 50
    (After sorting the bold part)
    Expected Time Complexity: O(N)
    Expected Auxiliary Space: O(1)
    Constraints:
    1 ≤ N ≤ 107
    1 ≤ Arr[i] ≤ 108
 */

import java.util.Arrays;

class Solution {
    static int[] printUnsorted(int[] arr, int n) {
        
            int[] a = new int[n];
            int[] b = new int[2];

            for(int i = 0;i < n;i++){
                a[i] = arr[i];
            }
            Arrays.sort(a);
            for(int i = 0;i < n; i++){
                if(a[i] != arr[i]){
                    b[0] = i;
                    break;
                }
            }
            for(int i = n-1; i >= 0; i--){
                if(a[i] != arr[i]){
                    b[1] = i;
                    break;
                }
            }
            return b;
        
    }
}

class Client{
    public static void main(String[] args) {
        int[] arr= {10,12,20,30,25,40,32,31,35,50,60};

        int[] res = Solution.printUnsorted(arr, arr.length);
        for(int i : res)
            System.out.print(i + " ");
    }
}
