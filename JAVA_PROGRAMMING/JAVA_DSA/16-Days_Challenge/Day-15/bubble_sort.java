/*
 * Code 2 : Bubble Sort

        Company: Microsoft, Wipro, SAP Labs, Cisco, Nagarro, redBus, Accenture,
        Huawei
        Platform: GFG
        Given an Integer N and a list arr. Sort the array using bubble sort algorithm.
        Example 1:
        Input:
        N = 5
        arr[] = {4, 1, 3, 9, 7}
        Output:
        1 3 4 7 9
        Example 2:
        Input:
        N = 10
        arr[] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1}
        Output:
        1 2 3 4 5 6 7 8 9 10
        Expected Time Complexity: O(N^2).
        Expected Auxiliary Space: O(1).
 */

class Solution{

    static void bubbleSort(int arr[], int n){

        boolean swapped = true;
        for(int i = 0; i < n - 1; i++){
            for(int j = 0; j < n - 1 - i; j++){
                if(arr[j] > arr[j+1]){
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;

                    swapped = false;
                }
            }
            if(swapped){
                break;
            }
        }

        for(int i : arr)
            System.out.print( i + "  ");
    }
}

class Client{
    public static void main(String[] args) {
        int[] arr = { 4,3,1,9,7};

        Solution.bubbleSort(arr, arr.length);

    }
}