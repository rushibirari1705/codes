
/*
 * Code 1: Pascal's Triangle
    Company: Amazon, Microsoft, Adobe +6 companies
    Platform: Leetcode - 118
    Fraz’s SDE sheet
    Given an integer numRows, return the first numRows of Pascal's triangle.
    In Pascal's triangle, each number is the sum of the two numbers directly above it
    as shown:

    Example 1:
    Input: numRows = 5
    Output: [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]
    Example 2:
    Input: numRows = 1
    Output: [[1]]
    Constraints:
    1 <= numRows <= 30
 */

import java.util.ArrayList;
import java.util.List;

class Solution {
    static List<List<Integer>> generate(int numRows) {
        
        List<List<Integer>> p = new ArrayList<>();
        
        for (int i = 0; i < numRows; i++) {
            List<Integer> list = new ArrayList<>();
            
                for (int j = 0; j <= i; j++) {
                if (j == 0 || j == i) {
                    list.add(1);
                } else {
                    list.add(p.get(i - 1).get(j - 1) + p.get(i - 1).get(j));
                }
            }
            p.add(list);
        }
        return p;
    }
}

class Client{
    public static void main(String[] args) {
        int rows = 5;

        List<List<Integer>> res = Solution.generate(rows);
        for(List<Integer> i : res) {
            System.out.println(i + " ");
        }
    }
}