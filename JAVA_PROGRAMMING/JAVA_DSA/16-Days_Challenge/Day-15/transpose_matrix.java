/*
 * Code 3: Transpose of Matrix
    Company: MakeMyTrip, InfoEdge, Bloomberg
    Platform: GFG
    Write a program to find the transpose of a square matrix of size N*N. Transpose of a matrix is
    obtained by changing rows to columns and columns to rows.
    Example 1:
    Input:
    N = 4
    mat[][] = {{1, 1, 1, 1},
    {2, 2, 2, 2}
    {3, 3, 3, 3}
    {4, 4, 4, 4}}
    Output:
        {{1, 2, 3, 4},
        {1, 2, 3, 4}
        {1, 2, 3, 4}
        {1, 2, 3, 4}}
    Example 2:
    Input:
    N = 2
    mat[][] = {{1, 2},
    {-9, -2}}
    Output:
    {{1, -9},
    {2, -2}}

    Expected Time Complexity: O(N * N)
    Expected Auxiliary Space: O(1)

    Constraints:
    1 <= N <= 103
    -109 <= mat[i][j] <= 109
 */

class Solution{
    static void transpose(int n,int a[][]){
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                
                int temp = a[i][j];
                a[i][j] = a[j][i];
                a[j][i] = temp;
            }
        }
        
        for(int i = 0; i < a.length; i++) {
            for(int j = 0; j < a[i].length; j++){
                System.out.print( " " + a[i][j] );
            }
            System.out.println();
        }
    }
}

class Client{
    public static void main(String[] args) {
        int arr[][] = { {1, 1, 1, 1},
                        {2, 2, 2, 2},
                        {3, 3, 3, 3},
                        {4, 4, 4, 4}};

        Solution.transpose(arr.length, arr);
    }
}