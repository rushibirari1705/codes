/*
 * Code 2: Binary Search

    Company: Infosys,Oracle,Wipro,Linkedin,Qualcomm,TCS,Cognizant, Accenture
    Platform: GFG

    Description:
    Given a sorted array of size N and an integer K, find the position(0-based
    indexing) at which K is present in the array using binary search.
    
    Example 1:
    Input:
    N = 5
    arr[] = {1 2 3 4 5}
    K = 4
    Output: 3
    Explanation: 4 appears at index 3.
    
    Example 2:
    Input:
    N = 5
    arr[] = {11 22 33 44 55}
    K = 445
    Output: -1
    Explanation: 445 is not present.
    Expected Time Complexity: O(LogN)
    Expected Auxiliary Space: O(LogN) if solving recursively and O(1) otherwise.

    Constraints:
    1 <= N <= 105
    1 <= arr[i] <= 106
    1 <= K <= 106
 */

class Solution {

    static int binarysearch(int arr[], int n, int k) {
        int ans = Search(arr, 0, n - 1, k);
        return ans;

    }
    // by using recursion
    public static int Search(int arr[], int start, int end, int k) {

        if (start <= end) {

            int mid = (start + end) / 2;

            if (arr[mid] == k)
                return mid;

            else if (arr[mid] < k)
                return Search(arr, mid + 1, end, k);

            else
                return Search(arr, start, mid - 1, k);
        }

        return -1;

    }

}

class Client{
    public static void main(String[] args) {
        
        int[] arr = { 1,2,3,4,5};

        int k = 4;

        System.out.println(Solution.binarysearch(arr,arr.length, k));


    }
}