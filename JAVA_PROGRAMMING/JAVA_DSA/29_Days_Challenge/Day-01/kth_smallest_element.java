
/*
 * Code 2: Kth smallest element
 * 
    Company: VMWare, Accolite, Amazon, Microsoft, Snapdeal, Hike, Adobe,
    Google, ABCO, Cisco
    Platform: GFG
    Bubbar’s DSA sheet.
    
    Description:
    Given an array arr[] and an integer K where K is smaller than size of array,
    the task is to find the Kth smallest element in the given array. It is given
    that all array elements are distinct.
    *Note :- l and r denotes the starting and ending index of the array.
    
    Example 1:
    Input:
    N = 6
    arr[] = 7 10 4 3 20 15
    K = 3
    L=0 R=5
    Output : 7
    Explanation :
    3rd smallest element in the given
    array is 7.

    Example 2:
    Input:
    N = 5
    arr[] = 7 10 4 20 15
    K = 4 L=0 R=4
    Output : 15
    Explanation :
    4th smallest element in the given
    array is 15.
 */

import java.util.Arrays;

class Solution{
    public static int kthSmallest(int[] arr, int l, int r, int k) { 
        
        Arrays.sort(arr);

        return arr[k-1];
    } 
}

class Client{
    public static void main(String[] args) {
        
        int[] arr = {7, 10, 4 ,3 ,20, 15};
        int k = 3;
        int l = 0, r = 5;

        System.out.println(Solution.kthSmallest(arr,l,r, k));

    }
}