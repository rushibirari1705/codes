
/*
 *  Code 1: Sort Colors / Sort an array of 0s, 1s and 2s
    Company: Paytm, Flipkart, Morgan Stanley, Amazon, Microsoft, OYO Rooms,
    Samsung, Snapdeal, Hike, MakeMyTrip, Ola Cabs, Walmart, MAQ Software,
    Adobe, Yatra.com, SAP Labs, Qualcomm
    Platform: Leetcode - 75, GFG
    Striver’s and Love Babbar’s DSA sheet.
    Description:
    Given an array nums with n objects colored red, white, or blue, sort them
    in-place so that objects of the same color are adjacent, with the colors in
    the order red, white, and blue.
    We will use the integers 0, 1, and 2 to represent the color red, white, and
    blue, respectively.
    You must solve this problem without using the library's sort function.
    Example 1:
    Input: nums = [2,0,2,1,1,0]
    Output: [0,0,1,1,2,2]
    Example 2:
    Input: nums = [2,0,1]
    Output: [0,1,2]

    Constraints:
    n == nums.length
    */

import java.util.Arrays;

class Solution{
    
        static int[] sortColors(int[] nums) {
            Arrays.sort(nums);
            return nums;
        }
    
}
class Client{
    public static void main(String[] args) {
        
        int[] arr = {2,0,2,1,1,0};
        Solution.sortColors(arr);

        for(int i : arr)
            System.out.print(i + " ");

        
    }
}