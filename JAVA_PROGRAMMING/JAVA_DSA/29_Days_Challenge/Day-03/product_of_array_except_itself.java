/*
 * Code 1:Product of Array Except Self
    Company: Amazon, Facebook, Microsoft, Goldman Sachs,Qualcomm
    Platform: leetcode-238
    Fraz’s SDE sheet.

    Description:
    Given an integer array nums, return an array answer such that answer[i] is
    equal to the product of all the elements of nums except nums[i].
    The product of any prefix or suffix of nums is guaranteed to fit in a 32-bit
    integer.
    You must write an algorithm that runs in O(n) time and without using the
    division operation.
    Example 1:
    Input: nums = [1,2,3,4]
    Output: [24,12,8,6]
    Example 2:
    Input: nums = [-1,1,0,-3,3]
    Output: [0,0,9,0,0]

    Constraints:
    2 <= nums.length <= 105
    -30 <= nums[i] <= 30
    The product of any prefix or suffix of nums is guaranteed to fit in a 32-bit integer.
    */

class Solution {
    public static  int[] productExceptSelf(int[] nums) {
        int[] prefix = new int[nums.length];
        
        prefix[0] = 1;
        
        for (int i = 1;i < nums.length; i++){
            prefix[i]  = nums[i-1] * prefix[i -1];
        }

        int temp = 1;

        for (int i = nums.length-1; i>=0;  i--){
            prefix[i] = prefix[i] * temp;
            temp = temp * nums[i];
        }

    return prefix;

            
    }
}
class Client{
    public static void main(String[] args) {
        int[] arr = {1,2,3,4};   
        
        int[] res = Solution.productExceptSelf(arr) ;
        for(int i : res){
            System.out.print(i + " ");
        }
    }
}