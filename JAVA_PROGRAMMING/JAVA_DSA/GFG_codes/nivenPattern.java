package Timepass;


// class Solution{

//     static void printPattern(int rows){
//         int num = 1;
//         for(int i = 1; i <= rows; i++){

//             for(int j = 1; j <= rows; j++){
//                 int sum = 0;
//                 int temp = num;

//                 while(temp > 0){
                    
//                     sum += temp % 10;;
//                     temp /= 10;

//                 }
                
//                 if(num % sum == 0){
//                     System.out.print(num + "  ");
//                 }
//                 num++;
//             }
//             System.out.println();
//         }

//     }
//     public static void main(String[] args) {
        
//         int rows = 5;
//         printPattern(rows);
//     }
// }

import java.util.Scanner;

class Solution {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the rows for Niven number pattern: ");
        int rows = scanner.nextInt();

        System.out.println("Niven Number Pattern up to " + rows + ":");
        printNivenNumberPattern(rows);

        scanner.close();
    }

    static void printNivenNumberPattern(int rows) {
        int num = 1;
        for (int i = 1; i <= rows; i++) {

                for (int j = 1; j <= rows; j++) {
                    if(isNivenNumber(num))
                        System.out.print(num + " ");
                    
                    num++;
                }
                System.out.println();
        }
        
    }

    static boolean isNivenNumber(int num) {
        int sum = 0;
        int originalNum = num;

        while (num > 0) {
            sum += num % 10;
            num /= 10;
        }

        return (originalNum % sum) == 0;
    }
}
