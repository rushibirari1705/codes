
import java.util.*;

class Solution{
    
    static String removeDups(String s1){

        char[] arr = s1.toCharArray();
        String str = "";
        LinkedHashSet<Character> hs  = new LinkedHashSet<Character>();
        for(char c : arr){
            hs.add(c);
        }

        Iterator<Character> it = hs.iterator();

        while(it.hasNext()){
            
            str += it.next();
        }
        return str;

    }
    
    public static void main(String[] args) {
        
        String str1 = "gfgf";
        System.out.println(removeDups(str1));

    }
}