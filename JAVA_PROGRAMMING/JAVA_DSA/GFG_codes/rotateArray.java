package GFG_codes;

// Rotate Array 
import java.util.*;
class Solution{
    static void rotateArray(int arr[], int d, int n){
        ArrayList<Integer> al=new ArrayList<Integer>(n);
        d = d % n;
        for(int i=d;i<n;i++){
            al.add(arr[i]);
        }
        for(int i=0;i<d;i++){
            al.add(arr[i]);
        }
        for(int i=0;i<n;i++){
            arr[i]=al.get(i);
        }
    }

    public static void main(String[] args) {
        
        int arr[] = {1,2,3,4,5};

         Solution.rotateArray(arr,2,arr.length);

         for(int i : arr) {
            System.out.print(i + "  ");
         }
    }
}