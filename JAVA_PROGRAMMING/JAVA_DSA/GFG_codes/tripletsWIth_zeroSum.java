

import java.util.*;
class Solution{

	static boolean findTriplets(int arr[] , int n) {
        Arrays.sort(arr);
        
        for(int i=0; i<n; i++){

            int j = i + 1;
            int k = n - 1;
            while(j < k){
            
                // int sum=arr[i];
                // sum+=arr[j];
                // sum+=arr[k];

                int sum = arr[i]+arr[j]+arr[k];
                
                if(sum == 0)
                    return true;
                else if(sum>0)
                    k--;
                else
                    j++;
                
            
            }
        }
        return false;
        
    }

    public static void main(String[] args) {
        int arr[] = {0, -1, 2, -3, 1};
        if(!findTriplets(arr, arr.length))
            System.out.println( 0);
        else    
            System.out.println(1);
    }
}