//package JAVA_DSA.LeetCode;


/*
    Given a signed 32-bit integer x, return x with its digits reversed. If reversing x causes the value to go outside the signed 32-bit integer range [-231, 231 - 1], then return 0.

        Assume the environment does not allow you to store 64-bit integers (signed or unsigned).

        Example 1:

        Input: x = 123
        Output: 321
        Example 2:

        Input: x = -123
        Output: -321
        Example 3:

        Input: x = 120
        Output: 21
 */

 class Solution {

    static int reverseIntegerValue(int num){

        long reverse = 0;

        for(int i = num ; i != 0; i/=10)
            reverse = reverse * 10 + (i % 10);

        return (reverse > Integer.MAX_VALUE || reverse < Integer.MIN_VALUE) ?  0 : (int) reverse;

    }

    public static void main(String[] args) {
        
        int num = -123;

        System.out.println(Solution.reverseIntegerValue(num));
    }
 }