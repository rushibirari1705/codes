// Concat whole linkedlist to another list
import java.util.*;
class Node {
    int data;
    Node next = null;

    Node(int data){
        this.data = data;
    }
}

class LinkedList{

    Node head = null;
    Node head2 = null;
    //Node head3 = null;

    void addNode(int data){
        Node newNode = new Node(data);
        if(head == null){
            head = newNode;   
        }else{
            Node temp = head;
            while(temp.next != null){

                temp = temp.next;
            }
            temp.next = newNode;
        }
        
    }

    void addNode2(int data){
        Node newNode = new Node(data);
        if(head2 == null){
            head2 = newNode;   
        }else{
            Node temp = head2;
            while(temp.next != null){

                temp = temp.next;
            }
            temp.next = newNode;
        }
        
    }

    //print SLL
    void printSLL(){
        if(head == null){
            System.out.println("LL is empty");
        }else{
            Node temp = head;
            System.out.println();
            while(temp != null){

                System.out.print("| "+temp.data +" |"+ "-->");
                temp = temp.next;
            }
            System.out.println("| Null |");   
        }

    }
    //print SLL
    void printSLL2(){
        if(head2 == null){
            System.out.println("LL is empty");
        }else{
            Node temp = head2;
            System.out.println();
            while(temp != null){

                System.out.print("| "+temp.data +" |"+ "-->");
                temp = temp.next;
            }
            System.out.println("| Null |");   
        }

    }
    /*void print_Concat(){
        if(head3 == null){
            System.out.println("LL is empty");
        }else{
            Node temp = head3;
            System.out.println();
            while(temp != null){

                System.out.print("| "+temp.data +" |"+ "-->");
                temp = temp.next;
            }
            System.out.println("| Null |");   
        }

    }*/

    void concat_WLL(){

            Node temp2 = head;
            while(temp2.next != null){

                temp2 = temp2.next;
            }
            temp2.next = head2;
            
            System.out.println("concatenated...");
                
    }
}


class User{

    public static void main(String[] args) {
        
        LinkedList ll = new LinkedList();
        //LinkedList ll2 = new LinkedList();
        Scanner sc = new Scanner(System.in);
        boolean flag = false;
        do{

            System.out.println("********* M E N U ***********");
            System.out.println("1. AddNode LL 1 : ");
            System.out.println("2. AddNode LL 2 :");
            System.out.println("3. Print LinkedList  1:");
            System.out.println("4. Print LinkedList  2:");
            System.out.println("5. Concat Whole LL");
            System.out.println("6. Print concatenated List :");
            System.out.println("7. Exit");
            System.out.println();
            System.out.println(" Enter the Choice");
            int ch = sc.nextInt();

            switch(ch){
                case 1 :{
                            System.out.println("Enter data : ");
                            ll.addNode(sc.nextInt());
                            System.out.println("Node inserted... ");
                            
                        }
                        break;

                case 2 :{
                            System.out.println("Enter data : ");
                            ll.addNode2(sc.nextInt());
                            System.out.println("Node inserted... ");
                            
                        }
                        break;
                
                case 3 :{
                            ll.printSLL();
                            System.out.println();
                        }
                        break;
                case 4:{
                            ll.printSLL2();
                            System.out.println();
                        }
                        break;
                case 5 :{
                            ll.concat_WLL();
                        } 
                        break;
                // case 6 :{
                //             ll.print_Concat();
                //         } 
                //         break; 
                        
                case 7 : 
                        flag = true;
            }


        }while(!flag);

        sc.close();
    }

}