
// Singly Linked list
// 1 : Count Number of Nodes in LL
class Node{
    int data;
    Node next = null;

    Node(int data){
        this.data = data;
    }
}

class LinkedList{

    Node head = null;

    void addNode(int data){

        Node newNode = new Node(data);

        if(head == null)
            head = newNode;
        else{
            Node temp = head;
            while(temp.next != null){
                temp = temp.next;
            }
            temp.next = newNode;
        }
    }

    void printSLL(){
        if(head == null){
            System.out.println("LL is empty!!");
        }else{
            Node  temp = head ;
            while(temp != null){
                System.out.print(temp.data + " -> ");
                temp = temp.next;
            }
        }
    }

    int countNode(){
        int cnt = 0;
        Node temp = head;
        while(temp != null){
            cnt++;
            temp = temp.next;
        }
        return cnt;
    }


}

class User{
    public static void main(String[] args) {
        
        LinkedList ll = new LinkedList();
        ll.addNode(10);
        ll.addNode(20);
        ll.addNode(30);
        ll.addNode(50); // consider as addlast()

        ll.printSLL();
        System.out.println();

        //count Nodes
        int count = ll.countNode();
        System.out.println(" Count is : " + count);
 
    }
}