import java.util.*;

class Node{
    int data;
    Node next = null;

    Node(int data){
        this.data = data;
    }
}

class LinkedList {
    Node head = null;

    // addAt beginning of list
    void addAtBeginning(int data){
        Node newNode = new Node(data);
        if(head == null){
            head = newNode;
        }else{
            newNode.next = head;
            head = newNode;

        }
    }
    // addat end of list
    void addAtEnding(int data){
        Node newNode = new Node(data);
        if(head == null){
            head = newNode;
        }else{
            Node temp = head;
            while(temp.next != null){
                temp = temp.next;
            }
            temp.next = newNode;
        }

    }

    void printLL(){
        if(head == null){
            System.out.println("LL is empty!!");
        }else{
            Node temp = head;
            while(temp != null){
                System.out.print("| " + temp.data + " |-->");
                temp = temp.next;
            }
            System.out.println("| Null |");
        }

    }
}

class User{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        char ch;

        LinkedList ll = new LinkedList();

        do{
            System.out.println(" ******** Menu ********");

            System.out.println("Enter the data : ");
            int data = sc.nextInt();

            System.out.println("Enter indicator (0 / 1) : ");
            int indicator = sc.nextInt();


            if(indicator == 0 || indicator == 1){
                if(indicator == 0){
                    ll.addAtBeginning(data);
                    System.out.println("Node added at Beginning..");
                }else{
                    ll.addAtEnding(data);
                    System.out.println("Node added at Ending..");
                }
            }else{
                System.out.println("Wrong indicator");
            }

            System.out.println();
            System.out.println("Do u want to continue ? : ");
            ch = sc.next().charAt(0);


        }while(ch == 'y' || ch == 'Y');
        System.out.println();
        ll.printLL();

        sc.close();
        
    }
}