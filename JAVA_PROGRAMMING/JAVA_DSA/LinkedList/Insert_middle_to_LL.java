

import java.util.*;

class Node {
    int data;
    Node next =null;

    Node(int data){
        this.data = data;
    }
}

class LinkedList{

    Node addNode(Node head, int data){

        Node newNode = new Node(data);
        if(head == null){
            head = newNode;
        }else{
            Node temp = head;
            while(temp.next != null){
                temp = temp.next;
            }
            temp.next = newNode;
        }

        return head;
    }

    // Approach 1
    // Node insertAtMiddle(Node head, int data){
    //     Node newNode = new Node(data);
    //     if(head == null){
    //         head = newNode;
            
    //     }else{
    //         Node slowptr = head;
    //         Node fastptr = head;

    //         while(fastptr != null && fastptr.next != null ){
    //             slowptr = slowptr.next;
    //             fastptr = fastptr.next.next;
    //         }

    //         newNode.next = slowptr.next;
    //         slowptr.next = newNode;
    //     }

    //     return head;

    // }

    Node insertAtMiddle(Node head,int data){

        Node newNode = new Node(data);
        if(head == null){
            head = newNode;
            
        }else{
            int count = countNode(head)/2;
            //count += 1;

            Node temp = head;
            if(count % 2 == 0){
                while(count != 1 ){
                    temp = temp.next;
                    count--;

                }
            }else{
                while(count != 0 ){
                    temp = temp.next;
                    count--;

                }
            }
                newNode.next = temp.next;
                temp.next = newNode;
        }
        return head;

    }

    void printLL(Node head){
        if(head == null){
            System.out.println("LL is empty");
        }else{
            Node temp = head;
            System.out.println();
            while(temp!=null){
                System.out.print("| "+temp.data + " |-->");
                temp = temp.next;
            }
            System.out.println(" Null");
            System.out.println();
        }
    }

    int countNode(Node head){
        int cnt = 0;
        Node temp = head;

        while(temp != null){
            cnt++;
            temp = temp.next;
        }
        return cnt;
    }

    
}

class User{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Node head = null;
        boolean flag = false;

        LinkedList ll = new LinkedList();
        int ch;

        do{
            System.out.println("*******M E N U ***********");

            System.out.println("1. AddNode");
            System.out.println("2. Insert at middle :");
            System.out.println("3. Print LL: ");
            System.out.println("4. Exit");
            System.out.println();
            System.out.println("Enter Choice : ");
            ch = sc.nextInt();

            switch(ch){
                case 1 :{
                        System.out.println("Enter data : ");

                        head = ll.addNode(head,sc.nextInt());
                        System.out.println("Node added..");
                        }
                        break;
                case 2:{
                        System.out.println("Enter data : ");
                        head = ll.insertAtMiddle(head,sc.nextInt());
                        System.out.println("Node added to middle..");

                        }break;

                case 3 :{
                        ll.printLL(head);
                        }break;
                case 4 : 
                        flag = true;
                        break;
                default : 
                        System.out.println("Wrong choice!!!");

            }

        }while(!flag);

        sc.close();
    }
}