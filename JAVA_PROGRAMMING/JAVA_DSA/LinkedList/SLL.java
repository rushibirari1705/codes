package LinkedList;

// Singly Linked list


class Node{
    int data;
    Node next = null;

    Node(int data){
        this.data = data;
    }
}

class LinkedList{

    Node head = null;

    void addNode(int data){

        Node newNode = new Node(data);

        if(head == null)
            head = newNode;
        else{
            Node temp = head;
            while(temp.next != null){
                temp = temp.next;
            }
            temp.next = newNode;
        }
    }

    void addFirst(int data){
        Node newNode = new Node(data);
        if(head == null)
            head = newNode;
        else{
            newNode.next = head;
            head = newNode;
        }
    }

    void addAtpost(int data){
        int pos = 2;

        if(pos <= 0 || pos > countNode() + 1){
            System.out.println("Invalid input");
        }else if( pos == 1){
            addFirst(data);
        }else if( pos == countNode() + 1){
            addNode(data);
        }else{
            Node temp = head;
            Node newNode = new Node(data);

            while(pos - 2 != 0){
                temp = temp.next;
                pos--;

            }
            newNode.next = temp.next;
            temp.next = newNode;
        }

    }

    void printSLL(){
        if(head == null){
            System.out.println("LL is empty!!");
        }else{
            Node  temp = head ;
            while(temp != null){
                System.out.print(temp.data + " -> ");
                temp = temp.next;
            }
        }
    }

    int countNode(){
        int cnt = 0;
        Node temp = head;
        while(temp != null){
            cnt++;
            temp = temp.next;
        }
        return cnt;
    }


}

class User{
    public static void main(String[] args) {
        
        LinkedList ll = new LinkedList();
        ll.addNode(10);
        ll.addNode(20);
        ll.addNode(30);
        ll.addNode(50); // consider as addlast()

        ll.printSLL();
        System.out.println();

        ll.addFirst(5);
        ll.printSLL();
        System.out.println();

        ll.addAtpost(14);
         ll.printSLL();
        System.out.println();

        
    }
}