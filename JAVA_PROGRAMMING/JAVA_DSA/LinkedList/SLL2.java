
// Singly Linked list


class Node{
    int data;
    Node next = null;

    Node(int data){
        this.data = data;
    }
}

class LinkedList{

    Node head = null;

    void addNode(int data){

        Node newNode = new Node(data);

        if(head == null)
            head = newNode;
        else{
            Node temp = head;
            while(temp.next != null){
                temp = temp.next;
            }
            temp.next = newNode;
        }
    }

    void addFirst(int data){
        Node newNode = new Node(data);
        if(head == null)
            head = newNode;
        else{
            newNode.next = head;
            head = newNode;
        }
    }

    void addAtpos(int data){
        int pos = 2;

        if(pos <= 0 || pos > countNode() + 1){
            System.out.println("Invalid input");
        }else if( pos == 1){
            addFirst(data);
        }else if( pos == countNode() + 1){
            addNode(data);
        }else{
            Node temp = head;
            Node newNode = new Node(data);

            while(pos - 2 != 0){
                temp = temp.next;
                pos--;

            }
            newNode.next = temp.next;
            temp.next = newNode;
        }

    }

    //delete first Node:

    int deleteFirst(){
        

        if(head == null)
            System.out.println("LL is empty!!");
        if(head.next == null){
            int data = head.data;
            head = null;
            return data;
        }else{
            int data = head.data;
            head = head.next;

            return data;
        }
        
    }

    //delete last

    int deleteLast(){
        int data = 0;
        if(head == null)
            System.out.println("LL is empty!!");
        else{
            Node temp = head;
            while(temp.next.next != null){
                temp = temp.next;
            }
            data = temp.next.data;
            temp.next = null;
        }
        return data;
        
    }

    //delete at position

    int deleteAtpos(int pos){
        
        int data = 0;
        if(pos <= 0 || pos > countNode() + 1){
            System.out.println("Invalid input");
        }else if( pos == 1){
            deleteFirst();
        }else if( pos == countNode() + 1){
            deleteLast();
        }else{
            Node temp = head;
            while(pos - 2 != 0){
                temp = temp.next;
                pos--;

            }
            data = temp.next.data;
            temp.next = temp.next.next;
            
        }
        return data;

    }

    void printSLL(){
        if(head == null){
            System.out.println("LL is empty!!");
        }else{
            Node  temp = head ;
            while(temp != null){
                System.out.print(temp.data + " -> ");
                temp = temp.next;
            }
            System.out.println("NULL");
        }
    }

    int countNode(){
        int cnt = 0;
        Node temp = head;
        while(temp != null){
            cnt++;
            temp = temp.next;
        }
        return cnt;
    }


}

class User{
    public static void main(String[] args) {
        
        LinkedList ll = new LinkedList();

        // Addition Operations
        ll.addNode(10);
        ll.addNode(20);
        ll.addNode(30);
        ll.addNode(50); // consider as addlast()

        ll.printSLL();
        System.out.println();

        ll.addFirst(5);
        ll.printSLL();
        System.out.println();

        ll.addAtpos(14);
         ll.printSLL();
        System.out.println();

        //Deletion operations

        int data = ll.deleteFirst();
        System.out.println(data + " is deleted ");
        ll.printSLL();

        System.out.println();

        data = ll.deleteLast();
        System.out.println(data + " is deleted ");
        ll.printSLL();

        System.out.println();
        data = ll.deleteAtpos(2);
        System.out.println(data + " is deleted ");
        ll.printSLL();

        System.out.println();

        
    }
}