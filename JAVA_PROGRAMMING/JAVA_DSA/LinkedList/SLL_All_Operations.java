
import java.util.*;
class Node {
    int data;
    Node next = null;

    Node(int data){
        this.data = data;
    }
}

class LinkedList{

    Node head = null;

    void addNode(int data){
        Node newNode = new Node(data);
        if(head == null){
            head = newNode;   
        }else{
            Node temp = head;
            while(temp.next != null){

                temp = temp.next;
            }
            temp.next = newNode;
        }
        
    }

    // addFirst
    void addFirst(int data){
        Node newNode = new Node(data);
        if(head == null){
            head = newNode;
        }else{
            newNode.next = head;
            head = newNode;
        }
    }

    //void addAtPos
    void addAtPos(int data, int pos){

        if(pos <= 0 || pos > countNode() + 1){
            System.out.println("Invalid position");
        }else if(pos == 1){
            addFirst(data);
        }else if( pos == countNode() + 1){
            addNode(data);
        }else{
            Node temp = head;
            Node newNode = new Node(data);

            while(pos - 2 != 0){
                temp = temp.next;
                pos--;
            }
            newNode.next = temp.next;
            temp.next = newNode;
        }

    }

    //deleteLast

    void deleteLast(){
        if(head == null){
            System.out.println("LL is Empty");
        }else if(head.next == null){
            head = null;
        }else{
            Node temp = head;
            while(temp.next.next != null){
                temp = temp.next;
            }
            System.out.println(temp.next.data + " is deleted..");
            temp.next = null;
        }
    }

    //deletefirst
    void deleteFirst(){
        if(head == null){
            System.out.println("LL is empty!!");
        }else{
            System.out.println(head.data + " is deleted..");
            head = head.next;
        }
    }

    //delete at position

    void deleteAtPos(int pos){

        if(pos <= 0 || pos > countNode() + 1){
            System.out.println("Invalid position..");
        }else if(pos == 1){
            deleteFirst();
        }else if(pos == countNode()){
            deleteLast();
        }else{
            Node temp = head;
            while(pos - 2 != 0){
                temp = temp.next;
                pos--;
            }
            System.out.println(temp.next.data + " is deleted");
            temp.next = temp.next.next;
        }

    }


    //Count Nodes

    int countNode(){
        int cnt = 0;
        Node temp = head;
        while(temp != null){
            cnt++;
            temp = temp.next;
        }

        return cnt;
    }

    //print SLL
    void printSLL(){
        if(head == null){
            System.out.println("LL is empty");
        }else{
            Node temp = head;
            System.out.println();
            while(temp != null){

                System.out.print("| "+temp.data +" |"+ "-->");
                temp = temp.next;
            }
            System.out.println("| Null |");   
        }

    }
}

class User{

    public static void main(String[] args) {
        
        LinkedList ll = new LinkedList();
        Scanner sc = new Scanner(System.in);
        boolean flag = false;
        do{

            System.out.println("********* M E N U ***********");
            System.out.println("1. AddNode : ");
            System.out.println("2. AddFirst :");
            System.out.println("3. AddAtPosition :");
            System.out.println("4. DeleteLast :");
            System.out.println("5. DeleteFirst :");
            System.out.println("6. DeleteAtPosition :");
            System.out.println("7. Print LinkedList :");
            System.out.println("8. CountNode :");
            System.out.println("9. Exit :");
            System.out.println();
            System.out.println(" Enter the Choice");
            int ch = sc.nextInt();

            switch(ch){
                case 1 :{
                            System.out.println("Enter data : ");
                            ll.addNode(sc.nextInt());
                            System.out.println("Node inserted... ");
                            
                        }
                        break;
                case 2 :{
                            System.out.println("Enter data : ");
                            ll.addFirst(sc.nextInt());
                            System.out.println("Node inserted... ");

                        } 
                        break;
                case 3 :{
                            System.out.println("Enter data : ");
                            int data = sc.nextInt();
                            System.out.println("Enter position : ");
                            int pos = sc.nextInt();

                            ll.addAtPos(data,pos);
                            System.out.println("Node inserted... ");

                        }
                        break;
                case 4 :{ 
                           ll.deleteLast();

                        } 
                        break;  
                case 5 :{
                            ll.deleteFirst();

                        }
                        break;
                case 6 :{
                            System.out.println("Enter position : ");
                            ll.deleteAtPos(sc.nextInt());
                        } 
                        break; 
                case 7 :{
                            ll.printSLL();
                            System.out.println();
                        }
                        break;
                case 8 :{
                            System.out.println(ll.countNode());
                        } 
                        break; 
                        
                case 9 : 
                        flag = true;
            }


        }while(!flag);

        sc.close();
    }

}