
// Add numbers of two Linkedlist

import java.util.*;
class Node {
    int data;
    Node next = null;

    Node(int data){
        this.data = data;
    }
}

class LinkedList{

    Node head = null;
    Node head2 = null;

    void addNode(int data){
        Node newNode = new Node(data);
        if(head == null){
            head = newNode;   
        }else{
            Node temp = head;
            while(temp.next != null){

                temp = temp.next;
            }
            temp.next = newNode;
        }
        
    }

    void addNode2(int data){
        Node newNode = new Node(data);
        if(head2 == null){
            head2 = newNode;   
        }else{
            Node temp = head2;
            while(temp.next != null){

                temp = temp.next;
            }
            temp.next = newNode;
        }
        
    }

    //print SLL
    void printSLL(){
        if(head == null){
            System.out.println("LL is empty");
        }else{
            Node temp = head;
            System.out.println();
            while(temp != null){

                System.out.print("| "+temp.data +" |"+ "-->");
                temp = temp.next;
            }
            System.out.println("| Null |");   
        }

    }
    //print SLL
    void printSLL2(){
        if(head2 == null){
            System.out.println("LL is empty");
        }else{
            Node temp = head2;
            System.out.println();
            while(temp != null){

                System.out.print("| "+temp.data +" |"+ "-->");
                temp = temp.next;
            }
            System.out.println("| Null |");   
        }

    }

    void addTwoNumbers(){

        if(head == null && head2 == null){
            System.out.println("LL is empty!!!");
        }else{
            String sum1 = "";
            String sum2 = "";
            Node temp1 = head;
            Node temp2 = head2;

            while(temp1 != null){
                sum1 = temp1.data + sum1;
                temp1 = temp1.next;

            }
            while(temp2 != null){
                sum2 = temp2.data + sum2;
                temp2 = temp2.next;

            }
            int total = Integer.valueOf(sum1) + Integer.valueOf(sum2);
            while(total != 0){
                System.out.print( "  " + total % 10 + "  ");
                total /= 10;
            }
            System.out.println();
        }
        
    }



}


class User{

    public static void main(String[] args) {
        
        LinkedList ll = new LinkedList();
        Scanner sc = new Scanner(System.in);
        boolean flag = false;
        do{

            System.out.println("********* M E N U ***********");
            System.out.println("1. AddNode LL 1 : ");
            System.out.println("2. AddNode LL 2 :");
            System.out.println("3. Print LinkedList  1:");
            System.out.println("4. Print LinkedList  2:");
            System.out.println("5. Add two Numbers");
            System.out.println("6. Exit");
            System.out.println();
            System.out.println(" Enter the Choice");
            int ch = sc.nextInt();

            switch(ch){
                case 1 :{
                            System.out.println("Enter data : ");
                            ll.addNode(sc.nextInt());
                            System.out.println("Node inserted... ");
                            
                        }
                        break;

                case 2 :{
                            System.out.println("Enter data : ");
                            ll.addNode2(sc.nextInt());
                            System.out.println("Node inserted... ");
                            
                        }
                        break;
                
                case 3 :{
                            ll.printSLL();
                            System.out.println();
                        }
                        break;
                case 4:{
                            ll.printSLL2();
                            System.out.println();
                        }
                        break;
                case 5 :{
                            System.out.println();
                            ll.addTwoNumbers();
                        } 
                        break;
                        
                case 6 : 
                        flag = true;
            }


        }while(!flag);
      
        sc.close();
    }

}