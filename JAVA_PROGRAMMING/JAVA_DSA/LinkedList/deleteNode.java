
// Singly Linked list
// delete node without head pointer
import java.util.*;
class Node{
    int data;
    Node next = null;

    Node(int data){
        this.data = data;
    }
}

class LinkedList{

    

    Node addNode(Node head,int data){

        Node newNode = new Node(data);

        if(head == null)
            head = newNode;
        else{
            Node temp = head;
            while(temp.next != null){
                temp = temp.next;
            }
            temp.next = newNode;
        }
        return head;
    }

    public void deleteNode(Node node) {

        if(node.next == null){
            System.out.println("Last Node can't delete..");
            return;
        }
        
        node.data = node.next.data;
        node.next = node.next.next;

    }

    
    void printSLL(Node head){
        if(head == null){
            System.out.println("LL is empty!!");
        }else{
            Node  temp = head ;
            while(temp != null){
                System.out.print(temp.data + " -> ");
                temp = temp.next;
            }
        }
    }

}

class User{
    public static void main(String[] args) {
        Node head = null;
        LinkedList ll = new LinkedList();
        Scanner sc = new Scanner(System.in);
        head = ll.addNode(head,10);

        ll.addNode(head,20);
        ll.addNode(head,30);
        ll.addNode(head,40); // consider as addlast()

        ll.printSLL(head);
        System.out.println();
        System.out.println("Enter Node : ");
        int data = sc.nextInt();

        Node temp = head;
        while(temp != null) {
            if(temp.data == data) {
                ll.deleteNode(temp);
            }
            temp = temp.next;
        }

        ll.printSLL(head);
        System.out.println();

        
    }
}