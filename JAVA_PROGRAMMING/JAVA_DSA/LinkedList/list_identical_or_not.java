
// check whether lists are identical or not


import java.util.*;
class Node {
    int data;
    Node next = null;

    Node(int data){
        this.data = data;
    }
}

class LinkedList{


    Node addNode(Node head1, int data){
        Node newNode = new Node(data);
        if(head1 == null){
            head1 = newNode;   
        }else{
            Node temp = head1;
            while(temp.next != null){

                temp = temp.next;
            }
            temp.next = newNode;
        }

        return head1;
        
    }
    //print SLL
    void printSLL(Node head){
        if(head == null){
            System.out.println("LL is empty");
        }else{
            Node temp = head;
            System.out.println();
            while(temp != null){

                System.out.print("| "+temp.data +" |"+ "-->");
                temp = temp.next;
            }
            System.out.println("| Null |");   
        }

    }

    //count nodes
    int countNode(Node head){
        int cnt = 0;
        Node temp = head;
        while(temp != null){
            cnt++;
            temp = temp.next;
        }

        return cnt;
    }

    //Check Identitical or not
    boolean checkIdenticalOrNot(Node head1, Node head2) {

        if(countNode(head1) != countNode(head2))
            return false;
        else{

            Node temp1 = head1;
            Node temp2 = head2;

            while(temp1 != null){

                if(temp1.data != temp2.data){
                    return false;
                }

                temp1 = temp1.next;
                temp2 = temp2.next;
            }
        }
        return true;
    }
    
}


class User{

    public static void main(String[] args) {
        
        LinkedList ll = new LinkedList();
        Node head1 = null;
        Node head2= null;
        Scanner sc = new Scanner(System.in);
        boolean flag = false;
        do{

            System.out.println("********* M E N U ***********");
            System.out.println("1. AddNode LL 1 : ");
            System.out.println("2. AddNode LL 2 :");
            System.out.println("3. Print LinkedList  1:");
            System.out.println("4. Print LinkedList  2:");
            System.out.println("5. Check Identical Or Not :");
            System.out.println("6. Exit");
            System.out.println();
            System.out.println(" Enter the Choice");
            int ch = sc.nextInt();

            switch(ch){
                case 1 :{
                            System.out.println("Enter data : ");
                            head1 = ll.addNode(head1,sc.nextInt());
                            System.out.println("Node inserted... ");
                            
                        }
                        break;

                case 2 :{
                            System.out.println("Enter data : ");
                            head2 =ll.addNode(head2,sc.nextInt());
                            System.out.println("Node inserted... ");
                            
                        }
                        break;
                
                case 3 :{
                            ll.printSLL(head1);
                            System.out.println();
                        }
                        break;
                case 4:{
                            ll.printSLL(head2);
                            System.out.println();
                        }
                        break;
                case 5 :{
                            System.out.println();
                            if(ll.checkIdenticalOrNot(head1,head2)){
                                System.out.println("Both Lists are Identical");
                            }else{
                                System.out.println("List are Not Identical");
                            }
                        } 
                        break;
                        
                case 6 : 
                        flag = true;
            }


        }while(!flag);
      
        sc.close();
    }

}