
import java.util.*;

class Node {
    int data;
    Node next =null;

    Node(int data){
        this.data = data;
    }
}

class LinkedList{

    Node addNode(Node head, int data){

        Node newNode = new Node(data);
        if(head == null){
            head = newNode;
        }else{
            Node temp = head;
            while(temp.next != null){
                temp = temp.next;
            }
            temp.next = newNode;
        }

        return head;
    }

    //remove duplicates

    Node removeDuplicates(Node head){

        if(head == null)
            System.out.println("LL is empty");
        else{

            Node current = head;

            while( current != null){
                Node temp = current;

                while(temp != null && temp.data == current.data){
                    temp = temp.next;
                }

                current.next = temp;
                current = current.next;
            }
        }

        return head;
    }


    void printLL(Node head){
        if(head == null){
            System.out.println("LL is empty");
        }else{
            Node temp = head;
            System.out.println();
            while(temp!=null){
                System.out.print("| "+temp.data + " |-->");
                temp = temp.next;
            }
            System.out.println(" Null");
            System.out.println();
        }
    }

    int countNode(Node head){
        int cnt = 0;
        Node temp = head;

        while(temp != null){
            cnt++;
            temp = temp.next;
        }
        return cnt;
    }

    
}

class User{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Node head = null;
        boolean flag = false;

        LinkedList ll = new LinkedList();
        int ch;

        do{
            System.out.println("*******M E N U ***********");

            System.out.println("1. AddNode");
            System.out.println("2. Remove duplicates :");
            System.out.println("3. Print LL: ");
            System.out.println("4. Exit");
            System.out.println();
            System.out.println("Enter Choice : ");
            ch = sc.nextInt();

            switch(ch){
                case 1 :{
                        System.out.println("Enter data : ");

                        head = ll.addNode(head,sc.nextInt());
                        System.out.println("Node added..");
                        }
                        break;
                case 2:{
                        System.out.println("Before removing :");
                        ll.printLL(head);
                        System.out.println();
                        head = ll.removeDuplicates(head);
                        System.out.println("After removing :");
                        ll.printLL(head);
                        }break;

                case 3 :{
                        ll.printLL(head);
                        }break;
                case 4 : 
                        flag = true;
                        break;
                default : 
                        System.out.println("Wrong choice!!!");

            }

        }while(!flag);

        sc.close();
    }
}