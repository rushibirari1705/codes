
// reverse LinkedList
import java.util.*;
class Node {
    int data;
    Node next = null;

    Node(int data){
        this.data = data;
    }
}

class LinkedList{

    static Node head = null;
    

    void addNode(int data){
        Node newNode = new Node(data);
        if(head == null){
            head = newNode;   
        }else{
            Node temp = head;
            while(temp.next != null){

                temp = temp.next;
            }
            temp.next = newNode;
        }
        
    }

    
    //print SLL
    void printSLL(){
        if(head == null){
            System.out.println("LL is empty");
        }else{
            Node temp = head;
            System.out.println();
            while(temp != null){

                System.out.print("| "+temp.data +" |"+ "-->");
                temp = temp.next;
            }
            System.out.println("| Null |");   
        }

    }

    Node reverseLinkedList(Node node){

        if(head == null)
            System.out.println("LL is empty !!!");
        else{
            Node prev = null;
            Node curr = node;
            Node next = null;

            while(curr != null){
                next = curr.next;
                curr.next = prev;
                prev = curr;
                curr = next;
            }
            node = prev;
        }

        return node;
    }
}


class User{

    public static void main(String[] args) {
        
        LinkedList ll = new LinkedList();
        //LinkedList ll2 = new LinkedList();
        Scanner sc = new Scanner(System.in);
        boolean flag = false;
        do{

            System.out.println("********* M E N U ***********");
            System.out.println("1. AddNode LL 1 : ");
            System.out.println("2. ReverseLinkedList :");
            System.out.println("3. Print LinkedList :");
            System.out.println("4. Exit");
            System.out.println();
            System.out.println(" Enter the Choice");
            int ch = sc.nextInt();

            switch(ch){
                case 1 :{
                            System.out.println("Enter data : ");
                            ll.addNode(sc.nextInt());
                            System.out.println("Node inserted... ");
                            
                        }
                        break; 
                case 2 : {
                            LinkedList.head = ll.reverseLinkedList(LinkedList.head);
                        }
                        break;               
                case 3 :{
                            ll.printSLL();
                            System.out.println();
                        }
                        break;
                        
                case 4 : 
                        flag = true;
            }


        }while(!flag);

        sc.close();
    }

}