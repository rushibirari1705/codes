
// Implementation of Queue using array .

import java.util.*;

class Queue{

    int[] queueArr;
    int front;
    int rear;
    int maxSize ;

    Queue(int size){
        this.queueArr = new int[size];
        this.front = -1;
        this.rear = -1;
        this.maxSize = size;
    }

    void enQueue(int data){

        if(rear == maxSize - 1){
            System.out.println("Queue is full ");
            return;
        }
        if(front == -1)
            front = rear = 0;
        else
            rear++;
        
        queueArr[rear] = data;
        
    }

    int deQueue(){

        if(isEmpty()){
            System.out.println("Queue is Empty!");
            return -1;
        }else{
            int data = queueArr[front];
            front++;

            if(front > rear){
                front = rear = -1;
            }

            return data;

        }

    }

    int frontElement(){
        if(isEmpty()){
            System.out.println("Queue is empty");
            return -1;
        }else{
            return queueArr[front];
        }
    }

    int rearElement(){
        if(isEmpty()){
            System.out.println("Queue is empty");
            return -1;
        }else{
            return queueArr[rear];
        }
    }

    void printQueue(){
        if(isEmpty())
            System.out.println("Queue is empty");   
        else {
            for(int i = front; i <= rear;i++){
                System.out.print(queueArr[i] + "  ");
            }
            System.out.println();
            System.out.println();
        } 
    }

    boolean isEmpty(){
        if(front == -1)
            return true;
        else
            return false;
    }
    
}

class Client{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the size of Array  :");
        int size = sc.nextInt();

        Queue qe = new Queue(size);

        boolean flag = false;

        do{
            System.out.println("****** M E N U *******");
            System.out.println("1. Enqueue Data : ");
            System.out.println("2. Dequeue Data : ");
            System.out.println("3. Empty :");
            System.out.println("4. Front element : ");
            System.out.println("5. Rear element :");
            System.out.println("6. PrintQueue : ");
            System.out.println("7. Exit :");

            System.out.println();
            System.out.println("Enter your choice : ");
            int ch = sc.nextInt();

            switch(ch){

                case 1 : {
                            System.out.println("Enter the dada : ");
                            qe.enQueue(sc.nextInt());
                            
                        }break;
                case 2 : {
                            int ret = qe.deQueue();
                            if(ret != -1)
                                System.out.println( ret + "  is dequeued..");

                        }break;
                case 3 : {
                            if(qe.isEmpty())
                                System.out.println("Queue is Empty..");
                            else 
                                System.out.println("Queue is Not Empty..");

                        }break;
                case 4 : {  

                            int ret = qe.frontElement();
                            if(ret != -1)
                                System.out.println("Front element is  : " + ret);

                        }break;
                case 5 : {
                            int ret = qe.rearElement();
                            if(ret != -1)
                                System.out.println("Front element is  : " + ret);

                        }break;
                case 6 : {
                            qe.printQueue();
                        }break;
                case 7 :{  
                            flag  = true;
                        }break;
                default :
                        System.out.println("Wrong choice !!!");
                

            }



        }while(!flag);

        sc.close();
    }
}