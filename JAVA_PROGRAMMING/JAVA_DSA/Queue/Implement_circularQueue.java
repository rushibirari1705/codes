
// Implementation of Circular Queue.

import java.util.*;

class CircularQueue {
    int[] queArr;
    int front;
    int rear;
    int maxSize;

    CircularQueue(int size){
        this.queArr = new int[size];
        this.front = -1;
        this.rear = -1;
        this.maxSize = size;
    }

    void enQueue(int data){

        if((front == 0 && rear == maxSize - 1) || ((rear + 1 )% maxSize) == front) {
            System.out.println("Queue is Full... \n ");
            return;
        }else if( front == -1)
            front = rear = 0;
        else if( rear == maxSize - 1 && front != 0)
            rear = 0;
        else
            rear++;
        
        queArr[rear] = data;
    }

    int deQueue(){

        if(front == -1){
            System.out.println("Queue is empty.. \n");
            return -1;
        }else{
            int ret = queArr[front];
            if(front == rear){
                front = rear = -1;
            }else if(front == maxSize -1){
                front = 0;
            }else{
                front++;
            }
            return ret;
        }
    }

    void printQueue(){
        if(front == -1){
            System.out.println("Queue is empty.. \n");
            return;
        }else if(front <= rear){
            for(int i = front ; i<= rear ; i++){
                System.out.print(queArr[i] + " ");
            }
        }else{
            for(int i = front ; i < maxSize ; i++){
                System.out.print(queArr[i] + " ");
            }
            for(int i = 0 ; i <= rear ; i++){
                System.out.print(queArr[i] + " ");
            }
        }
    }

    boolean isEmpty(){
        if(front == -1)
            return true;
        else
            return false;
    }


}


class Client{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the size of Queue : ");
        int size = sc.nextInt();

        CircularQueue cq = new CircularQueue(size);

        boolean flag = true;

        do{
            System.out.println(" ***** M E N U ***** ");
            System.out.println("1. Enqueue  :");
            System.out.println("2. Dequeue : ");
            System.out.println("3. PrintQueue : ");
            System.out.println("4. EmptyOrNot :");
            System.out.println("5. Exit :");

            System.out.println(" Enter the choice : ");
            int ch = sc.nextInt();

            switch(ch){
                case 1: {
                            System.out.println("Enter the data : ");
                            cq.enQueue(sc.nextInt());
                            
                        }break;
                case 2 :{
                            int ret = cq.deQueue();
                            if(ret != -1)
                                System.out.println(ret + "  is dequeued..");

                        }break;
                case 3 :{
                            cq.printQueue();
                            System.out.println("\n");

                        }break;
                case 4 :{
                            if(cq.isEmpty())
                                System.out.println("Queue is Empty");
                            else    
                                System.out.println("Queue is Not Empty");

                        }break;
                case 5 : 
                        flag = false;
                        break;
                default : 
                        System.out.println("Wrong input!!!");
            }

        }while(flag);

        sc.close();
    }
}

