

import java.util.*;

class QueueTwoStacks{

    Stack<Integer> s1 = new Stack<>();
    Stack<Integer> s2 = new Stack<>();


    void enQueue(int data){
        while(!s1.isEmpty())
            s2.push(s1.pop());

        s1.push(data);

        while(!s2.isEmpty())
            s1.push(s2.pop());

    }

    int deQueue(){
        if(s1.isEmpty())
            return -1;
        
        return s1.pop();
    }

    void printQueue(){
        if(s1.isEmpty()){
            System.out.println("Queue is empty..");
            return ;
        }
        Collections.reverse(s1);
        System.out.println(s1 + "\n");
        Collections.reverse(s1);
    }
    
}

class Client{

    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        QueueTwoStacks que = new QueueTwoStacks();

        boolean flag = false;

        do{

            System.out.println("****** M E N U *****");
            System.out.println("1. Enqueue :");
            System.out.println("2. Dequeue : ");
            System.out.println("3. PrintQueue  :");
            System.out.println("4. Exit");

            System.out.println("Enter the choice : ");
            int choice = sc.nextInt();

            switch(choice){
                case 1 : {
                    System.out.println("Enter the Data : ");
                    int data = sc.nextInt();
                    que.enQueue(data);

                }break;
                case 2 :{
                    int ret = que.deQueue();
                    if(ret == -1)
                        System.out.println("Queue is empty..");
                    else
                        System.out.println(ret + " is dequeued.");

                }break;
                case 3 :{
                        que.printQueue();

                }break;
                case 4 :
                        flag = true;
                        break;

                default : 
                    System.out.println("Wrong Choice !!!!");
            }
        }while(!flag);

        sc.close();
    }
}
