package Queue_;
/*
    Q5. Queue Operations
    Given N integers, the task is to insert those elements in the queue. Also, given M
    integers, the task is to find the frequency of each number in the Queue.
    Note:
    insert() will be called N times by main().
    findFrequency() will be called M times by main();
    Where k is each element passing through respective function calls.

    Example 1:
        Input:
        N = 8
        1 2 3 4 5 2 3 1
        M = 5
        1 3 2 9 10
        Output:
        2
        2
        2
        -1
        -1
        Explanation:
        After inserting 1, 2, 3, 4, 5, 2, 3 and 1 into the queue, frequency of 1 is 2, 3 is 2 and
        2 is 2. Since 9 and 10 are not there in the queue we output -1 for them.
        
    Example 2:
        Input:
        N = 6
        1 2 1 1 1 4
        M = 4
        1 5 4 3
        Output:
        4
        -1
        1
        -1
        Explanation:
        After inserting 1, 2, 1, 1, 1 and 4 into the queue, frequency of 1 is 4 and that of 4 is 1.
        Since 5 and 3 are not there in the queue we output -1 for them.
 */

import java.util.*;
import java.util.LinkedList;

class QueueOperations{

    void findFrequency(Queue<Integer> que , int[] arr){

        for(int i = 0 ; i < arr.length; i++){
            int freq = 0;
            for(int j = 0 ; j < que.size() ; j++){
                
                int x = que.poll();
                if(arr[i] == x){
                    freq++;
                }
                que.offer(x);

            }
            if(freq != 0)
                System.out.println(freq);
            else
                System.out.println(-1);
        }
    }

}

class Client{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter size of queue : ");
        int size = sc.nextInt();

        Queue<Integer> que = new LinkedList<Integer>();
        System.out.println("Enter queue elements : ");
        for(int i = 0; i < size;i++){
            que.offer(sc.nextInt());
        }

        System.out.println("Enter size to find frequency of elements : ");
        int size_m = sc.nextInt();
        System.out.println("Enter M elements : ");
        int[] arr = new int[size_m];
        for(int i = 0; i < size_m;i++){
            arr[i] = sc.nextInt();
        }

        QueueOperations obj = new QueueOperations();
        obj.findFrequency(que,arr);

        sc.close();
    }
}