

/*
    Queue Reversal
        Given a Queue Q containing N elements. The task is to reverse the Queue. Your
        task is to complete the function rev(), that reverses the N elements of the queue.
        Example 1:
        Input:
        6
        4 3 1 10 2 6
        Output:
        6 2 10 1 3 4
        Explanation:
        After reversing the given elements of the queue , the resultant queue will be 
        6 2 10 1 3 4
 */
import java.util.*;
import java.util.LinkedList;

class QueueReversal{

    Queue<Integer> reverseQueue(Queue<Integer> que){

        Stack<Integer> st = new Stack<>();

        while(!que.isEmpty())
            st.push(que.poll());
        
        while(!st.isEmpty())
            que.offer(st.pop());

        return que;
    }

}

class Client{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the size of the queue : ");
        int size = sc.nextInt();

        Queue<Integer> qe = new LinkedList<>();
        for(int i = 0; i < size; i++)
            qe.offer(sc.nextInt());
        System.out.println("Before reverse queue : ");
        System.out.println(qe);

        QueueReversal qr = new QueueReversal();
        qe = qr.reverseQueue(qe);
        System.out.println("After reverse queue : ");
        System.out.println(qe);

        sc.close();
    }
}