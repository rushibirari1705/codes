
/*Q3. Reverse First K elements of Queue
    Given an integer K and a queue of integers, we need to reverse the order of the first
    K elements of the queue, leaving the other elements in the same relative order.
    Only following standard operations are allowed on queue.
    enqueue(x) : Add an item x to rear of queue
    dequeue() : Remove an item from front of queue
    size() : Returns number of elements in queue.
    front() : Finds front item.
    Note: The above operations represent the general processings. In-built functions of
    the respective languages can be used to solve the problem.
    Example 1:
    Input:
    5 3
    1 2 3 4 5
    Output:
    3 2 1 4 5 
    
    */

import java.util.*;
import java.util.LinkedList;


class QueueReversal{

    Queue<Integer> modifyQueue(Queue<Integer> q, int k) {
        
        Stack<Integer> st= new Stack<>();
        Queue<Integer> q1= new LinkedList<>();
        while(k!=0){
            st.push(q.poll());
            k--;
        }
        while(!st.isEmpty())
            q1.offer(st.pop());
        
        while(!q.isEmpty())
            q1.offer(q.poll());

        return q1;
    }

}

class Client{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the size of the queue : ");
        int size = sc.nextInt();

        Queue<Integer> qe = new LinkedList<>();
        for(int i = 0; i < size; i++)
            qe.offer(sc.nextInt());

        System.out.println("Enter the K elements u want to reverse : ");
        int k = sc.nextInt();

        System.out.println("Before reverse queue : ");
        System.out.println(qe);

        QueueReversal qr = new QueueReversal();
        qe = qr.modifyQueue(qe,k);
        System.out.println("After reverse queue : ");
        System.out.println(qe);

        sc.close();
    }
}