package Sorting_Codes;

import java.util.*;
class MergeSort{

    static void merge(int[] arr, int start, int mid ,int end){

        int s1 = mid - start + 1;
        int s2 = end - mid;

        int arr1[] = new int[s1];
        int arr2[] = new int[s2];

        for(int i = 0; i < s1 ; i++){
            arr1[i] = arr[start + i];
        }

        for(int i = 0; i < s2 ; i++){
            arr2[i] = arr[mid + i + 1];
        }

        int i = 0, j = 0, k = start;

        while(i < s1 && j < s2){
            if(arr1[i] <= arr2[j]){
                arr[k] = arr1[i];
                i++;
            }else{
                arr[k]  = arr2[j];
                j++;
            }

            k++;
        }

        while(i < s1){
            arr[k] = arr1[i];
            i++;
            k++;
        }

        while(j < s2){
            arr[k] = arr2[j];
            j++;
            k++;
        }

    }

    static void mergeSort(int[] arr, int start, int end){

        if(start < end){
            int mid = (start + end)/2;

            mergeSort(arr, start, mid);
            mergeSort(arr, mid + 1, end);
            merge(arr, start,mid, end);
        }
    }


    public static void main(String[] args) {
        
        int[] arr = new int[]{3,5,0,8,-1,3,4};
        System.out.println("Before sorting");
        for(int i=0; i<arr.length; i++){
            System.out.print( arr[i] + " ");
        }

        mergeSort(arr, 0, arr.length - 1);

        System.out.println();
        System.out.println("after sorting");
        
        System.out.println(Arrays.toString(arr));
        
    }
}