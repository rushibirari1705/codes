
//Quick sort
package Sorting_Codes;

import java.util.*;
class QuickSort{

    static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    static int partition(int[] arr, int start, int end){

        int pivot  = arr[end];
        int i = start - 1;

        for(int j = start; j < end; j++){
            if(arr[j] < pivot){

                i++;
                swap(arr,i, j);
            }
        }
        i++;
        swap(arr, i, end);

        return i;
    }

    static void quickSort(int[] arr, int start, int end){

        if(start < end){

            int pivot = partition(arr, start, end);

            quickSort(arr, start, pivot - 1);
            quickSort(arr, pivot + 1, end);

        }

    }

    public static void main(String[] args) {
        
        int[] arr = new int[]{3,5,0,8,-1,3,4};
        System.out.println("Before sorting");
        for(int i=0; i<arr.length; i++){
            System.out.print( arr[i] + " ");
        }

        quickSort(arr, 0, arr.length - 1);

        System.out.println();
        System.out.println("after sorting");
        
        System.out.println(Arrays.toString(arr));
        
    }
}