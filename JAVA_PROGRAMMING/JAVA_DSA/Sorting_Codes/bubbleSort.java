package Sorting_Codes;

//bubble sort

class Bubble{

    static void bubbleSort(int[] arr){

        for(int i = 0; i < arr.length - 1; i++){
            for(int j = 0; j < arr.length - i - 1; j++){

                if(arr[j] > arr[j+1]){
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }
        }

    }


    public static void main(String[] args) {
        int[] arr = {5,6,1,2,-8,6};
        System.out.println("Before sorting");
        for(int i=0; i<arr.length; i++){
            System.out.print( arr[i] + " ");
        }

        bubbleSort(arr);
        System.out.println();
        System.out.println("after sorting");
        for(int i=0; i<arr.length; i++){
            System.out.print( arr[i] + " ");
        }
    }
}