// Implementation of two stack in array

import java.util.*;

class MyStack{
    int size;
    int[] stackArr;
    int top1 = -1;
    int top2;

    MyStack(int size){

        this.size = size;
        this.top2 = size;
        this.stackArr = new int[size];

    }


    // push data into stack 1
    int push1(int data){

        if(top2 - top1 >  1  ){
            top1++;
            stackArr[top1] = data;
            return 0;
        }
        return -1;
        
    }

    // push data into stack 2
    int push2(int data){
        if(top2 - top1 > 1){
            top2--;
            stackArr[top2] = data;
            return 0;
        
        }
        return -1;
    }

    // pop from stack 1
    int pop1(){
        if(top1 == -1)
            return -1;
        else{
            
            int data = stackArr[top1];
            top1--;
            return data;
        }
    }

    // pop from stack 2
    int pop2(){
        if(top1 == size)
            return -1;
        else{
            int data = stackArr[top2];
            top2++;
            return data;
        }
    }

    int printStack1(){
        if(top1 == -1){
            return -1;
        }else{
            System.out.print(" [");
            for(int i = 0; i <= top1; i++){
                System.out.print("  "+stackArr[i]);
            }
            System.out.println(" ] ");

        }
        return 0;
    }

    int printStack2(){
        if(top2 == size){
            return -1;
        }else{
            System.out.print(" [");
            for(int i = top2; i < size ; i++){
                System.out.print("  "+stackArr[i]);
            }
            System.out.println(" ] ");

        }
        return 0;
    }

}

class Client{
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");
        int size = sc.nextInt();

        MyStack st = new MyStack(size);

        boolean flag  = false;
        do{
            System.out.println("1. Push 1");
            System.out.println("2. Push 2");
            System.out.println("3. pop 1");
            System.out.println("4. pop 2");
            System.out.println("5. print Stack 1");
            System.out.println("6. print Stack 2");
            System.out.println("7. Exit");
            System.out.println();
            System.out.println("Enter the choice : ");
            int ch = sc.nextInt();

            switch (ch) {
                case 1:{    
                            System.out.println("Enter data :");
                            int ret = st.push1(sc.nextInt());
                            if(ret == -1)
                                System.out.println("Stack1 is overFlow");
                                                
                        }break;
                case 2:{
                            System.out.println("Enter data :");
                            int ret = st.push2(sc.nextInt());
                            if(ret == -1)
                                System.out.println("Stack2 is overFlow");

                        }break;
                case 3:{    

                        int ret = st.pop1();
                        if(ret == -1){
                            System.out.println("Stack1 is UnderFlow");
                        }else{
                            System.out.println(ret + " is popped !!");
                        }                    
                        }break;
                case 4:{
                        int ret = st.pop2();
                        if(ret == -1){
                            System.out.println("Stack2 is UnderFlow");
                        }else{
                            System.out.println(ret + " is popped !!");
                        }
                        }break;
                case 5:{
                            int ret = st.printStack1();
                            if(ret == -1)
                            System.out.println("stack is empty!!!");
                        }break;

                case 6:{
                            int ret = st.printStack2();
                            if(ret == -1)
                            System.out.println("stack is empty!!!");
                        }break;
                case 7:{
                            flag = true;
                    
                        }break;
            
                default:
                System.out.println("Wrong choice!!");
                    break;
            }
            


        }while(!flag);
        sc.close();
        
    }
}