
/*
    9. Get min at pop
    Now, we'll try to solve a famous stack problem.
    You are given an array A of size N. You need to first push the elements of the array into a
    stack and then print minimum in the stack at each pop until stack becomes empty.
    Example 1:
    Input:
    N = 5
    A = {1 2 3 4 5}
    Output:
    1 1 1 1 1
    Explanation:
    After pushing elements to the stack,
    the stack will be "top -> 5, 4, 3, 2, 1".
    Now, start popping elements from the stack
    popping 5: min in the stack is 1.popped 5
    popping 4: min in the stack is 1. popped 4
    popping 3: min in the stack is 1. popped 3
    popping 2: min in the stack is 1. popped 2
    popping 1: min in the stack is 1. popped 1
 */
import java.util.*;
class GetMin{
    
    void pushData(int arr[]){
        Stack<Integer> st = new Stack<Integer>();
        int min = arr[0];

        for(int i=0;i<arr.length;i++){
            min = Math.min(min,arr[i]);// using math min() function
            st.push(min);   
        }
        getMinAtPop(st);
    }
            
    
    void getMinAtPop(Stack<Integer>s)
    {
        while(!s.isEmpty())
        System.out.print(s.pop()+" ");
    }
}

class Client{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the array size : ");
        int size = sc.nextInt();
        int[] arr = new int[size];
        System.out.println("Enter the array element : ");
        for(int i = 0; i < size; i++){
            arr[i] = sc.nextInt();

        }
        GetMin obj = new GetMin();
        obj.pushData(arr); 

        sc.close();
    }
}