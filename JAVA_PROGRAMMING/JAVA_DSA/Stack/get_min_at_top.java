

import java.util.*;
class GetMin{
    
    void pushData(int arr[]){
        Stack<Integer> st = new Stack<Integer>();
        int min = arr[0];

        for(int i=0;i<arr.length;i++){
            min = Math.min(min,arr[i]);// using math min() function
            st.push(min);   
        }
        getMinAtPop(st);
    }
            
    
    void getMinAtPop(Stack<Integer>s)
    {
        while(!s.isEmpty())
        System.out.print(s.pop()+" ");
    }
}

class Client{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the array size : ");
        int size = sc.nextInt();
        int[] arr = new int[size];
        System.out.println("Enter the array element : ");
        for(int i = 0; i < size; i++){
            arr[i] = sc.nextInt();

        }
        GetMin obj = new GetMin();
        obj.pushData(arr); 

        sc.close();
    }
}