

// Problem 1 :
/* 1. Immediate Smaller Element
Given an integer array Arr of size N. For each element in the array, check whether the
right adjacent element (on the next immediate position) of the array is smaller. If next
element is smaller, update the current index to that element. If not, then -1.

Example 1:
Input:
N = 5
Arr[] = {4, 2, 1, 5, 3}
Output:
2 1 -1 3 -1

Explanation: Array elements are 4, 2, 1, 5
3. Next to 4 is 2 which is smaller, so we
print 2. Next of 2 is 1 which is smaller,
so we print 1. Next of 1 is 5 which is
greater, so we print -1. Next of 5 is 3
which is smaller, so we print 3. Note
that for last element, output is always
going to be -1 because there is no element
on right.*/

import java.util.*;
class ImmediateSmaller{

    int[] checkImmediateSmaller(int[] arr){
        int arr1[] = new int[arr.length];
        for(int i = 0; i < arr.length - 1; i++){

            if(arr[i] > arr[i + 1]){
                arr1[i] = arr[i + 1];

            }else{
                arr1[i] = -1;
            }

        }
        arr1[arr.length - 1] = -1;
        return arr1;
    }

}

class Client{
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the array size : ");
        int size = sc.nextInt();
        System.out.println("Enter elements : ");
        int arr[] = new int[size];
        for(int i = 0; i < size; i++){
            arr[i] = sc.nextInt();
        }

        ImmediateSmaller im = new ImmediateSmaller();

        int[] retArr = im.checkImmediateSmaller(arr);

        for(int i : retArr){
            System.out.print(i + " ");
        }
        System.out.println();
    
    
        sc.close();
    }
}