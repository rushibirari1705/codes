
/*
   Implement stack using linkedlist
 */

 import java.util.*;
class Node{
    int data;
    Node next = null;
    Node(int data){
        this.data = data;
    }   
}

class LinkedList{

    Node head = null;
    int flag = 0;

    void pushNode(int data){
        Node newNode = new Node(data);
        if(head == null){
            head = newNode;

        }else{
            newNode.next = head;
            head = newNode;
        }
    }

    void printStack(){
        if(head == null){
            System.out.println("Stack is Empty");
        }else{
            Node temp = head;
            System.out.println();
            while(temp != null){
                System.out.print("|"+temp.data + "|->");
                temp = temp.next;
            }
            System.out.println("Null");
            System.out.println();
        }
    }

    int size(){
        int cnt = 0;
        Node temp = head;
        while(temp != null){
            cnt++;
            temp = temp.next;
        }
        return cnt;
    }

    int popNode(){
        if(head == null){
            flag = 1;
            return -1;
        }else{
            flag = 0;
            int data = head.data;
            head = head.next;
            return data;
        }
    }

}

class Client{
    public static void main(String[] args) {
        
        LinkedList ll = new LinkedList();
        Scanner sc = new Scanner(System.in);

        boolean flag = false;
        do{
            System.out.println("******** M E N U *********");
            System.out.println("1. Push Node");
            System.out.println("2. Pop Node");
            System.out.println("3. size");
            System.out.println("4. printStack ");
            System.out.println("5. Exit");

            System.out.println("Enter the choice : ");
            int ch = sc.nextInt();

            switch(ch){
                case 1 : {
                    System.out.println("Enter the data : ");
                    ll.pushNode(sc.nextInt());

                }break;
                case 2 : {
                    int ret = ll.popNode();
                    if(ll.flag != 0){
                        System.out.println("Stack is Empty..");
                    }else{
                        System.out.println(ret+ " is popped ..");
                    }
                    

                }break;
                case 3 : {
                    System.out.println("size of stack : " + ll.size());

                }break;
                case 4 : {
                    ll.printStack();

                }break;
                case 5 : {
                    flag = true;

                }break;
                default:
                    System.out.println("Invalid choice");
                    break;

            }
        }while(!flag);

        sc.close();

    }
}