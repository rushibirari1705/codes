
// Implement two stacks in Array
import java.util.*;
class TwoStacks{

    int stackArr[];
    int maxSize;
    int top1;
    int top2;

    TwoStacks(int size){
        this.stackArr = new int[size];
        this.top1 = -1;
        this.top2 = size;
        this.maxSize = size;

    }

    void push1(int data){
        if(top2 - top1 > 1){
            stackArr[++top1] = data;
            System.out.println("data inserted in stack 1");
        }else{
            System.out.println("Stack 1 is Overflow");

        }
    }

    void push2(int data){
        if(top2 - top1 > 1){
            stackArr[--top2] = data;
            System.out.println("data inserted in stack 2");
        }else{
            System.out.println("Stack 2 is Overflow");

        }

    }

    int pop1(){

        if(top1 == -1){
            System.out.println(" stack 1 is empty ..");
        }
            int ret = stackArr[top1];
            top1--;
            return ret;
    }

    int pop2(){

        if(top2 == maxSize){
            System.out.println(" stack 2 is empty ..");
        }
            int ret = stackArr[top2];
            top2++;
            return ret;
    }

    void printStack1(){
        if(top1 == -1)
            System.out.println("Stack 1 is Empty.");
        else{
            System.out.print("[ ");
            for(int i = 0; i <= top1; i++){
                System.out.print(stackArr[i] + " ");
            }
            System.out.println("]");
        }
    }

    void printStack2(){
        if(top1 == maxSize)
            System.out.println("Stack 2 is Empty.");
        else{
            System.out.print("[ ");
            for(int i = maxSize - 1; i >= top2; i--){
                System.out.print(stackArr[i] + " ");
            }
            System.out.println("]");
        }
    }
}

class Client{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the size of array : ");
        int size  = sc.nextInt();

        TwoStacks ts = new TwoStacks(size);
        
        boolean flag = false;

        do{
            System.out.println("******** M  E  N  U *********");
            System.out.println("1. Push 1");
            System.out.println("2. Push 2");
            System.out.println("3. Pop 1");
            System.out.println("4. Pop 2");
            System.out.println("5. Print stack 1");
            System.out.println("6. Print stack 2");
            System.out.println("7. Exit");

            System.out.println();
            System.out.println("Enter your choice : ");
            int choice = sc.nextInt();

            switch (choice) {
                case 1:{
                            System.out.println("Enter the data : ");
                            ts.push1(sc.nextInt());  
                        }
                        break;
                case 2:{
                            System.out.println("Enter the data : ");
                            ts.push2(sc.nextInt());
                        }
                        break;
                case 3 :{
                            int ret = ts.pop1();
                            if(ret != -1)
                            System.out.println(ret + " popped.");
                        }
                        break;
                case 4:{
                            int ret = ts.pop2();
                            if(ret != -1)
                            System.out.println(ret + " popped.");
                        }
                        break;
                case 5:{
                            ts.printStack1();

                        }
                        break;
                case 6:{
                            ts.printStack2();
                        }
                        break;
                case 7:    
                        flag = true;
                        break;
            
                default:
                        System.out.println("Wrong choice !!! ");
                    break;
            }

        }while(!flag);
        sc.close();
    }

    
}