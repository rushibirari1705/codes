
/*
    14. Insert an Element at the Bottom of a Stack

    You are given a stack St of N integers and an element X. You have to insert X at the
    bottom of the given stack.
    Example 1:
    Input:
    N = 5
    X = 2
    St = {4,3,2,1,8}
    Output:
    {2,4,3,2,1,8}
    Explanation:
    After insertion of 2, the final stack will be {2,4,3,2,1,8}.

    Example 2:
    Input:
    N = 3
    X = 4
    St = {5,3,1}
    Output:
    {4,5,3,1}
    Explanation:
    After insertion of 4, the final stack will be {4,5,3,1}.
 */

import java.util.*;

class InsertElement{

    Stack<Integer> insertElement(Stack<Integer> stack ,int key){
        
        int[] arr = new int[stack.size() + 1];
        arr[0] = key;
        for(int i = arr.length - 1 ; i >= 1; i--){
            arr[i] = stack.pop();
        }
        for( int i = 0; i < arr.length; i++){
            stack.push(arr[i]);
        }
     
        return stack;
    }
    
}

class Client{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Enter size of stack : ");
        int size = sc.nextInt();
        System.out.println("Enter Element of stack : ");
        
        Stack<Integer> st = new Stack<>();

        for(int i = 0; i < size; i++) {
            st.push(sc.nextInt());

        }

        System.out.println("Enter element you want to insert :");
        int key = sc.nextInt();

        InsertElement obj = new InsertElement();
        Stack<Integer> newStack = obj.insertElement(st, key);

        System.out.println(newStack);
    }
}
