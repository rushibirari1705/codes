
/*8. Evaluation of Postfix Expression
    Given string S representing a postfix expression, the task is to evaluate the expression
    and find the final value. Operators will only include the basic arithmetic operators like *,/, + and -.
    Example 1:
    Input: S = "231*+9-"
    Output: -4
    Explanation:
    After solving the given expression,
    we have -4 as a result.

    Example 2:
    Input: S = "123+*8-"
    Output: -3
    Explanation:
    After solving the given postfix
    expression, we have -3 as a result.
    Expected Time Complexity: O(|S|)
    Expected Auxiliary Space: O(|S|) */

    import java.util.*;
    class PostfixEvalution{

        int postfixEvalution(String str){

            Stack<Integer> st = new Stack<Integer>();
            int c_cnt = 0;
            int n_cnt = 0;

            for(int i = 0; i < str.length(); i++){
                if(Character.isDigit(str.charAt(i)))
                    n_cnt++;
                
                if(Character.isLetter(str.charAt(i)))
                    c_cnt++;
            }
            if(n_cnt - c_cnt >= 1){
                for(int i = 0; i < str.length(); i++){
                    char ch = str.charAt(i);

                    if(Character.isDigit(ch)){
                        st.push(ch - '0');

                    }else{
                        
                        int x = st.pop();
                        int y = st.pop();

                        if(ch == '+')
                            st.push(x + y);
                        if(ch == '-')
                            st.push(y - x);
                        if(ch == '*')
                            st.push(x * y);
                        if(ch == '/')
                            st.push(y / x);   
                    }
                }
            }else{
                System.out.println("Invalid Expression");
                
            }
            return st.pop();
        }

    }

    class Client{
        public static void main(String[] args) {
            Scanner sc = new Scanner(System.in);
            System.out.println("Enter the postfix expression : ");
            String exp = sc.next();

            PostfixEvalution p = new PostfixEvalution();

            System.out.println(p.postfixEvalution(exp));
            sc.close();

        }
    }