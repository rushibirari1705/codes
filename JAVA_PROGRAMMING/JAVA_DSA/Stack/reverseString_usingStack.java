
// problem 1 .
// Reverse string using stack
import java.util.*;

class ReverseString {
    
    String reverseString(String str){

        Stack<Character> st = new Stack<Character>();

        char chArr[] = new char[str.length()];

        for(int i = 0; i < str.length(); i++){
            st.push(str.charAt(i));
        }

        int i = 0;

        while(!st.empty()){
            chArr[i] = st.pop();
            i++;
        }

        return new String(chArr); // constructor

    }

}
class Client{

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the string : ");
        String str = sc.nextLine();

        ReverseString rs = new ReverseString();

        System.out.println(rs.reverseString(str));

    }
}