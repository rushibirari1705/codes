
/*
    11. Reversing the equation
    Given a mathematical equation that contains only numbers and +, -, *, /. Print the
    equation in reverse, such that the equation is reversed, but the numbers remain the same.
    It is guaranteed that the given equation is valid, and there are no leading zeros.
    Example 1:
    Input:
    S = "20-3+5*2"
    Output: 2*5+3-20
    Explanation: The equation is reversed with
    numbers remaining the same.
    Example 2:
    Input:
    S = "5+2*56-2/4"
    Output: 4/2-56*2+5
    Explanation: The equation is reversed with
    numbers remaining the same.
    Expected Time Complexity: O(|S|).
    Expected Auxiliary Space: O(|S|).
    
*/

import java.util.*;

class ReverseEquation {

    String reverseEquation(String S){

        Stack<Character> s = new Stack<>();
        
        int i = S.length()-1;
        StringBuilder sb = new StringBuilder("");
        
        while(i>=0)
        {
            char ch = S.charAt(i);
            if(ch == '+' || ch == '-' || ch == '*' || ch == '/')
            {
                while(!s.isEmpty())
                {
                    char c = s.peek();
                    sb.append(c);
                    s.pop();
                }
                
                sb.append(ch);
                
            }else{
                s.push(ch);
            }
            
            i--;
            
        }
            while(!s.isEmpty())
                {
                    char c = s.peek();
                    sb.append(c);
                    s.pop();
                }
        return sb.toString();
    }

}
class Client{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the equation : ");
        String str = sc.next();
        
        ReverseEquation eq = new ReverseEquation();
        String str1 = eq.reverseEquation(str);

        System.out.println(str1);

        sc.close();

    }
}