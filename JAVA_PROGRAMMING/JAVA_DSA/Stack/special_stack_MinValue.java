
/*  Problem :  Special stack
 * Design a data-structure SpecialStack that supports all the stack operations like push(),
    pop(), isEmpty(), isFull() and an additional operation getMin() which should return
    minimum element from the SpecialStack. Your task is to complete all the functions, using
    stack data-Structure.
    Example 1:
    Input:
    Stack: 18 19 29 15 16
    Output: 15
    Explanation:
    The minimum element of the stack is 15.
    Note: The output of the code will be the value returned by getMin() function.
    */

import java.util.*;

class MyStack{

    int maxSize;
    int[] stackArr;
    int top  = -1 ;
    int flag = 0;

    MyStack(int size){
        this.stackArr = new int[size];
        this.maxSize = size;
    }

    void push(int data){

        if(top == maxSize - 1){
            System.out.println("Stack is overflow..");
            return;
        }else{
            top++;
            stackArr[top] = data;
        }
    }

    int pop(){
        if(empty()){
            System.out.println("Stack is underflow..");
            return -1;
        }else{
            int val = stackArr[top];
            top--;
            return val;
        }
    }


    boolean empty(){
        if(top == -1)
            return true;
        else
            return false;
    }

    int size(){
        return top + 1;
    }

    void printStack(){
        if(empty()){
            System.out.println("Nothing to Print");
        }else{
            System.out.print("[ ");

            for(int i = 0; i <= top; i++){
                System.out.print(stackArr[i] +" ");
            }
            System.out.println("]");
        }
    }

    int peek(int top){
        return stackArr[top];
    }

    int getMinValue(){
        int minValue = Integer.MAX_VALUE;
        if(empty()){
            System.out.println("stack is empty");
            flag = 0;
        }else{
            
            int tp = top;
            for(int i = tp; i >= 0; i--){
                int x = peek(i);
                if(x < minValue){
                    minValue = x;    
                }
            }
            flag = 1;
        }
        return minValue;

    }
    
    
}

class Client{
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the stack size : ");
        int size = sc.nextInt();

        MyStack st = new MyStack(size);

        boolean flag = false;

        do{
            System.out.println("****** M E N U ********");
            System.out.println(" 1. push ");
            System.out.println(" 2. pop ");
            System.out.println(" 3. empty");
            System.out.println(" 4. size");
            System.out.println(" 5. Print stack");
            System.out.println(" 6. Get min value");
            System.out.println(" 7. Exit");

            System.out.println("Enter the choice : ");
            int ch = sc.nextInt();

            switch(ch){
                case 1 :{
                            System.out.println("Enter the data : ");
                            st.push(sc.nextInt());
                        }
                        break;

                case 2 :{
                            int val = st.pop();
                            if(val != -1){
                                System.out.println(val + " is popped...");
                            }
                        }
                        break;

                case 3 : {
                                if(st.empty())
                                    System.out.println("Stack is empty");
                                else    
                                    System.out.println("Stack is Not empty");
                        }
                        break;
                    
                case 4 : {
                            System.out.println("size is : " + st.size());
                        }
                        break;  
                case 5 : {
                            st.printStack();
                        }
                        break;
                case 6 : {
                            int retVal = st.getMinValue();
                            if(st.flag != 0)
                                System.out.println("Minimum Value is : " + retVal);
                        }break;
                case 7 :
                            flag = true;
                            break;

                default :
                            System.out.println("Wrong choice!!!");

            }

        }while(!flag);
        
        sc.close();
    }
}