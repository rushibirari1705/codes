
import java.util.*;
// Implementating Stack using array:

class MyStack{

    int maxSize;
    int[] stackArr;
    int top  = -1 ;

    MyStack(int size){
        this.stackArr = new int[size];
        this.maxSize = size;
    }

    void push(int data){

        if(top == maxSize - 1){
            System.out.println("Stack is overflow..");
            return;
        }else{
            top++;
            stackArr[top] = data;
        }
    }

    int pop(){
        if(empty()){
            System.out.println("Stack is underflow..");
            return -1;
        }else{
            int val = stackArr[top];
            top--;
            return val;
        }
    }

    int peek(){
        if(empty()){
            System.out.println("Stack is empty");
            return -1;
        }else{
            return stackArr[top];
        }
    }

    boolean empty(){
        if(top == -1)
            return true;
        else
            return false;
    }

    int size(){
        return top + 1;
    }

    void printStack(){
        if(empty()){
            System.out.println("Nothing to Print");
        }else{
            System.out.print("[ ");

            for(int i = 0; i <= top; i++){
                System.out.print(stackArr[i] +" ");
            }
            System.out.println("]");
        }
    }
    
    
}

class Client{
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the stack size : ");
        int size = sc.nextInt();

        MyStack st = new MyStack(size);

        boolean flag = false;

        do{
            System.out.println(" 1. push ");
            System.out.println(" 2. pop ");
            System.out.println(" 3. peek");
            System.out.println(" 4. empty");
            System.out.println(" 5. size");
            System.out.println(" 6. Print stack");
            System.out.println(" 7. Exit");

            System.out.println("Enter the choice : ");
            int ch = sc.nextInt();

            switch(ch){
                case 1 :{
                            System.out.println("Enter the data : ");
                            st.push(sc.nextInt());
                        }
                        break;

                case 2 :{
                            int val = st.pop();
                            if(val != -1){
                                System.out.println(val + " is popped...");
                            }
                        }
                        break;

                case 3 : {
                            int val = st.peek();
                            if(val != -1)
                                System.out.println(val);
                        }
                        break;

                case 4 : {
                                if(st.empty())
                                    System.out.println("Stack is empty");
                                else    
                                    System.out.println("Stack is Not empty");
                        }
                        break;
                    
                case 5 : {
                            System.out.println("size is : " + st.size());
                        }
                        break;  
                case 6 : {
                            st.printStack();
                        }
                        break;
                case 7 :
                            flag = true;
                            break;

                default :
                            System.out.println("Wrong choice!!!");

            }

        }while(!flag);
        

    }
}