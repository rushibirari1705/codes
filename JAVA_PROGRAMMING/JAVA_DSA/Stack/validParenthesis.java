
// Problem :  Paranthesis checker

import java.util.*;
class ParanthesisChecker{

    boolean validParanthesis(String str){

        Stack<Character> st = new Stack<Character>();

        for(int i = 0; i < str.length(); i++){
            char ch = str.charAt(i);
            if(ch =='(' || ch == '{' || ch == '['){
                st.push(ch);
            }else{
                if(!st.empty()){
                    char topChar = st.peek();
                    if((topChar == '(' && ch == ')') || (topChar == '{' && ch == '}') 
                                                    || (topChar == '[' && ch == ']')){
                                                        st.pop();

                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            }

        }
        if(st.empty())
            return true;
        else
            return false;

    }

}

class Client{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter Expression : ");
        String str = sc.next();

        ParanthesisChecker p1 = new ParanthesisChecker(); 

        boolean ret =  p1.validParanthesis(str);
        if(ret)
            System.out.println("Balanced");
        else
            System.out.println("Not Balanced");
            sc.close();
    }
}