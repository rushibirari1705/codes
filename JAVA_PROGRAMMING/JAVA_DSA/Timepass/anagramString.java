package Timepass;
import java.util.Arrays;

class Solution{    
    static boolean isAnagram(String a,String b){

        char[] c1 = a.toCharArray();
        char[] c2 =  b.toCharArray();
    
        if(a.length()!=b.length()){
            return false;
        }
        Arrays.sort(c1);
        Arrays.sort(c2);
        
        for( int i=0;i<a.length();i++){
            if(c1[i]!=c2[i]){
                return false;
            }
        } 
        
        return true;
        
        
    }

    public static void main(String[] args) {
        
        String s1 = "shashi";
        String s2 = "ashish";

        if(isAnagram(s1, s2))
            System.out.println("Anagram ");
        else    
            System.out.println("Not anagram");
    }
}