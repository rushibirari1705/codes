

import java.util.*;
class Solution{

    static ArrayList<Integer> commonElements(int A[], int B[], int C[], int n1, int n2, int n3) {
        int i=0;
        int j=0;
        int k=0;
        
        ArrayList<Integer> A1 = new ArrayList<>();

        while (i < n1 && j < n2 && k < n3) {
            if (A[i] == B[j] && B[j] == C[k]) {
        
                A1.add(A[i]);
                i++;
                j++;
                k++;
                
            } else if (A[i] < B[j]) {
                i++;
            } else if (B[j] < C[k]) {
                j++;
            } else {
                k++;
            }
        }
            ArrayList<Integer> list = new ArrayList<>(new HashSet<>(A1));
        
        Collections.sort(list);
        return list;

    }

    public static void main(String[] args) {

        int A[] = {1, 5, 10, 20, 40, 80};
        int B[] = {6, 7, 20, 80, 100};
        int C[] = {3, 4, 15, 20, 30, 70, 80, 120};

        System.out.println(commonElements(A, B, C, A.length, B.length, C.length));


        
    }
}