package Timepass;

class Solution {

    static int equilibriumPoint(long a[], int n) {

        long sum=0,lsum=0;
        for(int i=0;i<n;i++)
            sum += a[i];
        for(int i=0;i<n;i++){
            sum=sum-a[i];
            if(lsum == sum)
                return i+1;

            lsum+=a[i];
        }
        return -1;
    
    }

    public static void main(String[] args) {
        
        long arr[] = new long[]{1,3,5,2,2};
        System.out.println(Solution.equilibriumPoint(arr, arr.length));


    }
}
