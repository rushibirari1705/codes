
// find Majority element form array:

class Solution
{
    static int majorityElement(int arr[], int n){
        int count=1,temp =0;
        
        for(int i=0;i<n;i++){

            if(arr[temp]==arr[i])
                count++;
            else
                count--;

            if(count==0){
                temp = i;
                count = 1;
            }
        }

        count = 0;
        for(int i=0;i<n;i++){

            if(arr[temp] == arr[i])
                count++;
        }
        
        if(count<=n/2)
            return -1;
        
            
        return arr[temp] ;
    }

    public static void main(String[] args) {
        int arr[] ={3,3,2,2,2,2,5};
        System.out.println(majorityElement(arr, arr.length));
    }
}