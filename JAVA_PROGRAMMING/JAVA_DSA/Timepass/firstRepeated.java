
package Timepass;

import java.util.HashMap;

class Solution {

    static int firstRepeated(int[] arr, int n) {

        //approach 1
        /*for( int i = 0;i<n-1;i++){
            for(int j =i+1;j<n;j++){
                if(arr[i]==arr[j]){
                    return i+1;
                }
            }
        }
        return -1;

        */
        //approach 2
        HashMap<Integer,Integer> hm = new HashMap<>();
        
        for(int i =0;i<n;i++) {
                hm.put(arr[i],hm.getOrDefault(arr[i],0)+1);
        }
        
        for(int i =0;i<n;i++) {
            if(hm.containsKey(arr[i]) && (hm.get(arr[i])>1)) {
                return i+1;
            }
        }
        return -1;
    }



    public static void main(String[] args) {
        
        int arr[] = {1, 5, 3, 4, 3, 5, 6};

        System.out.println(firstRepeated(arr, arr.length));
    }
}