package Timepass;

class PeakElement {

        static public int peakElement(int[] arr,int n){
            int count = 0;
            int index = 0;
            for(int i= 0; i<n; i++){
            count = 0;
        
            if(i-1 >= 0 && arr[i-1] <= arr[i] ){
                count++;
            }else if(i-1 < 0){
                count++;
            }
            
            if(i+1 < n && arr[i+1] <= arr[i]){
                count++;
            }else if(i+1 > n-1){
                count++;
            }

            if(count == 2){
                index = i;
                break;
            }
        }
            return index;
    }

    public static void main(String[] args) {
        
        int arr[] = new int[]{1,2,3};

        System.out.println(peakElement(arr,arr.length));
    }
    
}
