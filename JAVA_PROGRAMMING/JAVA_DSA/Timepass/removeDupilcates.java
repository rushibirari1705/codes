package Timepass;


import java.util.*;
import java.util.LinkedList;

class Node{
    int data;
    Node next = null;

    Node(int data){
        this.data = data;

    }
}


class Solution {

    static Node removeDuplicates(Node head)
    {
   
       Node curr = head;
       Node prev= head;

       while(prev!=null)
       {
           if(curr.data!=prev.data)
           {
               curr.next = prev;
               curr = prev;
           }
           else if(prev.next==null)
           {
               curr.next = null;
           }
           prev = prev.next;
       }
       return head;
    }

    public static void main(String[] args) {
        
        List<Integer> ll = new LinkedList<Integer>();
        ll.add(10);
        ll.add(10);
        ll.add(20);
        ll.add(30);
        ll.add(30);

        System.out.println(ll);

        //Node head = removeDuplicates();


    }
    
}
