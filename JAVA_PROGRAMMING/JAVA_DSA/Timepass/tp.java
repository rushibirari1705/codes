package Timepass;


class Solution {
    
    /*public static String merge(String word1, String word2) {
        StringBuilder sb = new StringBuilder();
        int min = Math.min(word1.length() , word2.length());
        for(int i=0 ; i<min ; i++){
            sb.append(word1.charAt(i));
            sb.append(word2.charAt(i));
        }
        if(word1.length() > min){
            sb.append(word1.substring(min,word1.length()));
        }
        if(word2.length() > min){
            sb.append(word2.substring(min,word2.length()));
        }
        return sb.toString();
    }*/

    static String merge1(String str1, String str2){

        int l1 = str1.length();
        int l2 = str2.length();

       int i = 0;
       int j = 0;
        String ans = "";
       while(i < l1 && j < l2) {

        ans += str1.charAt(i);
        ans += str2.charAt(j);

        i++;
        j++;
       }

       if(i < l1) {
        ans += str1.substring(i);
       }

       if(j < l2) {
        ans += str2.substring(j);
        }
        return ans;
    } 

	
	public static void main(String[] args) {
		String word1="Rushi";
		String word2="Birari";
		//System.out.println(Solution.merge(word1,word2));
        System.out.println();
        System.out.println(Solution.merge1(word1, word2));
		
		
	}
}
