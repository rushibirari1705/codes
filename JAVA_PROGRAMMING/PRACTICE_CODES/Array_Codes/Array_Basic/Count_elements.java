/*
 *  Given an array of integers of size N.
 *  Count the number of elements having at least one element greater than itself
 *  
 *  ip :
 *      arr = [2,5,1,4,8,0,8,1,3,8]
 *      N = 10 
 *  op :
 *      7    
 *  
 */

class Solution {

    static int countElements(int arr[], int N) {

        int count = 1;
        int max = Integer.MIN_VALUE;

        for (int i = 0; i < N; i++) {

            if (max == arr[i])
                count++;

            if (arr[i] > max) {

                max = arr[i];
                count = 1;
            }
        }

        return N - count;
    }

    public static void main(String[] args) {

        int arr[] = new int[] { 1, 2, 3, 4, 5, 6, 6 };

        System.out.println(Solution.countElements(arr, arr.length));

    }
}