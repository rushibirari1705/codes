
class Solution{
        static void RotateArray(int arr[],int n, int k){
            
            k = k % n;

            for(int i = 0; i < n; i++){
                if(i<k){
                        System.out.print(arr[n + i - k] + " ");
                }else{
                    System.out.print(arr[i - k] + " ");
                }
            }
            System.out.println();
        }
	
// Driver program
public static void main(String[] args)
{
	int[] Arr = {1, 2, 3, 4};
	int N = Arr.length;

	int K = 2;
	RotateArray(Arr, N, K);

}
}

