
//1 . Search an element in an array

import java.util.*;

class Solution{

    // method

    int serachElement(int arr[] , int key){
         int idx = -1;
        for(int i=0; i<arr.length; i++){
           
            if(arr[i] == key){
                idx = i;
            }

        }
        return idx;
    }
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        System.out.println("Enter the Array element you want to search  : " );
        int key = sc.nextInt();

        Solution obj = new Solution();

        int ret = obj.serachElement(arr, key);
        if(ret == -1)
            System.out.println("Element is not found in Array..");
        else    
            System.out.println("Element is found in Array at index : " + ret);

    }
}