
//10 . Max Odd Sum

import java.util.*;
class Solution{

    int maxOddSum(int[] arr){

        int sum = 0;
        int max = arr[0];
        for(int i = 0; i < arr.length; i++){
            if( max < arr[i] ){
                max = arr[i];
            }           
        }

        if( max <= 0)
            return max;

        for( int i = 0; i < arr.length; i++){
            if(arr[i] > 0)
                    sum += arr[i];
        }
        return sum;
    }
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        Solution obj = new Solution();

        int maxOddSum= obj.maxOddSum(arr);

        System.out.println("Maximum odd sum :" + maxOddSum);
    }
}