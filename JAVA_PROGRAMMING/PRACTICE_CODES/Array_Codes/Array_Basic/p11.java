

// 11 . Product of maximun element in first array and minimum element in second array

import java.util.*;
class Solution{
    


    int productElements(int arr[] , int arr1[]){
        int min = arr[0];
        int max = arr[0];

        for(int i = 0; i < arr1.length; i++){
            if(min > arr1[i]){
                min = arr1[i];
            }

        }
        for(int i = 0; i < arr.length; i++){
            if(max < arr[i]){
                max = arr[i];
            }

        }
        return max * min;
    }

    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array 1 size : ");

        int size1 = sc.nextInt();

        int arr[] = new int[size1];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size1 ; i++){
            arr[i] = sc.nextInt();
        }

        System.out.println("Enter the Array 2 size : ");

        int size2 = sc.nextInt();

        int arr1[] = new int[size1];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size2 ; i++){
            arr1[i] = sc.nextInt();
        }

        Solution obj = new Solution();

        int product = obj.productElements(arr, arr1);
        System.out.println("Product  : " + product);
    }
}