
//12 . First and last Occurences of element

import java.util.*;
class Solution{

    void firstLastOccurence(int[] arr, int ele){

        if(ele <= arr.length -1){
            int first = -1;
            int last = -1;
            for(int i=0; i<arr.length; i++){
                if(arr[i] == ele){
                    first = i;
                    break;
                }
            }

            for(int i = arr.length - 1; i >= 0; i--){
                if(arr[i] == ele){
                    last = i;
                    break;
                }
            }
            System.out.println("First Occurence of " + ele + "  at index  : " + first);
            System.out.println("Last Occurence of " + ele + "  at index  : " + last);

        }else{
            System.out.println(-1);
        }
    }
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        Solution obj = new Solution();

        System.out.println("Enter the element  : ");
        int ele = sc.nextInt();    

        obj.firstLastOccurence(arr,ele);

        
    }
}