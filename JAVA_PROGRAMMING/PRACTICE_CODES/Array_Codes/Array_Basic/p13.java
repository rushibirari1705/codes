
// 2 . Find unique element from array 

import java.util.*;
class Solution{
    
    int UniqueElement(int[] arr){
        
        for(int i=0; i<arr.length-1; i++){
            int cnt = 0;

            for(int j = 1; j<arr.length; j++){

                if(arr[i] == arr[j]){
                    cnt++;
                }

            }

            if(cnt == 0){
                return arr[i];
            }

        }
        return -1;
       
    }
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        Solution obj = new Solution();

        int element = obj.UniqueElement(arr);
        System.out.println(element);
    }
}