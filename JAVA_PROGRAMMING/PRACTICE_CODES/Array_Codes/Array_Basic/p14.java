
// 2 . Maximum repeating element

import java.util.*;
class Solution{
    
    int MaxRepeatingElement(int[] arr){
         int num = arr[0];
        for(int i = 0; i < arr.length - 1 ; i++){
            int cnt = 0;
          
               if(arr[i] == arr[i + 1 ]){
                    cnt++;
                    num = arr[i];
               }
        }
      return num;
    }
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        Solution obj = new Solution();

        int element = obj.MaxRepeatingElement(arr);
        System.out.println(element);

        sc.close();
    }
}