
// 15. Sum of Distinct Elements

import java.util.*;
class Solution{
    
    int SumOfDistinctElements(int[] arr){
         
        int sum = 0;
        int cnt = 0;

        for(int i = 0; i < arr.length; i++){
            
            for(int j = 0; j <= i; j++){

             if( j == i){
                sum += arr[j];
             }   

             if(arr[i] == arr[j]){
                break;
             }
            }
            
        }
        return sum;
    }
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        Solution obj = new Solution();

        int element = obj.SumOfDistinctElements(arr);
        System.out.println("Sum is : "  + element);

        sc.close();
    }
}