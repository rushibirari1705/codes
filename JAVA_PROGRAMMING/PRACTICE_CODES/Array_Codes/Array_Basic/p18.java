
// 18. find subArray with the given set and sum

import java.util.*;
class Solution{
    
    int SubarraySum(int[] arr , int keySum){

        
            int sum = arr[0] ,start = 0, i ;

            for(i = 1 ; i < arr.length ; i++){

                while(sum > keySum && start < i - 1){

                    sum = sum + arr[start];
                    start++;

                }

                if(sum == keySum){
                    int idx = i - 1;
                    System.out.println("SubArray found between " + start + " and " +idx);
                    return 1;
                    
                }

                if(i < arr.length)
                    sum += arr[i];  

            }
            System.out.println("No sub Array found..");
        return 0;
        }

    

       
    
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        System.out.println("Enter the sum : ");
        int sum = sc.nextInt();

        Solution obj = new Solution();

        obj.SubarraySum(arr, sum);
        
    }
}