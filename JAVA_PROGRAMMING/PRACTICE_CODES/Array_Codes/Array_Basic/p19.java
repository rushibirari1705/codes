

//19 . Find common elements in three sorted array
import java.util.*;
class Solution{

     void findCommonElements(int[] arr1, int[] arr2, int[] arr3) {
        
        int size1 = arr1.length;
        int size2 = arr2.length;
        int size3 = arr3.length;
        
        ArrayList<Integer> a = new ArrayList<Integer>();
    
        int i = 0, j = 0, k = 0; 
        
        while (i < size1 && j < size2 && k < size3) {
            
            if (arr1[i] == arr2[j] && arr2[j] == arr3[k]) {
                a.add(arr1[i]);
                
                i++;
                j++;
                k++;

            }else if (arr1[i] < arr2[j]) {
                i++; 

            }else if (arr2[j] < arr3[k]) {
                j++; 

            }else {
                k++; 
            }
           
        }
        System.out.println("Common Elements are :  " + a);
       
         
    }

    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

      System.out.println("Enter the Array 1 size : ");

       int size1 = sc.nextInt();

        int arr1[] = new int[size1];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size1 ; i++){
            arr1[i] = sc.nextInt();
        }
        // for sorting 
        for(int i = 0 ; i < size1 - 1 ; i++){
            if(arr1[i] > arr1[i + 1]){
                int temp = arr1[i];
                arr1[i] = arr1[i+1];
                arr1[i+1] = temp;

            }
        }

        System.out.println("Enter the Array 2 size : ");

        int size2 = sc.nextInt();

        int arr2[] = new int[size2];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size2 ; i++){
            arr2[i] = sc.nextInt();
        }
        // for sorting
        for(int i = 0 ; i < size2 - 1 ; i++){
            if(arr2[i] > arr2[i + 1]){
                int temp = arr2[i];
                arr2[i] = arr2[i+1];
                arr2[i+1] = temp;

            }
            
        }
         System.out.println("Enter the Array 3 size : ");

        int size3 = sc.nextInt();

        int arr3[] = new int[size3];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size3 ; i++){
            arr3[i] = sc.nextInt();
        }
        // for sorting
        for(int i = 0 ; i < size3 - 1 ; i++){
            if(arr3[i] > arr3[i + 1]){
                int temp = arr3[i];
                arr3[i] = arr3[i+1];
                arr3[i+1] = temp;

            }
            
        }
        
        Solution obj = new Solution();
        obj.findCommonElements(arr1,arr2,arr3);
       


       
    }
}