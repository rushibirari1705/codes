
// 2 . Find the Minimum and Maximum elements from array.
import java.util.*;
class Solution{
    // find the minimum element 
/*
    int minElement(int arr[]){
        int min = arr[0];
        for(int i = 0; i < arr.length; i++){
            if(min > arr[i]){
                min = arr[i];
            }

        }
        return min;
    }

    // find the maximum element 

    int maxElement(int arr[]){
        int max = arr[0];
        for(int i = 0; i < arr.length; i++){
            if(max < arr[i]){
                max = arr[i];
          }

        }
        return max;
    }
*/
        int[] findMinMax(int[] arr){
   
            int[] newarr  = new int[2];
            Arrays.sort(arr);
            newarr[0] = arr[0];
            newarr[1] = arr[arr.length - 1];

         return newarr;
    }

    
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        Solution obj = new Solution();
/*
        int min = obj.minElement(arr);
        int max = obj.maxElement(arr);

        System.out.println("Minimun element: " + min);
        System.out.println("Maximun element: " + max);
 */
        int arr1[] = obj.findMinMax(arr);
        System.out.println();
        System.out.println(arr1[0]);
        System.out.println(arr1[1]);
    }
}