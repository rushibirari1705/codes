
// 20. check if pair with given Sum in array:


import java.util.*;
class Solution{

    int checkPairSum(int[] arr, int sum){
        
        for(int i = 0; i < arr.length -1; i++){

            if((arr[i] + arr[i + 1]) == sum){

                //System.out.println(i + " and " + (i+1));
                return 1;
            }
        }
        return 0;
    }
      public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        System.out.println("Enter the sum : ");
        int sum = sc.nextInt();

        Solution obj = new Solution();

       int ret = obj.checkPairSum(arr, sum);
       if(ret == 1)
            System.out.println("sum found : Yes");
       else
            System.out.println("sum not found : No");
            
    }
}