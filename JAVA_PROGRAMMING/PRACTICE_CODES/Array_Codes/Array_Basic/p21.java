
// 21. first element to occur k times




import java.util.*;
class Solution{

     int ElementOccur(int arr[] , int occur){

       HashMap<Integer, Integer> map = new HashMap<>();
        
       for(int ele : arr){
        if(map.containsKey(ele)){
            map.put(ele, map.get(ele)+1);
            if(map.get(ele) == occur){
                return ele;
            }
        }else{
            map.put(ele,  1);
            if(map.get(ele) == occur){
                return ele;
            }

        }
    } 
       return -1;    
   }

      public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        System.out.println("Enter the occur time : ");
        int occur = sc.nextInt();

        Solution obj = new Solution();

       int element = obj.ElementOccur(arr, occur);
       System.out.println("first occur element : " + element);
      
            
    }
}