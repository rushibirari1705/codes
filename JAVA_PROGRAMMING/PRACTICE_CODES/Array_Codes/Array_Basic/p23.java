
// 23. Find the smallest amd second smallest element in an array:

import java.util.*;
class Solution{

    void MinEle_Second_MinEle(int[] arr){

        Arrays.sort(arr);
        System.out.println("Minimum element in array is  : " +arr[0]);
        System.out.println("Second Minimum element in array is  : " +arr[1]);
        
    }
    
      public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        
        Solution obj = new Solution();

        obj.MinEle_Second_MinEle(arr);


    
    }
}