

// 24. Ceil and floor of given element

import java.util.*;
class Solution{

        void CeilAndFloor(int arr[],int ele){

            Arrays.sort(arr);
            int floor = -1;
            int ceil =  -1;
            for(int i = 0; i < arr.length - 1; i++){

                if(ele == arr[i]){
                    //System.out.println("Ceil and floor :  " + arr[i]);
                    floor = arr[i];
                    ceil = arr[i];
                   
                }else{
                    if(ele < arr[0] && ele > arr[arr.length - 1]){
                        floor = -1;
                        ceil =-1;
                    }else if(ele <= arr[0] ){

                        if(ele > arr[i] || ele < arr[i+1]){
                            ceil = arr[i+1];
                        }

                    }else if(ele >= arr[arr.length - 1] ){

                        if(ele > arr[i] || ele < arr[i+1]){
                            floor = arr[i+1];
                        }
                    }else if(ele > arr[i] && ele < arr[i+1]){
                        floor = arr[i];
                        ceil = arr[i+1];                    
                    
                    }
            }
         }

            System.out.println("floor : " + floor);
            System.out.println("ceil : " + ceil);
        

        }
   
      public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        System.out.println("Enter the Element : ");
        int ele = sc.nextInt();

        
        Solution obj = new Solution();

        obj.CeilAndFloor(arr,ele);


    
    }
}