


//  25. Maximum product of numbers

import java.util.*;
class Solution{
    int maxProduct(int[] arr){

        Arrays.sort(arr);
        return arr[arr.length - 1] * arr[arr.length - 2];

    }
        
      public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        Solution obj = new Solution();

        int ret = obj.maxProduct(arr);
        System.out.println("Product is :  " + ret);


    
    }
}