
// 26 . Positive and Negative elements :

import java.util.*;
class Solution{
    
    int[] sequenceNum(int[] arr){

        int[] result = new int[arr.length];
        int itr = 0;
        for(int i = 0; i < arr.length; i++){

            if(arr[i] > 0){
                if(itr % 2 == 1){  
                    result[itr - 1] = arr[i];
                }
            }else {
                if(itr % 2 == 0){
                 result[itr + 1] = arr[i];
                }
            }
            itr++;
        }
        return result;
    }
        
      public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        Solution obj = new Solution();

        int[] ret = obj.sequenceNum(arr);
        for(int i = 0 ; i < ret.length ; i++){
            System.out.print(ret[i] + "  ");
        }


    
    }
}