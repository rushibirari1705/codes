
// 27 . Count Pair sum

import java.util.*;
class Solution{

        int countPairSum(int[] arr1, int[] arr2, int sum){
            int cnt = 0;

            for(int i=0; i<arr1.length; i++){
                for(int j=0; j<arr2.length; j++){

                    if((arr1[i] + arr2[j]) == sum){
                        cnt++;
                    }

                }
            }
            return cnt;
        }
        
   
      public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array 1 size : ");

        int size1 = sc.nextInt();

        int arr1[] = new int[size1];

        System.out.println("Enter the Array 1 Elements  : ");

        for(int i = 0 ; i < size1 ; i++){
            arr1[i] = sc.nextInt();
        }
        
        System.out.println("Enter the Array 2 size : ");

        int size2 = sc.nextInt();

        int arr2[] = new int[size2];

        System.out.println("Enter the Array 2 Elements  : ");

        for(int i = 0 ; i < size2 ; i++){
            arr2[i] = sc.nextInt();
        }

        System.out.println("Enter the sum : ");
        int sum = sc.nextInt();
                
        Solution obj = new Solution();

        int count = obj.countPairSum(arr1,arr2 , sum);
        System.out.println("Count : "+count);

        


    
    }
}