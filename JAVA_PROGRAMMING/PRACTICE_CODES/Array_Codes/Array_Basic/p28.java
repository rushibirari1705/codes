
// 28 . Remove dupplicates from unsorted array

import java.util.*;
class Solution{

        int[] removeDuplicates(int[] arr){

            HashSet<Integer> set = new HashSet<>();
            for(int i = 0; i <arr.length ; i++){
                set.add(arr[i]);
            }
           // System.out.println(set);
            int[] newArr = new int[set.size()];
            int i = 0;
            for(int ele : set){
                newArr[i++] = ele;
            }
            return newArr;
        }
       
      public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        Solution obj = new Solution();

        int[] result = obj.removeDuplicates(arr);
        System.out.println("After removing duplicates : ");
        for(int i = 0 ; i < result.length ; i++){
            System.out.print( result[i] + "  ");
        }


    
    }
}
