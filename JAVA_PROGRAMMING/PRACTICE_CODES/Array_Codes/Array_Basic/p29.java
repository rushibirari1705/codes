
// 29 . Last index of One from string

import java.util.*;
class Solution{

    int findOneIndex(String str){

        char[] ch = str.toCharArray();
        int idx = -1;
        for(int i = ch.length -1 ; i >= 0 ; i--){
            if(ch[i] == '1'){
                idx = i;
                break;
            }
        }

        return idx;

    }
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the string in 0's and 1's  :  ");
        String str  = sc.next();

        Solution s = new Solution();
        int retval = s.findOneIndex(str);

        System.out.println("Last index : " + retval);
        
    }
}