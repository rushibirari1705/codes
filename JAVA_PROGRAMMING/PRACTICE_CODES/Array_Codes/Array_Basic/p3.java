


// 3. Find largest elements from array.
import java.util.*;
class Solution{

    // find the maximum element 

    int maxElement(int arr[]){
        
        /* bruteforce approach
        int max = arr[0];
        for(int i = 0; i < arr.length; i++){
            if(max < arr[i]){
                max = arr[i];
          }

        }
        */
        
        Arrays.sort(arr);

        return arr[0];

    }
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        Solution obj = new Solution();

        int max = obj.maxElement(arr);

        System.out.println("Largest element: " + max);
    }
}