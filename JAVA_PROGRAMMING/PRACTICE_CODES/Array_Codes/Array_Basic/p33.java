
// 33. Multiply left and right array sum

import java.util.*;
class Solution{

     int multiplyArraySum(int[] arr){

       int n = arr.length;

       if(n == 1){

           return arr[0];

       }else{

           int low = 0;
           int high = arr.length-1;
           int mid=  low + (high-low)/2;

           if(n % 2 == 0){

               mid++;

           }

           return  Sum(arr, low, mid-1) * (Sum(arr, mid, high));
       }

   }
    int Sum(int[] arr, int left, int right){

       int sum = 0;
       for(int i = left; i <= right; i++){
           sum += arr[i];
       }

       return sum;

   }
      public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        Solution obj = new Solution();

        int product = obj.multiplyArraySum(arr);
        System.out.println("Product  : "+product);
       


    
    }
}
