
// 34. Print the array in Pendulam Arrangement..
/*
import java.util.*;
class Solution{

        void pendulumArrange(int[] arr){

        }
      public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        Solution obj = new Solution();

        obj.pendulumArrange(arr);
       


    
    }
}

*/
import java.util.*;
class Solution {	

   public static void main(String args[]){

      Scanner sc = new Scanner(System.in);   

      System.out.println("Enter the size of the array (odd) :");
      int size = sc.nextInt();

      int [] arr = new int[size];

      System.out.println("Enter elements of the array :");
      for(int i = 0; i< size; i++){
         arr[i] = sc.nextInt(); 
      }
      System.out.println("Array Elemens are :  "+Arrays.toString(arr));
      Arrays.sort(arr);
      int temp = arr[0];
      int mid = (size - 1)/2;
      int k = 1;
      int[] result = new int[size];
      result[mid] = temp;
      for(int i = 1; i <= mid; i++){
          result[mid+i] = arr[k++];
          result[mid-i] = arr[k++];
      }      
            
      System.out.print("Pendulum arrangement :  ");
      for(int i = 0; i < result.length; i++){
        System.out.print(result[i]+ "  ");
      }
   }
}
