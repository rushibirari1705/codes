
//  25. Minimum product of  k elememts

import java.util.*;
class Solution{
    
        int minProduct(int[] arr, int k){
            int product = 1;
            Arrays.sort(arr);
            for(int i = 0; i < k; i++){
                product *= arr[i];
            }
            return product;
        }
        
      public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        System.out.println("Enter the value of K : ");
        int k = sc.nextInt();

        Solution obj = new Solution();

        int ret = obj.minProduct(arr,k);
        System.out.println("Product is :  " + ret);


    
    }
}