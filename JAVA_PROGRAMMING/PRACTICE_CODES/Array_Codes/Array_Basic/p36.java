
// 36.Find the peak element which is not smaller than its neighbours.

import java.util.*;
class Solution{

        int findPeakElement(int[] arr){
            int max = arr[0];
            
            for(int i = 1; i < arr.length - 1; i++){

                if((arr[i] > max ) && (arr[i + 1] > max)){
                
                    max = arr[i];
                }

            }
            return max;
        }
      public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        Solution obj = new Solution();

        int element = obj.findPeakElement(arr);
        System.out.println("Peak Element  :" + element);
       


    
    }
}
