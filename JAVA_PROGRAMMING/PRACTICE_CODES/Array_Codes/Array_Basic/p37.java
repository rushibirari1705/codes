
// 37. Move all negative numbers to begining and postive to end of array

import java.util.*;
class Solution{

        int[] MoveElements(int[] arr){

            int[] result = new int[arr.length];
            int cnt = 0;
            for(int i = 0; i < arr.length; i++){
                if(arr[i] < 0){
                    cnt++;
                }
            }
            int itr = cnt;
            int j = 0;
            for(int i = 0; i < arr.length; i++){
                if(arr[i] < 0){
                    result[j++] = arr[i];
                }
                if(arr[i] > 0){
                    result[itr++] = arr[i];
                    
                }

            }

            return result;
            
        }
      public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        Solution obj = new Solution();

        int[] result = obj.MoveElements(arr);
        System.out.println("After moving numbers : "+ Arrays.toString(result));
       


    
    }
}
