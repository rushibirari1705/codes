
// 38 . Intermediate smaller number

import java.util.*;
class Solution{

    int[] intermediateSmallerNumbers(int[] arr){

        int[] newArr = new int[arr.length];

        for(int i = 0; i < arr.length - 1; i++){

            if(arr[i] > arr[i+1]){
                newArr[i] = arr[i+1];
            }else{
                newArr[i] = -1;
            }
        }
        newArr[arr.length -1] = -1;
        return newArr;
    }
    
      public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        Solution obj = new Solution();

        int[] result = obj.intermediateSmallerNumbers(arr);
        System.out.println("result is  : "+ Arrays.toString(result));
       


    
    }
}
