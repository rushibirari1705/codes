
// 39 . Leaders in Array

import java.util.*;
class Solution{

    void LeadersInArray(int[] arr){

        int maxElement = arr[arr.length - 1];
        List<Integer> al = new ArrayList<Integer>();
        al.add(maxElement);

        for(int i = arr.length  - 2 ; i >= 0 ; i--){
            if(arr[i] > maxElement){
                al.add(arr[i]);
                maxElement = arr[i];
            }
        }
        Collections.reverse(al);
        System.out.println("Leaders in Array are : " + al);
    }
    
      public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        Solution obj = new Solution();

        obj.LeadersInArray(arr);


    
    }
}
