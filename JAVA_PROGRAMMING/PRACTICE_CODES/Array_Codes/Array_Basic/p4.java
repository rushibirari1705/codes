

// 2 . Product of elements from array.
import java.util.*;
class Solution{
   

    int getProduct(int[] arr){

        int product = 1;
        for(int i = 0; i < arr.length; i++){
            product *= arr[i];
        }

        return product;
    }
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        Solution obj = new Solution();

       int product = obj.getProduct(arr);
        System.out.println("Product of elements of array  : " + product);
    }
}