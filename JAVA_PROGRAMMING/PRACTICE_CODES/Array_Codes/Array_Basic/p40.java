
// 40 . Fibonacci in the array

import java.util.*;
class Solution{

    int fibonacciElements(int arr[]){
        int a = 0, b = 1, c = 0;
        int cnt = 0;
        Arrays.sort(arr);
        int max = arr[arr.length - 1];
        List<Integer> al = new ArrayList<Integer>();
        al.add(a);
        
        for(int i = 1; i <= max; i++){
            c = a + b;
            a = b;
            b = c;
            al.add(a);
        }

        for(int i = 0; i < arr.length; i++){
            if(al.contains(arr[i]))
                cnt++;
        }
        return cnt;
        
    }

      public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        Solution obj = new Solution();

        int count  = obj.fibonacciElements(arr);

        System.out.println("Count : " + count);


    
    }
}
