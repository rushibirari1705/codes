
// 40 . Countries at war

import java.util.*;
class Solution{

      String winnerCountry(int[] a, int[] b){
       

        int winsForA = 0;
        int winsForB = 0;
        
        for (int i = 0; i < a.length; i++) {
            if (a[i] > b[i]) {
                winsForA++;
            } else if (a[i] < b[i]) {
                winsForB++;
            }
        }
        
        if (winsForA > winsForB) {
            return "A";
        } else if (winsForA < winsForB) {
            return "B";
        } else{
            return "Tie";
        }
        
    }

      public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size 1 : ");

        int size1 = sc.nextInt();

        int A[] = new int[size1];

        System.out.println("Enter the Array 1 Elements  : ");

        for(int i = 0 ; i < size1 ; i++){
            A[i] = sc.nextInt();
        }

        System.out.println("Enter the Array size 2 : ");

        int size2 = sc.nextInt();

        int B[] = new int[size2];

        System.out.println("Enter the Array 1 Elements  : ");

        for(int i = 0 ; i < size2 ; i++){
            B[i] = sc.nextInt();
        }


        Solution obj = new Solution();

        String winner  = obj.winnerCountry(A, B);

        System.out.println("Winner country : " + winner);


    
    }
}
