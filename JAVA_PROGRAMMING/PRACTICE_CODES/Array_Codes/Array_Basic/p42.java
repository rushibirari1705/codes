
// 42. Count the number of elements between two given elementsin an array . Given an unsorted array


import java.util.*;
class Solution{

    int countElements(int[] arr, int num1, int num2){
     
        int n1_idx = -1;
        int n2_idx = -1;
        int min = arr[0];
        int max = arr[0];
        for(int i = 1; i < arr.length; i++){
            if(min > arr[i]){
                min = arr[i];
            }

            if(max < arr[i]){
                max = arr[i];
            }
        }  
        
        if(min <= num1 && max >= num2){
            for(int i = 0; i < arr.length ; i++){
            
                if(arr[i] == num1){
                    n1_idx = i;
                    break;
                }
            }

            for(int i = arr.length - 1; i >= 0; i--){
                
                    if(arr[i] == num2){
                        n2_idx = i;
                        break;
                        
                    }
            }
        }else{
            return -1 ;
        }

       return n2_idx - n1_idx - 1;
    }

      public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        System.out.println("Enter the Num 1 : ");
        int num1 = sc.nextInt();
        System.out.println("Enter the Num 2 : ");
        int num2 = sc.nextInt();
        


        Solution obj = new Solution();

        int count  = obj.countElements(arr, num1, num2);

        System.out.println("Count : " + count);


    
    }
}
