// 5. replace all 0's with number 5

import  java.util.*;

class Solution{

    int replaceNum(int num){
/* 
        //bruteforce
        int rev = 0;
        while(num != 0){
            int rem = num % 10;

            if(rem == 0)
                rem = 5;

           rev = rev * 10 + rem;
            num /= 10;

        }
        
        int n = rev;
        int rev1 = 0;
        while(n != 0){
             int rem = n % 10;

            rev1 = rev1 * 10 + rem;
            n /= 10;
 
        }
        return rev1;*/

        // optimze

        String str = Integer.toString(num);
        str = str.replaceAll("0", "5");

        return Integer.valueOf(str);
        
    }
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Number :: ");
        int num = sc.nextInt();

        Solution obj = new Solution();

        int retval = obj.replaceNum(num);

        System.out.println("Number :: " + retval);


    }
}