
// 6. Elements in range
import java.util.*;
class Solution{

   String getElements(int arr[] , int start, int end){

        for(int i=0; i<arr.length; i++){
            if( arr[i] >= start && arr[i] <= end)
                return "Yes";

        }

        return "No";
    }
   
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        System.out.println("Enter the Range A to B  : ");
        int a = sc.nextInt();
        int b = sc.nextInt();

        Solution obj = new Solution();

       String value= obj.getElements(arr, a, b);
       System.out.println(value);

    
    }
}