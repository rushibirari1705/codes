
// 7. Form the largest number from digits

import java.util.*;
class Solution{

    String formNumber(int[] arr){
        
        Arrays.sort(arr);
        String str = "";

        for(int i = 0; i < arr.length; i++){

            str = Integer.toString(arr[i]) + str;
        }
        return str;
    }
    
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        Solution obj = new Solution();

       String value= obj.formNumber(arr);
       System.out.println(value);

    
    }
}