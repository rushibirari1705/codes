
// 8. Even Occuring elements

import java.util.*;
class Solution{

    ArrayList<Integer> EvenNumbers(int arr[]){
     /*
        for(int i=0; i<arr.length; i++){
            int count = 0;
            
            for(int j=0; j<arr.length; j++){
                if(arr[i] == arr[j]){
                    count++;
                }
            }

            if(count >= 2){
                System.out.print(" " + arr[i]);
                
            }
        }
        */

        Arrays.sort(arr);
        int cnt = 0;

        ArrayList<Integer> al = new ArrayList<>();
        for(int i = 0; i < arr.length - 1;i++){
            cnt++;
            if(arr[i] != arr[i+1]){
                
                if(cnt % 2 == 0){
                    //System.out.println("  " + arr[i]);
                    al.add(arr[i]);
                }
                cnt = 0;
            }
        }

        if(cnt > 0){
          al.add(arr[arr.length-1]);
        }

        if (al.isEmpty()){
            al.add(-1);
        }

        return al;
    }
       
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        Solution obj = new Solution();

        ArrayList<Integer> al = obj.EvenNumbers(arr);
        System.out.println(al);
    
    
    
    }
}