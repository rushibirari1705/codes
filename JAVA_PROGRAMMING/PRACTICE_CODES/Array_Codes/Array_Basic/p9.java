
// 9. Remove an element at Specific Index from an Array

import java.util.*;
class Solution{

   int[] RemoveElement(int arr[] ,int index){

        int newArr[] = new int[arr.length - 1];
        int k = 0;
        for(int i = 0; i < arr.length; i++){
            if(index != i){
                newArr[k] = arr[i];
                k++;
            }
        }

        return newArr;


    }
   
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the Array size : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        System.out.println("Enter the Array Elements  : ");

        for(int i = 0 ; i < size ; i++){
            arr[i] = sc.nextInt();
        }

        System.out.println("Enter the index: ");
    
        int index = sc.nextInt();

        Solution obj = new Solution();

      int[] newArr =  obj.RemoveElement(arr, index);

       for(int i = 0; i < newArr.length ; i++){
        System.out.print(newArr[i] + "  ");
       }
       

    
    }
}